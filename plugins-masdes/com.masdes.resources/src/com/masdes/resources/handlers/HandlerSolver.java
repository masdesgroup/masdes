/**
 * 
 */
package com.masdes.resources.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.masdes.resources.Resources;

/**
 * @author maria messageKind
 */
public abstract class HandlerSolver extends AbstractHandler {

    /**
     * Determine if using selected file in workbench or an external one
     */
    protected Boolean useExternalFilePath;

    private Path inputFilePath;

    /**
     * File name without extension of the net
     */
    private String netFileName;

    private String[] commandParameters;

    protected String inputFilePathString;

    protected String inputDirectoryPathString;

    protected String outputDirectoryPathString;

    /**
     * Constructor
     */
    public HandlerSolver() {
        
        setUseExternalFilePath( false );
    }
    
    
    public HandlerSolver( String netFileName ) {
        
        setUseExternalFilePath( false );
        setNetFileName( netFileName );
    }
    
    public HandlerSolver( Path inputFilePath ) {
        
        setInputFilePath( inputFilePath );
        setUseExternalFilePath( true );
    }
    
    
    public HandlerSolver( Path inputFilePath, String netFileName ) {
        
        setInputFilePath( inputFilePath );
        setUseExternalFilePath( true );
        setNetFileName( netFileName );
    }
    
    

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.
     * ExecutionEvent)
     */
    public Object execute( IStructuredSelection structuredSelection )
            throws ExecutionException {

        // check if it is an IFile
        if ( structuredSelection.getFirstElement() instanceof IFile ) {

            String outputDirectoryPathString = "";
            String inputDirectoryPathString;

            if ( useExternalFilePath() ) {

                Path workspaceDirectory = getWorkspaceLocation();

                IPath outputDirectoryPath = workspaceDirectory.append( getInputFilePath().toPortableString() );

                // Construct the output absolute directory path (folder results
                // in project)
                outputDirectoryPathString = outputDirectoryPath.removeLastSegments( 1 ).toPortableString();

                // get the directory path, relative to workspace
                inputDirectoryPathString = getInputFilePath().removeLastSegments( 1 ).toPortableString();

            } else {

                // get the selected file
                IFile inputFile = ( IFile ) structuredSelection.getFirstElement();
                setInputFilePath( ( Path ) inputFile.getFullPath() );

                // Construct the output absolute directory path (folder results in project)
                outputDirectoryPathString = inputFile.getParent().getLocation().toPortableString() + Resources.RESULTS_FOLDER;

                // get the directory path, relative to workspace
                inputDirectoryPathString = inputFile.getParent().getFullPath().toPortableString();
            }

            //this.setNetFileName( getInputFilePath().removeFileExtension().toFile().getName() );
            this.setInputFilePathString( getInputFilePath().toPortableString() );
            this.setOutputDirectoryPathString( outputDirectoryPathString );
            this.setInputDirectoryPathString( inputDirectoryPathString );
        }

        return null;
    }

    protected Path getInputFilePath() {

        return inputFilePath;
    }

    protected void setInputFilePath( Path inputFilePath ) {
        
        this.inputFilePath = inputFilePath;
    }

    
    /**
     * @param size
     *            Number of elements of the commandParameters parameter
     * 
     */
    protected void initCommandParameters( int size ) {
        
        commandParameters = new String[ size ];
    }
    
    
    protected String[] getCommandParameters() {
        
        return commandParameters;
    }
    
    
    protected void setCommandParameters( int pos, String queryArg ) {
        
        this.commandParameters[ pos ] = queryArg;
    }
    
    
    protected String getInputFilePathString() {
        
        return inputFilePathString;
    }
    
    
    private void setInputFilePathString( String inputFilePathString ) {
        
        this.inputFilePathString = inputFilePathString;
    }
    
    
    protected String getOutputDirectoryPathString() {
        
        return outputDirectoryPathString;
    }
    
    
    private void setOutputDirectoryPathString( String outputDirectoryPathString ) {
        
        this.outputDirectoryPathString = outputDirectoryPathString;
    }
    
    
    protected String getInputDirectoryPathString() {
        
        return inputDirectoryPathString;
    }
    
    
    private void setInputDirectoryPathString( String inputDirectoryPathString ) {
        
        this.inputDirectoryPathString = inputDirectoryPathString;
    }
    
    
    public String getNetFileName() {
        
        return netFileName;
    }
    
    
    public void setNetFileName( String netFileName ) {
        
        this.netFileName = netFileName;
    }
    
    
    private Boolean useExternalFilePath() {
        
        return useExternalFilePath;
    }
    
    
    protected void setUseExternalFilePath( Boolean useExternalFilePath ) {
        
        this.useExternalFilePath = useExternalFilePath;
    }
    
    
    /**
     * Get location of workspace
     * 
     * @return
     */
    public static Path getWorkspaceLocation() {

        IWorkspace workspace = ResourcesPlugin.getWorkspace();

        Path workspaceDirectory = ( Path ) workspace.getRoot().getLocation();

        return workspaceDirectory;
    }
}
