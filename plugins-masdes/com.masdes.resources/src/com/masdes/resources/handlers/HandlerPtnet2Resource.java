/**
 * 
 */
package com.masdes.resources.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;


/**
 * @author maria
 *messageKind
 */
public abstract class HandlerPtnet2Resource extends AbstractHandler {


    /**
     * Determine if using selected file in workbench or an external one
     */
    protected Boolean useExternalFilePath;
    
    private Path inputFilePath;
    
    private String netFileName;
    
    private String [] queryArgs;
    
    protected String inputFilePathString;
    
    protected String inputDirectoryPathString;
    
    protected String outputDirectoryPathString;
    
    
    /**
     * Constructor
     */
    public HandlerPtnet2Resource() {
        
        setUseExternalFilePath( false );
    }
    
    public HandlerPtnet2Resource( String netFileName ) {
        
        setNetFileName( netFileName );
        setUseExternalFilePath( false );
    }
    
    
    public HandlerPtnet2Resource( Path inputFilePath ) {
        
        setInputFilePath( inputFilePath );
        setUseExternalFilePath( true );
    }
    
    
    public HandlerPtnet2Resource( Path inputFilePath, String netFileName ) {
        
        setNetFileName( netFileName );
        setInputFilePath( inputFilePath );
        setUseExternalFilePath( true );
    }
    
    
    private Boolean useExternalFilePath() {
        
        return useExternalFilePath;
    }

    protected void setUseExternalFilePath( Boolean useExternalFilePath ) {
        
        this.useExternalFilePath = useExternalFilePath;
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    public Object execute( IStructuredSelection structuredSelection )
            throws ExecutionException {
        
        //check if it is an IFile
        if ( structuredSelection.getFirstElement() instanceof IFile ) {
            
            String outputDirectoryPathString = "";
            String inputDirectoryPathString;
            
            if ( useExternalFilePath() ) {
                
                
                Path workspaceDirectory = getWorkspaceLocation();
                
                IPath outputDirectoryPath = workspaceDirectory.append( getInputFilePath().toPortableString() );
                
                // Construct the output absolute directory path (folder results in project) 
                outputDirectoryPathString = outputDirectoryPath.removeLastSegments( 1 ).toPortableString();
                
                // get the directory path, relative to workspace
                inputDirectoryPathString = getInputFilePath().removeLastSegments( 1 ).toPortableString();
                
            } else {
                
                // get the selected file
                IFile inputFile = ( IFile ) structuredSelection.getFirstElement();
                setInputFilePath( ( Path ) inputFile.getFullPath() );
                
                // Construct the output absolute directory path (folder results in project)
                outputDirectoryPathString = inputFile.getParent().getLocation().toPortableString();
                
                // get the directory path, relative to workspace
                inputDirectoryPathString = inputFile.getParent().getFullPath().toPortableString();
            }
            
            this.setInputFilePathString( getInputFilePath().toPortableString() );
            this.setOutputDirectoryPathString( outputDirectoryPathString );
            this.setInputDirectoryPathString( inputDirectoryPathString );
        }
        
        return null;
    }
    
    
    protected Path getInputFilePath() {
        
        return inputFilePath;
    }
    
    
    protected void setInputFilePath( Path inputFilePath ) {
        this.inputFilePath = inputFilePath;
    }
    
    
    /**
     * @param size
     *          Number of elements of the queryArgs parameter
     * 
     */
    protected void initQueryArgs( int size ) {
        
        queryArgs = new String[ size ];
    }
    
    
    protected String[] getQueryArgs() {
        
        return queryArgs;
    }
    
    
    protected void setQueryArgs( int pos, String queryArg ) {
        
        this.queryArgs[ pos ] = queryArg;
    }
    
    
    protected String getInputFilePathString() {
        
        return inputFilePathString;
    }
    
    
    private void setInputFilePathString( String inputFilePathString ) {
        
        this.inputFilePathString = inputFilePathString;
    }
    
    
    protected String getOutputDirectoryPathString() {
        
        return outputDirectoryPathString;
    }
    
    
    private void setOutputDirectoryPathString( String outputDirectoryPathString ) {
        
        this.outputDirectoryPathString = outputDirectoryPathString;
    }
    
    
    protected String getInputDirectoryPathString() {
        
        return inputDirectoryPathString;
    }
    
    
    private void setInputDirectoryPathString( String inputDirectoryPathString ) {
        
        this.inputDirectoryPathString = inputDirectoryPathString;
    }
    
    
    protected String getNetFileName() {
        return netFileName;
    }
    
    
    protected void setNetFileName(String netFileName) {
        this.netFileName = netFileName;
    }
    
    
    /**
     * Get location of workspace
     * 
     * @return
     */
    public static Path getWorkspaceLocation() {
        
        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        
        Path workspaceDirectory = ( Path ) workspace.getRoot().getLocation();
        
        return workspaceDirectory;
    }
}
