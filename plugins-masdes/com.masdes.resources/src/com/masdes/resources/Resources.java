package com.masdes.resources;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

public class Resources {

    public static final Boolean DEVELOP = true;
    public static final String EXTENSION_PTNET = "ptnet";
    public static final String EXTENSION_UML = "uml";
    public static final String EXTENSION_NET = "net";
    public static final String EXTENSION_LTL = "ltl";
    public static final String EXTENSION_PRB = "prb";
    public static final String EXTENSION_XML = "xml";
    public static final String EXTENSION_ASM = "asm";
    public static final String EXTENSION_LOG = "log";
    public static final String EXTENSION_ECORE = "ecore";
    
    public static final String START_FILE_PATH = "file:";
    public static final String START_NET_TINA_FILE = "net ";
    public static final String START_NET_PROD_FILE = "/* net: ";
    public static final String START_LTL_FILE = "# Queries: ";
    public static final String START_PRB_FILE = "/* Queries: ";
    public static final String END_NET_PROD_FILE = " */";
    public static final String END_PRB_FILE = " */";
    public static final String SUFFIX_PROD = "_prod";
    public static final String SUFFIX_TINA = "_tina";
    
    public static final String RESULTS_FOLDER = "/results";
    public static final String DOTS = "...";
    public static final String DOT = ".";
    public static final String PATH_SEPARATOR = "/";
    public static final String SEPARATOR_MODULES = ",";
    
    public static final String PROP_MODULES = ".modules";
    public static final String PROP_METAMODELS = ".metamodels";
    public static final String PROP_LIBRARIES = ".libraries";
    public static final String PROP_OPTIONS = ".options";
    
    public static final String LAUNCHER_RUN = "run";
    
    
    // ATL
    // ***
    public static final String METAMODEL_PTNET = "ptnet";
    public static final String MODEL_IN = "IN";
    
    
    // Console constants
    // *****************
    
    // Console name
    public static final String CONSOLE_TITLE = "MASDES Analyzer";
    
    // Messages
    public static final String MSG_ATL_SUCCESS = "ATL transformation success in ";
    public static final String MSG_RESULT_FILES = "Creating result files...";
    public static final String MSG_RESULTS_SAVED = "Results saved in ";
    public static final String MSG_LOAD_INPUT_MODEL = "Loading input model...";
    public static final String MSG_LOAD_PARAMETERS = "Loading parameters...";
    public static final String MSG_ATL_START = "Starting ATL transformation...";
    public static final String MSG_SOLVER_START = "Starting solver...";
    public static final String MSG_SECONDS = " s.";
    public static final String MSG_FIN = "FIN";
    public static final String MSG_ARGUMENTS_NOT_VALID4 = 
            "Arguments not valid : {IN_model_path, logicalProperties_model_path, OUT_model_path, output console}.";
    public static final String MSG_ARGUMENTS_NOT_VALID3 = 
            "Arguments not valid : {IN_model_path, logicalProperties_model_path, OUT_model_path}.";
    public static final String MSG_ARGUMENTS_NOT_VALID2 = 
            "Arguments not valid : {IN_model_path, OUT_model_path}.";
    public static final String MSG_ARGUMENTS_NOT_VALID1 = "Arguments not valid : {IN_model_path}.";
    public static final String MSG_FILE_NOT_FOUND = " file not found.";
    public static final String MSG_STATE_MACHINE_NOT_DEFINED = "StateMachine not defined";
    
    
    
    public static String createResultFiles( Collection<String> resultFiles, String directoryPathString, String netFileName ) 
            throws IOException {
        
        String resultFileName = "";
        
        for ( Iterator<String> resFile = resultFiles.iterator(); resFile.hasNext(); ) {
            
            // Get the file content
            String resFileString = ( String ) resFile.next();
            
            File resultFile = getResultFile( resFileString, directoryPathString, netFileName );
            
            if ( resultFile != null ) {
                
                writeBufferInResultFile( resultFile, resFileString );
                
                resultFileName = resultFile.toString();
                
                resultFileName = resultFile.getName();
                resultFileName = resultFileName.substring( 0, resultFileName.lastIndexOf( Resources.DOT ) );
            }
        }
        
        return resultFileName;
    }
    
    
    /**
     * Create a write flow (buffer) over the result file, overwriting the content if exists
     * 
     * @param resultFile
     * @param tinaFileString
     * @throws IOException
     */
    private static void writeBufferInResultFile( File resultFile, String tinaFileString )
            throws IOException {
        
        FileOutputStream fileOutputStream = new FileOutputStream( resultFile, false );
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter( fileOutputStream, "utf-8" );
        BufferedWriter resultBuffer = new BufferedWriter( outputStreamWriter );
        
        resultBuffer.write( tinaFileString );
        resultBuffer.close();
    }
    
    
    /**
     * Creates a result file with the name extracted in first line of inFileString
     * @param inFileString
     * @param resultPathString
     *            the directory path where locate the file
     * @return
     *        a File Object for the resultPathString
     */
    private static File getResultFile( String inFileString, String resultPathString, String netFileName ) {
        
        String cleanNetFileName = netFileName.replaceAll( "[^a-zA-Z0-9]+", "_" );
        String resultNetString = "";
        String resultFileString = "";
        String resultFileExtension = "";
        if ( inFileString.startsWith( START_NET_TINA_FILE ) ) {
            
            resultNetString = inFileString.substring( START_NET_TINA_FILE.length(), inFileString.indexOf( "\n" ) );
            resultFileString = resultNetString + SUFFIX_TINA;
            resultFileExtension = EXTENSION_NET;
        }
        
        if ( inFileString.startsWith( START_LTL_FILE )) {
            
            resultNetString = inFileString.substring( START_LTL_FILE.length(), inFileString.indexOf( "\n" ) );
            resultFileString = resultNetString + SUFFIX_TINA;
            resultFileExtension = EXTENSION_LTL;
        }
        
        if ( inFileString.startsWith( START_PRB_FILE )) {
            
            resultNetString = inFileString.substring( START_PRB_FILE.length(), inFileString.indexOf( "\n" ) - END_PRB_FILE.length() );
            resultFileString = resultNetString + SUFFIX_PROD;
            resultFileExtension = EXTENSION_PRB;
        }
        
        if ( inFileString.startsWith( START_NET_PROD_FILE )) {
            
            resultNetString = inFileString.substring( START_NET_PROD_FILE.length(), inFileString.indexOf( "\n" ) - END_NET_PROD_FILE.length() );
            resultFileString = resultNetString +  SUFFIX_PROD;
            resultFileExtension = EXTENSION_NET;
        }
        
        if ( resultNetString.equals( cleanNetFileName ) ) {
            
            // Built the hole path of the destination file
            Path resultPath = new Path( resultPathString  + "/" + resultFileString );
            IPath resultIPath = resultPath.addFileExtension( resultFileExtension );
            
            File resultFile = resultIPath.toFile();
            
            return resultFile;
        }
        
        return null;
    }
    
    
    /**
     * @param event
     * @return
     * @throws ExecutionException
     */
    public static IStructuredSelection getStructuredSelection( ExecutionEvent event )
        throws ExecutionException {
        
        // get workbench window
        IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked( event );
        // set selection service
        ISelectionService service = window.getSelectionService();
        // set structured selection
        IStructuredSelection structured = ( IStructuredSelection ) service.getSelection();
        return structured;
    }
    
    
    /**
     * Tests if eclipse is running.
     * 
     * @return <code>true</code> if eclipse is running
     *
     * @generated
     */
    public static boolean isEclipseRunning() {
        
        try {
            
            return Platform.isRunning();
        } catch ( Throwable exception ) {
            // Assume that we aren't running.
        }
        return false;
    }
}