/**
 */
package com.masdes.misusecasesprofile.impl;

import com.masdes.misusecasesprofile.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MisusecasesProfileFactoryImpl extends EFactoryImpl implements MisusecasesProfileFactory {
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static MisusecasesProfileFactory init() {
        try {
            MisusecasesProfileFactory theMisusecasesProfileFactory = (MisusecasesProfileFactory)EPackage.Registry.INSTANCE.getEFactory(MisusecasesProfilePackage.eNS_URI);
            if (theMisusecasesProfileFactory != null) {
                return theMisusecasesProfileFactory;
            }
        }
        catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new MisusecasesProfileFactoryImpl();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public MisusecasesProfileFactoryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case MisusecasesProfilePackage.MISUSE: return createMisuse();
            case MisusecasesProfilePackage.THREATENS: return createThreatens();
            case MisusecasesProfilePackage.MITIGATES: return createMitigates();
            case MisusecasesProfilePackage.MISUSER: return createMisuser();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Misuse createMisuse() {
        MisuseImpl misuse = new MisuseImpl();
        return misuse;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Threatens createThreatens() {
        ThreatensImpl threatens = new ThreatensImpl();
        return threatens;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Mitigates createMitigates() {
        MitigatesImpl mitigates = new MitigatesImpl();
        return mitigates;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Misuser createMisuser() {
        MisuserImpl misuser = new MisuserImpl();
        return misuser;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public MisusecasesProfilePackage getMisusecasesProfilePackage() {
        return (MisusecasesProfilePackage)getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
    @Deprecated
    public static MisusecasesProfilePackage getPackage() {
        return MisusecasesProfilePackage.eINSTANCE;
    }

} //MisusecasesProfileFactoryImpl
