/**
 */
package com.masdes.misusecasesprofile.impl;

import com.masdes.misusecasesprofile.Misuse;
import com.masdes.misusecasesprofile.MisusecasesProfilePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.uml2.uml.UseCase;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Misuse</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.misusecasesprofile.impl.MisuseImpl#getBase_UseCase <em>Base Use Case</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MisuseImpl extends EObjectImpl implements Misuse {
    /**
     * The cached value of the '{@link #getBase_UseCase() <em>Base Use Case</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getBase_UseCase()
     * @generated
     * @ordered
     */
    protected UseCase base_UseCase;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected MisuseImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return MisusecasesProfilePackage.Literals.MISUSE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public UseCase getBase_UseCase() {
        if (base_UseCase != null && base_UseCase.eIsProxy()) {
            InternalEObject oldBase_UseCase = (InternalEObject)base_UseCase;
            base_UseCase = (UseCase)eResolveProxy(oldBase_UseCase);
            if (base_UseCase != oldBase_UseCase) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, MisusecasesProfilePackage.MISUSE__BASE_USE_CASE, oldBase_UseCase, base_UseCase));
            }
        }
        return base_UseCase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public UseCase basicGetBase_UseCase() {
        return base_UseCase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setBase_UseCase(UseCase newBase_UseCase) {
        UseCase oldBase_UseCase = base_UseCase;
        base_UseCase = newBase_UseCase;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MisusecasesProfilePackage.MISUSE__BASE_USE_CASE, oldBase_UseCase, base_UseCase));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case MisusecasesProfilePackage.MISUSE__BASE_USE_CASE:
                if (resolve) return getBase_UseCase();
                return basicGetBase_UseCase();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case MisusecasesProfilePackage.MISUSE__BASE_USE_CASE:
                setBase_UseCase((UseCase)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case MisusecasesProfilePackage.MISUSE__BASE_USE_CASE:
                setBase_UseCase((UseCase)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case MisusecasesProfilePackage.MISUSE__BASE_USE_CASE:
                return base_UseCase != null;
        }
        return super.eIsSet(featureID);
    }

} //MisuseImpl
