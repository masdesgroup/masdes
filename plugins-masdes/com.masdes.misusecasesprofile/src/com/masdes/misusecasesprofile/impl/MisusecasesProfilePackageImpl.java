/**
 */
package com.masdes.misusecasesprofile.impl;

import com.masdes.misusecasesprofile.Misuse;
import com.masdes.misusecasesprofile.MisusecasesProfileFactory;
import com.masdes.misusecasesprofile.MisusecasesProfilePackage;
import com.masdes.misusecasesprofile.Misuser;
import com.masdes.misusecasesprofile.Mitigates;
import com.masdes.misusecasesprofile.Threatens;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MisusecasesProfilePackageImpl extends EPackageImpl implements MisusecasesProfilePackage {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass misuseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass threatensEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass mitigatesEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass misuserEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see com.masdes.misusecasesprofile.MisusecasesProfilePackage#eNS_URI
     * @see #init()
     * @generated
     */
    private MisusecasesProfilePackageImpl() {
        super(eNS_URI, MisusecasesProfileFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>This method is used to initialize {@link MisusecasesProfilePackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static MisusecasesProfilePackage init() {
        if (isInited) return (MisusecasesProfilePackage)EPackage.Registry.INSTANCE.getEPackage(MisusecasesProfilePackage.eNS_URI);

        // Obtain or create and register package
        MisusecasesProfilePackageImpl theMisusecasesProfilePackage = (MisusecasesProfilePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MisusecasesProfilePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MisusecasesProfilePackageImpl());

        isInited = true;

        // Initialize simple dependencies
        UMLPackage.eINSTANCE.eClass();

        // Create package meta-data objects
        theMisusecasesProfilePackage.createPackageContents();

        // Initialize created meta-data
        theMisusecasesProfilePackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theMisusecasesProfilePackage.freeze();

  
        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(MisusecasesProfilePackage.eNS_URI, theMisusecasesProfilePackage);
        return theMisusecasesProfilePackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getMisuse() {
        return misuseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getMisuse_Base_UseCase() {
        return (EReference)misuseEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getThreatens() {
        return threatensEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getThreatens_Base_Relationship() {
        return (EReference)threatensEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getMitigates() {
        return mitigatesEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getMitigates_Base_Relationship() {
        return (EReference)mitigatesEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getMisuser() {
        return misuserEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getMisuser_Base_Classifier() {
        return (EReference)misuserEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public MisusecasesProfileFactory getMisusecasesProfileFactory() {
        return (MisusecasesProfileFactory)getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        misuseEClass = createEClass(MISUSE);
        createEReference(misuseEClass, MISUSE__BASE_USE_CASE);

        threatensEClass = createEClass(THREATENS);
        createEReference(threatensEClass, THREATENS__BASE_RELATIONSHIP);

        mitigatesEClass = createEClass(MITIGATES);
        createEReference(mitigatesEClass, MITIGATES__BASE_RELATIONSHIP);

        misuserEClass = createEClass(MISUSER);
        createEReference(misuserEClass, MISUSER__BASE_CLASSIFIER);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes

        // Initialize classes and features; add operations and parameters
        initEClass(misuseEClass, Misuse.class, "Misuse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getMisuse_Base_UseCase(), theUMLPackage.getUseCase(), null, "base_UseCase", null, 1, 1, Misuse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(threatensEClass, Threatens.class, "Threatens", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getThreatens_Base_Relationship(), theUMLPackage.getRelationship(), null, "base_Relationship", null, 1, 1, Threatens.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(mitigatesEClass, Mitigates.class, "Mitigates", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getMitigates_Base_Relationship(), theUMLPackage.getRelationship(), null, "base_Relationship", null, 1, 1, Mitigates.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(misuserEClass, Misuser.class, "Misuser", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getMisuser_Base_Classifier(), theUMLPackage.getClassifier(), null, "base_Classifier", null, 1, 1, Misuser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        // Create resource
        createResource(eNS_URI);
    }

} //MisusecasesProfilePackageImpl
