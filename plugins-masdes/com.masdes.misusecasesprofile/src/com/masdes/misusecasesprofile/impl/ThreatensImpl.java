/**
 */
package com.masdes.misusecasesprofile.impl;

import com.masdes.misusecasesprofile.MisusecasesProfilePackage;
import com.masdes.misusecasesprofile.Threatens;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.uml2.uml.Relationship;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Threatens</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.misusecasesprofile.impl.ThreatensImpl#getBase_Relationship <em>Base Relationship</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ThreatensImpl extends EObjectImpl implements Threatens {
    /**
     * The cached value of the '{@link #getBase_Relationship() <em>Base Relationship</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getBase_Relationship()
     * @generated
     * @ordered
     */
    protected Relationship base_Relationship;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ThreatensImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return MisusecasesProfilePackage.Literals.THREATENS;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Relationship getBase_Relationship() {
        if (base_Relationship != null && base_Relationship.eIsProxy()) {
            InternalEObject oldBase_Relationship = (InternalEObject)base_Relationship;
            base_Relationship = (Relationship)eResolveProxy(oldBase_Relationship);
            if (base_Relationship != oldBase_Relationship) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, MisusecasesProfilePackage.THREATENS__BASE_RELATIONSHIP, oldBase_Relationship, base_Relationship));
            }
        }
        return base_Relationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Relationship basicGetBase_Relationship() {
        return base_Relationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setBase_Relationship(Relationship newBase_Relationship) {
        Relationship oldBase_Relationship = base_Relationship;
        base_Relationship = newBase_Relationship;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MisusecasesProfilePackage.THREATENS__BASE_RELATIONSHIP, oldBase_Relationship, base_Relationship));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case MisusecasesProfilePackage.THREATENS__BASE_RELATIONSHIP:
                if (resolve) return getBase_Relationship();
                return basicGetBase_Relationship();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case MisusecasesProfilePackage.THREATENS__BASE_RELATIONSHIP:
                setBase_Relationship((Relationship)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case MisusecasesProfilePackage.THREATENS__BASE_RELATIONSHIP:
                setBase_Relationship((Relationship)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case MisusecasesProfilePackage.THREATENS__BASE_RELATIONSHIP:
                return base_Relationship != null;
        }
        return super.eIsSet(featureID);
    }

} //ThreatensImpl
