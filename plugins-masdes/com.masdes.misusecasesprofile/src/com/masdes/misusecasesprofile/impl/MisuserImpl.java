/**
 */
package com.masdes.misusecasesprofile.impl;

import com.masdes.misusecasesprofile.MisusecasesProfilePackage;
import com.masdes.misusecasesprofile.Misuser;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.uml2.uml.Classifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Misuser</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.misusecasesprofile.impl.MisuserImpl#getBase_Classifier <em>Base Classifier</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MisuserImpl extends EObjectImpl implements Misuser {
    /**
     * The cached value of the '{@link #getBase_Classifier() <em>Base Classifier</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getBase_Classifier()
     * @generated
     * @ordered
     */
    protected Classifier base_Classifier;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected MisuserImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return MisusecasesProfilePackage.Literals.MISUSER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Classifier getBase_Classifier() {
        if (base_Classifier != null && base_Classifier.eIsProxy()) {
            InternalEObject oldBase_Classifier = (InternalEObject)base_Classifier;
            base_Classifier = (Classifier)eResolveProxy(oldBase_Classifier);
            if (base_Classifier != oldBase_Classifier) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, MisusecasesProfilePackage.MISUSER__BASE_CLASSIFIER, oldBase_Classifier, base_Classifier));
            }
        }
        return base_Classifier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Classifier basicGetBase_Classifier() {
        return base_Classifier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setBase_Classifier(Classifier newBase_Classifier) {
        Classifier oldBase_Classifier = base_Classifier;
        base_Classifier = newBase_Classifier;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MisusecasesProfilePackage.MISUSER__BASE_CLASSIFIER, oldBase_Classifier, base_Classifier));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case MisusecasesProfilePackage.MISUSER__BASE_CLASSIFIER:
                if (resolve) return getBase_Classifier();
                return basicGetBase_Classifier();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case MisusecasesProfilePackage.MISUSER__BASE_CLASSIFIER:
                setBase_Classifier((Classifier)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case MisusecasesProfilePackage.MISUSER__BASE_CLASSIFIER:
                setBase_Classifier((Classifier)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case MisusecasesProfilePackage.MISUSER__BASE_CLASSIFIER:
                return base_Classifier != null;
        }
        return super.eIsSet(featureID);
    }

} //MisuserImpl
