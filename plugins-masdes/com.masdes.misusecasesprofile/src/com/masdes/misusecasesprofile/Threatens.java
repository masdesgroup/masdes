/**
 */
package com.masdes.misusecasesprofile;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Relationship;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Threatens</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.misusecasesprofile.Threatens#getBase_Relationship <em>Base Relationship</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.misusecasesprofile.MisusecasesProfilePackage#getThreatens()
 * @model
 * @generated
 */
public interface Threatens extends EObject {
    /**
     * Returns the value of the '<em><b>Base Relationship</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Relationship</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Base Relationship</em>' reference.
     * @see #setBase_Relationship(Relationship)
     * @see com.masdes.misusecasesprofile.MisusecasesProfilePackage#getThreatens_Base_Relationship()
     * @model required="true" ordered="false"
     * @generated
     */
    Relationship getBase_Relationship();

    /**
     * Sets the value of the '{@link com.masdes.misusecasesprofile.Threatens#getBase_Relationship <em>Base Relationship</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Base Relationship</em>' reference.
     * @see #getBase_Relationship()
     * @generated
     */
    void setBase_Relationship(Relationship value);

} // Threatens
