/**
 */
package com.masdes.misusecasesprofile;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.UseCase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Misuse</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.misusecasesprofile.Misuse#getBase_UseCase <em>Base Use Case</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.misusecasesprofile.MisusecasesProfilePackage#getMisuse()
 * @model
 * @generated
 */
public interface Misuse extends EObject {
    /**
     * Returns the value of the '<em><b>Base Use Case</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Use Case</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Base Use Case</em>' reference.
     * @see #setBase_UseCase(UseCase)
     * @see com.masdes.misusecasesprofile.MisusecasesProfilePackage#getMisuse_Base_UseCase()
     * @model required="true" ordered="false"
     * @generated
     */
    UseCase getBase_UseCase();

    /**
     * Sets the value of the '{@link com.masdes.misusecasesprofile.Misuse#getBase_UseCase <em>Base Use Case</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Base Use Case</em>' reference.
     * @see #getBase_UseCase()
     * @generated
     */
    void setBase_UseCase(UseCase value);

} // Misuse
