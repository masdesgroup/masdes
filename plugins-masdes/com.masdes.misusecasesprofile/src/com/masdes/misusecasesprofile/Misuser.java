/**
 */
package com.masdes.misusecasesprofile;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Classifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Misuser</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.misusecasesprofile.Misuser#getBase_Classifier <em>Base Classifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.misusecasesprofile.MisusecasesProfilePackage#getMisuser()
 * @model
 * @generated
 */
public interface Misuser extends EObject {
    /**
     * Returns the value of the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Classifier</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Base Classifier</em>' reference.
     * @see #setBase_Classifier(Classifier)
     * @see com.masdes.misusecasesprofile.MisusecasesProfilePackage#getMisuser_Base_Classifier()
     * @model required="true" ordered="false"
     * @generated
     */
    Classifier getBase_Classifier();

    /**
     * Sets the value of the '{@link com.masdes.misusecasesprofile.Misuser#getBase_Classifier <em>Base Classifier</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Base Classifier</em>' reference.
     * @see #getBase_Classifier()
     * @generated
     */
    void setBase_Classifier(Classifier value);

} // Misuser
