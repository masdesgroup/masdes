/**
 */
package com.masdes.misusecasesprofile;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.masdes.misusecasesprofile.MisusecasesProfilePackage
 * @generated
 */
public interface MisusecasesProfileFactory extends EFactory {
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    MisusecasesProfileFactory eINSTANCE = com.masdes.misusecasesprofile.impl.MisusecasesProfileFactoryImpl.init();

    /**
     * Returns a new object of class '<em>Misuse</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Misuse</em>'.
     * @generated
     */
    Misuse createMisuse();

    /**
     * Returns a new object of class '<em>Threatens</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Threatens</em>'.
     * @generated
     */
    Threatens createThreatens();

    /**
     * Returns a new object of class '<em>Mitigates</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Mitigates</em>'.
     * @generated
     */
    Mitigates createMitigates();

    /**
     * Returns a new object of class '<em>Misuser</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Misuser</em>'.
     * @generated
     */
    Misuser createMisuser();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
    MisusecasesProfilePackage getMisusecasesProfilePackage();

} //MisusecasesProfileFactory
