/**
 */
package com.masdes.misusecasesprofile;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.masdes.misusecasesprofile.MisusecasesProfileFactory
 * @model kind="package"
 * @generated
 */
public interface MisusecasesProfilePackage extends EPackage {
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "misusecasesprofile";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "http://com.masdes.misusecasesprofile/MisusecasesProfile/1.0";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "MisusecasesProfile";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    MisusecasesProfilePackage eINSTANCE = com.masdes.misusecasesprofile.impl.MisusecasesProfilePackageImpl.init();

    /**
     * The meta object id for the '{@link com.masdes.misusecasesprofile.impl.MisuseImpl <em>Misuse</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.misusecasesprofile.impl.MisuseImpl
     * @see com.masdes.misusecasesprofile.impl.MisusecasesProfilePackageImpl#getMisuse()
     * @generated
     */
    int MISUSE = 0;

    /**
     * The feature id for the '<em><b>Base Use Case</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int MISUSE__BASE_USE_CASE = 0;

    /**
     * The number of structural features of the '<em>Misuse</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int MISUSE_FEATURE_COUNT = 1;

    /**
     * The meta object id for the '{@link com.masdes.misusecasesprofile.impl.ThreatensImpl <em>Threatens</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.misusecasesprofile.impl.ThreatensImpl
     * @see com.masdes.misusecasesprofile.impl.MisusecasesProfilePackageImpl#getThreatens()
     * @generated
     */
    int THREATENS = 1;

    /**
     * The feature id for the '<em><b>Base Relationship</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int THREATENS__BASE_RELATIONSHIP = 0;

    /**
     * The number of structural features of the '<em>Threatens</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int THREATENS_FEATURE_COUNT = 1;

    /**
     * The meta object id for the '{@link com.masdes.misusecasesprofile.impl.MitigatesImpl <em>Mitigates</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.misusecasesprofile.impl.MitigatesImpl
     * @see com.masdes.misusecasesprofile.impl.MisusecasesProfilePackageImpl#getMitigates()
     * @generated
     */
    int MITIGATES = 2;

    /**
     * The feature id for the '<em><b>Base Relationship</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int MITIGATES__BASE_RELATIONSHIP = 0;

    /**
     * The number of structural features of the '<em>Mitigates</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int MITIGATES_FEATURE_COUNT = 1;

    /**
     * The meta object id for the '{@link com.masdes.misusecasesprofile.impl.MisuserImpl <em>Misuser</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.misusecasesprofile.impl.MisuserImpl
     * @see com.masdes.misusecasesprofile.impl.MisusecasesProfilePackageImpl#getMisuser()
     * @generated
     */
    int MISUSER = 3;

    /**
     * The feature id for the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int MISUSER__BASE_CLASSIFIER = 0;

    /**
     * The number of structural features of the '<em>Misuser</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int MISUSER_FEATURE_COUNT = 1;


    /**
     * Returns the meta object for class '{@link com.masdes.misusecasesprofile.Misuse <em>Misuse</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Misuse</em>'.
     * @see com.masdes.misusecasesprofile.Misuse
     * @generated
     */
    EClass getMisuse();

    /**
     * Returns the meta object for the reference '{@link com.masdes.misusecasesprofile.Misuse#getBase_UseCase <em>Base Use Case</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Base Use Case</em>'.
     * @see com.masdes.misusecasesprofile.Misuse#getBase_UseCase()
     * @see #getMisuse()
     * @generated
     */
    EReference getMisuse_Base_UseCase();

    /**
     * Returns the meta object for class '{@link com.masdes.misusecasesprofile.Threatens <em>Threatens</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Threatens</em>'.
     * @see com.masdes.misusecasesprofile.Threatens
     * @generated
     */
    EClass getThreatens();

    /**
     * Returns the meta object for the reference '{@link com.masdes.misusecasesprofile.Threatens#getBase_Relationship <em>Base Relationship</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Base Relationship</em>'.
     * @see com.masdes.misusecasesprofile.Threatens#getBase_Relationship()
     * @see #getThreatens()
     * @generated
     */
    EReference getThreatens_Base_Relationship();

    /**
     * Returns the meta object for class '{@link com.masdes.misusecasesprofile.Mitigates <em>Mitigates</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Mitigates</em>'.
     * @see com.masdes.misusecasesprofile.Mitigates
     * @generated
     */
    EClass getMitigates();

    /**
     * Returns the meta object for the reference '{@link com.masdes.misusecasesprofile.Mitigates#getBase_Relationship <em>Base Relationship</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Base Relationship</em>'.
     * @see com.masdes.misusecasesprofile.Mitigates#getBase_Relationship()
     * @see #getMitigates()
     * @generated
     */
    EReference getMitigates_Base_Relationship();

    /**
     * Returns the meta object for class '{@link com.masdes.misusecasesprofile.Misuser <em>Misuser</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Misuser</em>'.
     * @see com.masdes.misusecasesprofile.Misuser
     * @generated
     */
    EClass getMisuser();

    /**
     * Returns the meta object for the reference '{@link com.masdes.misusecasesprofile.Misuser#getBase_Classifier <em>Base Classifier</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Base Classifier</em>'.
     * @see com.masdes.misusecasesprofile.Misuser#getBase_Classifier()
     * @see #getMisuser()
     * @generated
     */
    EReference getMisuser_Base_Classifier();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    MisusecasesProfileFactory getMisusecasesProfileFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link com.masdes.misusecasesprofile.impl.MisuseImpl <em>Misuse</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.misusecasesprofile.impl.MisuseImpl
         * @see com.masdes.misusecasesprofile.impl.MisusecasesProfilePackageImpl#getMisuse()
         * @generated
         */
        EClass MISUSE = eINSTANCE.getMisuse();

        /**
         * The meta object literal for the '<em><b>Base Use Case</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference MISUSE__BASE_USE_CASE = eINSTANCE.getMisuse_Base_UseCase();

        /**
         * The meta object literal for the '{@link com.masdes.misusecasesprofile.impl.ThreatensImpl <em>Threatens</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.misusecasesprofile.impl.ThreatensImpl
         * @see com.masdes.misusecasesprofile.impl.MisusecasesProfilePackageImpl#getThreatens()
         * @generated
         */
        EClass THREATENS = eINSTANCE.getThreatens();

        /**
         * The meta object literal for the '<em><b>Base Relationship</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference THREATENS__BASE_RELATIONSHIP = eINSTANCE.getThreatens_Base_Relationship();

        /**
         * The meta object literal for the '{@link com.masdes.misusecasesprofile.impl.MitigatesImpl <em>Mitigates</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.misusecasesprofile.impl.MitigatesImpl
         * @see com.masdes.misusecasesprofile.impl.MisusecasesProfilePackageImpl#getMitigates()
         * @generated
         */
        EClass MITIGATES = eINSTANCE.getMitigates();

        /**
         * The meta object literal for the '<em><b>Base Relationship</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference MITIGATES__BASE_RELATIONSHIP = eINSTANCE.getMitigates_Base_Relationship();

        /**
         * The meta object literal for the '{@link com.masdes.misusecasesprofile.impl.MisuserImpl <em>Misuser</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.misusecasesprofile.impl.MisuserImpl
         * @see com.masdes.misusecasesprofile.impl.MisusecasesProfilePackageImpl#getMisuser()
         * @generated
         */
        EClass MISUSER = eINSTANCE.getMisuser();

        /**
         * The meta object literal for the '<em><b>Base Classifier</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference MISUSER__BASE_CLASSIFIER = eINSTANCE.getMisuser_Base_Classifier();

    }

} //MisusecasesProfilePackage
