package com.masdes.transformations.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.masdes.ptnet2prod.handlers.HandlerPtnet2prod;
import com.masdes.resources.ConsoleDisplayManager;
import com.masdes.resources.Resources;
import com.masdes.sm2ptnet.handlers.HandlerSm2ptnet;


/**
 * @author maria berenguer
 *
 */
public class HandlerSm2prod extends AbstractHandler {
    
    private String stateMachineName;
    
    protected void setStateMachineName( String stateMachineName ) {
        
        this.stateMachineName = stateMachineName;
    }
    
    protected String getStateMachineName() {
        
        return stateMachineName;
    }


    public HandlerSm2prod( String stateMachineName ) {
        
        setStateMachineName( stateMachineName );
    }
    
    
    /* (non-Javadoc)
     * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute( ExecutionEvent event ) 
            throws ExecutionException {
        
        ConsoleDisplayManager console = ConsoleDisplayManager.getDefault( Resources.CONSOLE_TITLE );
        console.clear();
        
        HandlerSm2ptnet handlerSm2ptnet = new HandlerSm2ptnet();
        Path resultPtnetFilePath = ( Path ) handlerSm2ptnet.execute( event );
        
        if ( getStateMachineName().equals( "" ) ) {
            
            throw new ExecutionException( "StateMachine not defined" );
        }
        HandlerPtnet2prod handlerPtnet2prod = new HandlerPtnet2prod( resultPtnetFilePath, getStateMachineName() );
        String netFileName = ( String ) handlerPtnet2prod.execute( event );
        
        return netFileName;
    }
    
    public Object executeHandler( ExecutionEvent event, Boolean useConsole, IStructuredSelection structuredSelection )
            throws ExecutionException {
        
        if ( useConsole ) {
            
            return execute( event );
        } else {
            HandlerSm2ptnet handlerSm2ptnet = new HandlerSm2ptnet();
            Path resultPtnetFilePath = ( Path ) handlerSm2ptnet.executeHandler( event, useConsole, structuredSelection );
            String netFileName = resultPtnetFilePath.toString();
            if ( getStateMachineName().equals( "" ) ) {
                
                throw new ExecutionException( "StateMachine not defined" );
            }
            HandlerPtnet2prod handlerPtnet2prod = new HandlerPtnet2prod( resultPtnetFilePath, getStateMachineName() );
            netFileName = ( String ) handlerPtnet2prod.executeHandler( event, useConsole, structuredSelection );
            
            return netFileName;
        }
        
    }

}
