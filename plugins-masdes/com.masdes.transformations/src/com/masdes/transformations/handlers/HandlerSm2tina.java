package com.masdes.transformations.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.masdes.ptnet2tina.handlers.HandlerPtnet2tina;
import com.masdes.resources.ConsoleDisplayManager;
import com.masdes.resources.Resources;
import com.masdes.sm2ptnet.handlers.HandlerSm2ptnet;


/**
 * @author maria berenguer
 *
 */
public class HandlerSm2tina extends AbstractHandler {

    private String stateMachineName;
    
    protected String getStateMachineName() {
        
        return stateMachineName;
    }
    
    protected void setStateMachineName( String stateMachineName ) {
        
        this.stateMachineName = stateMachineName;
    }
    
    
    public HandlerSm2tina( String stateMachineName ) {
        
        setStateMachineName( stateMachineName );
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute( ExecutionEvent event )
            throws ExecutionException {
        
        ConsoleDisplayManager console = ConsoleDisplayManager.getDefault( Resources.CONSOLE_TITLE );
        console.clear();
        
        HandlerSm2ptnet handlerSm2ptnet = new HandlerSm2ptnet();
        Path resultPtnetFilePath = ( Path ) handlerSm2ptnet.execute( event );
        
        if ( getStateMachineName().equals( "" ) ) {
            
            throw new ExecutionException( "StateMachine not defined" );
        }
        HandlerPtnet2tina handlerPtnet2tina = new HandlerPtnet2tina( resultPtnetFilePath, getStateMachineName() );
        String netFileName = ( String ) handlerPtnet2tina.execute( event );
        
        return netFileName;
    }
    
    
    public Object executeHandler( ExecutionEvent event, Boolean useConsole, IStructuredSelection structuredSelection )
            throws ExecutionException {
        
        if ( useConsole ) {
            
            return execute( event );
        } else {
            HandlerSm2ptnet handlerSm2ptnet = new HandlerSm2ptnet();
            Path resultPtnetFilePath = ( Path ) handlerSm2ptnet.executeHandler( event, useConsole, structuredSelection );
            String netFileName = resultPtnetFilePath.toString();
            if ( getStateMachineName().equals( "" ) ) {
                
                throw new ExecutionException( "StateMachine not defined" );
            }
            HandlerPtnet2tina handlerPtnet2tina = new HandlerPtnet2tina( resultPtnetFilePath, getStateMachineName() );
            netFileName = ( String ) handlerPtnet2tina.executeHandler( event, useConsole, structuredSelection );
            
            return netFileName;
        }
    }

}
