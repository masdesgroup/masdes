/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NFP Common Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getExpr <em>Expr</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getSource <em>Source</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getStatQ <em>Stat Q</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getDir <em>Dir</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getMode <em>Mode</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_CommonType()
 * @model
 * @generated
 */
public interface NFP_CommonType extends EObject {
    /**
     * Returns the value of the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Expr</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Expr</em>' attribute.
     * @see #setExpr(String)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_CommonType_Expr()
     * @model unique="false" dataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.VSL_Expression" ordered="false"
     * @generated
     */
    String getExpr();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getExpr <em>Expr</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Expr</em>' attribute.
     * @see #getExpr()
     * @generated
     */
    void setExpr(String value);

    /**
     * Returns the value of the '<em><b>Source</b></em>' attribute.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.SourceKind}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Source</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Source</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.SourceKind
     * @see #setSource(SourceKind)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_CommonType_Source()
     * @model unique="false" ordered="false"
     * @generated
     */
    SourceKind getSource();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getSource <em>Source</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Source</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.SourceKind
     * @see #getSource()
     * @generated
     */
    void setSource(SourceKind value);

    /**
     * Returns the value of the '<em><b>Stat Q</b></em>' attribute.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Stat Q</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Stat Q</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind
     * @see #setStatQ(StatisticalQualifierKind)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_CommonType_StatQ()
     * @model ordered="false"
     * @generated
     */
    StatisticalQualifierKind getStatQ();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getStatQ <em>Stat Q</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Stat Q</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind
     * @see #getStatQ()
     * @generated
     */
    void setStatQ(StatisticalQualifierKind value);

    /**
     * Returns the value of the '<em><b>Dir</b></em>' attribute.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.DirectionKind}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Dir</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Dir</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.DirectionKind
     * @see #setDir(DirectionKind)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_CommonType_Dir()
     * @model ordered="false"
     * @generated
     */
    DirectionKind getDir();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getDir <em>Dir</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Dir</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.DirectionKind
     * @see #getDir()
     * @generated
     */
    void setDir(DirectionKind value);

    /**
     * Returns the value of the '<em><b>Mode</b></em>' attribute list.
     * The list contents are of type {@link java.lang.String}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Mode</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Mode</em>' attribute list.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_CommonType_Mode()
     * @model unique="false" dataType="org.eclipse.uml2.types.String" ordered="false"
     * @generated
     */
    EList<String> getMode();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model probDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" probRequired="true" probOrdered="false"
     * @generated
     */
    void bernoulli(String prob);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model probDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" probRequired="true" probOrdered="false" trialsDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Integer" trialsRequired="true" trialsOrdered="false"
     * @generated
     */
    void binomial(String prob, int trials);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model meanDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" meanRequired="true" meanOrdered="false"
     * @generated
     */
    void exp(String mean);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model kDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Integer" kRequired="true" kOrdered="false" meanDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" meanRequired="true" meanOrdered="false"
     * @generated
     */
    void gamma(int k, String mean);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model meanDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" meanRequired="true" meanOrdered="false" standDevDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" standDevRequired="true" standDevOrdered="false"
     * @generated
     */
    void normal(String mean, String standDev);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model meanDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" meanRequired="true" meanOrdered="false"
     * @generated
     */
    void poisson(String mean);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model minDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" minRequired="true" minOrdered="false" maxDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" maxRequired="true" maxOrdered="false"
     * @generated
     */
    void uniform(String min, String max);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model pDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" pRequired="true" pOrdered="false"
     * @generated
     */
    void geometric(String p);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model minDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" minRequired="true" minOrdered="false" maxDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" maxRequired="true" maxOrdered="false" modeDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" modeRequired="true" modeOrdered="false"
     * @generated
     */
    void triangular(String min, String max, String mode);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model thetaDataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" thetaRequired="true" thetaOrdered="false"
     * @generated
     */
    void logarithmic(String theta);

} // NFP_CommonType
