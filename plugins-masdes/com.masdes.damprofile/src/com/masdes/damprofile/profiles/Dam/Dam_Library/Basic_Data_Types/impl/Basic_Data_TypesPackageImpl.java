/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl;

import com.masdes.damprofile.profiles.Core.CorePackage;

import com.masdes.damprofile.profiles.Core.impl.CorePackageImpl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Basic_DA_Enumeration_TypesPackage;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.impl.Basic_DA_Enumeration_TypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesFactory;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLikelihood;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.impl.Complex_Data_TypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.impl.MARTE_DataTypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.impl.MARTE_PrimitivesTypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl;

import com.masdes.damprofile.profiles.Maintenance.MaintenancePackage;

import com.masdes.damprofile.profiles.Maintenance.impl.MaintenancePackageImpl;

import com.masdes.damprofile.profiles.Redundancy.RedundancyPackage;

import com.masdes.damprofile.profiles.Redundancy.impl.RedundancyPackageImpl;

import com.masdes.damprofile.profiles.Threats.ThreatsPackage;

import com.masdes.damprofile.profiles.Threats.impl.ThreatsPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.uml2.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Basic_Data_TypesPackageImpl extends EPackageImpl implements Basic_Data_TypesPackage {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass daLevelEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass daCriticalLevelEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass daCurrencyEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass daLikelihoodEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass daFrequencyEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private Basic_Data_TypesPackageImpl() {
        super(eNS_URI, Basic_Data_TypesFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>This method is used to initialize {@link Basic_Data_TypesPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static Basic_Data_TypesPackage init() {
        if (isInited) return (Basic_Data_TypesPackage)EPackage.Registry.INSTANCE.getEPackage(Basic_Data_TypesPackage.eNS_URI);

        // Obtain or create and register package
        Basic_Data_TypesPackageImpl theBasic_Data_TypesPackage = (Basic_Data_TypesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Basic_Data_TypesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Basic_Data_TypesPackageImpl());

        isInited = true;

        // Initialize simple dependencies
        MARTEPackage.eINSTANCE.eClass();
        MeasurementUnitsPackage.eINSTANCE.eClass();
        GRM_BasicTypesPackage.eINSTANCE.eClass();
        MARTE_DataTypesPackage.eINSTANCE.eClass();
        BasicNFP_TypesPackage.eINSTANCE.eClass();
        TimeTypesLibraryPackage.eINSTANCE.eClass();
        TimeLibraryPackage.eINSTANCE.eClass();
        RS_LibraryPackage.eINSTANCE.eClass();
        MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        Basic_DA_Enumeration_TypesPackageImpl theBasic_DA_Enumeration_TypesPackage = (Basic_DA_Enumeration_TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Basic_DA_Enumeration_TypesPackage.eNS_URI) instanceof Basic_DA_Enumeration_TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Basic_DA_Enumeration_TypesPackage.eNS_URI) : Basic_DA_Enumeration_TypesPackage.eINSTANCE);
        Complex_Data_TypesPackageImpl theComplex_Data_TypesPackage = (Complex_Data_TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Complex_Data_TypesPackage.eNS_URI) instanceof Complex_Data_TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Complex_Data_TypesPackage.eNS_URI) : Complex_Data_TypesPackage.eINSTANCE);
        MARTE_PrimitivesTypesPackageImpl theMARTE_PrimitivesTypesPackage_1 = (MARTE_PrimitivesTypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage.eNS_URI) instanceof MARTE_PrimitivesTypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage.eNS_URI) : com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage.eINSTANCE);
        MARTE_DataTypesPackageImpl theMARTE_DataTypesPackage_1 = (MARTE_DataTypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage.eNS_URI) instanceof MARTE_DataTypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage.eNS_URI) : com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage.eINSTANCE);
        MeasurementUnitsPackageImpl theMeasurementUnitsPackage_1 = (MeasurementUnitsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage.eNS_URI) instanceof MeasurementUnitsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage.eNS_URI) : com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage.eINSTANCE);
        BasicNFP_TypesPackageImpl theBasicNFP_TypesPackage_1 = (BasicNFP_TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage.eNS_URI) instanceof BasicNFP_TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage.eNS_URI) : com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage.eINSTANCE);
        CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
        RedundancyPackageImpl theRedundancyPackage = (RedundancyPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RedundancyPackage.eNS_URI) instanceof RedundancyPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RedundancyPackage.eNS_URI) : RedundancyPackage.eINSTANCE);
        ThreatsPackageImpl theThreatsPackage = (ThreatsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ThreatsPackage.eNS_URI) instanceof ThreatsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ThreatsPackage.eNS_URI) : ThreatsPackage.eINSTANCE);
        MaintenancePackageImpl theMaintenancePackage = (MaintenancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MaintenancePackage.eNS_URI) instanceof MaintenancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MaintenancePackage.eNS_URI) : MaintenancePackage.eINSTANCE);

        // Create package meta-data objects
        theBasic_Data_TypesPackage.createPackageContents();
        theBasic_DA_Enumeration_TypesPackage.createPackageContents();
        theComplex_Data_TypesPackage.createPackageContents();
        theMARTE_PrimitivesTypesPackage_1.createPackageContents();
        theMARTE_DataTypesPackage_1.createPackageContents();
        theMeasurementUnitsPackage_1.createPackageContents();
        theBasicNFP_TypesPackage_1.createPackageContents();
        theCorePackage.createPackageContents();
        theRedundancyPackage.createPackageContents();
        theThreatsPackage.createPackageContents();
        theMaintenancePackage.createPackageContents();

        // Initialize created meta-data
        theBasic_Data_TypesPackage.initializePackageContents();
        theBasic_DA_Enumeration_TypesPackage.initializePackageContents();
        theComplex_Data_TypesPackage.initializePackageContents();
        theMARTE_PrimitivesTypesPackage_1.initializePackageContents();
        theMARTE_DataTypesPackage_1.initializePackageContents();
        theMeasurementUnitsPackage_1.initializePackageContents();
        theBasicNFP_TypesPackage_1.initializePackageContents();
        theCorePackage.initializePackageContents();
        theRedundancyPackage.initializePackageContents();
        theThreatsPackage.initializePackageContents();
        theMaintenancePackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theBasic_Data_TypesPackage.freeze();

  
        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(Basic_Data_TypesPackage.eNS_URI, theBasic_Data_TypesPackage);
        return theBasic_Data_TypesPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getDaLevel() {
        return daLevelEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getDaLevel_Value() {
        return (EAttribute)daLevelEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getDaCriticalLevel() {
        return daCriticalLevelEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getDaCriticalLevel_Value() {
        return (EAttribute)daCriticalLevelEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getDaCurrency() {
        return daCurrencyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getDaCurrency_Value() {
        return (EAttribute)daCurrencyEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getDaLikelihood() {
        return daLikelihoodEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getDaLikelihood_Value() {
        return (EAttribute)daLikelihoodEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getDaFrequency() {
        return daFrequencyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getDaFrequency_Unit() {
        return (EAttribute)daFrequencyEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getDaFrequency_Precision() {
        return (EAttribute)daFrequencyEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Basic_Data_TypesFactory getBasic_Data_TypesFactory() {
        return (Basic_Data_TypesFactory)getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        daLevelEClass = createEClass(DA_LEVEL);
        createEAttribute(daLevelEClass, DA_LEVEL__VALUE);

        daCriticalLevelEClass = createEClass(DA_CRITICAL_LEVEL);
        createEAttribute(daCriticalLevelEClass, DA_CRITICAL_LEVEL__VALUE);

        daCurrencyEClass = createEClass(DA_CURRENCY);
        createEAttribute(daCurrencyEClass, DA_CURRENCY__VALUE);

        daLikelihoodEClass = createEClass(DA_LIKELIHOOD);
        createEAttribute(daLikelihoodEClass, DA_LIKELIHOOD__VALUE);

        daFrequencyEClass = createEClass(DA_FREQUENCY);
        createEAttribute(daFrequencyEClass, DA_FREQUENCY__UNIT);
        createEAttribute(daFrequencyEClass, DA_FREQUENCY__PRECISION);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage theBasicNFP_TypesPackage_1 = (com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage)EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage.eNS_URI);
        Basic_DA_Enumeration_TypesPackage theBasic_DA_Enumeration_TypesPackage = (Basic_DA_Enumeration_TypesPackage)EPackage.Registry.INSTANCE.getEPackage(Basic_DA_Enumeration_TypesPackage.eNS_URI);
        TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        daLevelEClass.getESuperTypes().add(theBasicNFP_TypesPackage_1.getNFP_CommonType());
        daCriticalLevelEClass.getESuperTypes().add(theBasicNFP_TypesPackage_1.getNFP_CommonType());
        daCurrencyEClass.getESuperTypes().add(theBasicNFP_TypesPackage_1.getNFP_CommonType());
        daLikelihoodEClass.getESuperTypes().add(theBasicNFP_TypesPackage_1.getNFP_CommonType());
        daFrequencyEClass.getESuperTypes().add(theBasicNFP_TypesPackage_1.getNFP_Real());

        // Initialize classes and features; add operations and parameters
        initEClass(daLevelEClass, DaLevel.class, "DaLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDaLevel_Value(), theBasic_DA_Enumeration_TypesPackage.getLevel(), "value", "veryHigh", 1, 1, DaLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(daCriticalLevelEClass, DaCriticalLevel.class, "DaCriticalLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDaCriticalLevel_Value(), theBasic_DA_Enumeration_TypesPackage.getCriticalLevel(), "value", "minor", 1, 1, DaCriticalLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(daCurrencyEClass, DaCurrency.class, "DaCurrency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDaCurrency_Value(), theBasic_DA_Enumeration_TypesPackage.getDaCurrKind(), "value", "euro", 1, 1, DaCurrency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(daLikelihoodEClass, DaLikelihood.class, "DaLikelihood", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDaLikelihood_Value(), theBasic_DA_Enumeration_TypesPackage.getLikelihood(), "value", "frequent", 1, 1, DaLikelihood.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(daFrequencyEClass, DaFrequency.class, "DaFrequency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDaFrequency_Unit(), theBasic_DA_Enumeration_TypesPackage.getDaFrequencyUnitKind(), "unit", "ft_s", 1, 1, DaFrequency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
        initEAttribute(getDaFrequency_Precision(), theTypesPackage.getReal(), "precision", null, 1, 1, DaFrequency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        // Create resource
        createResource(eNS_URI);
    }

} //Basic_Data_TypesPackageImpl
