/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.DaFrequencyUnitKind;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Frequency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency#getUnit <em>Unit</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency#getPrecision <em>Precision</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage#getDaFrequency()
 * @model
 * @generated
 */
public interface DaFrequency extends NFP_Real {
    /**
     * Returns the value of the '<em><b>Unit</b></em>' attribute.
     * The default value is <code>"ft_s"</code>.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.DaFrequencyUnitKind}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Unit</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Unit</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.DaFrequencyUnitKind
     * @see #setUnit(DaFrequencyUnitKind)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage#getDaFrequency_Unit()
     * @model default="ft_s" required="true" ordered="false"
     * @generated
     */
    DaFrequencyUnitKind getUnit();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency#getUnit <em>Unit</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Unit</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.DaFrequencyUnitKind
     * @see #getUnit()
     * @generated
     */
    void setUnit(DaFrequencyUnitKind value);

    /**
     * Returns the value of the '<em><b>Precision</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Precision</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Precision</em>' attribute.
     * @see #setPrecision(double)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage#getDaFrequency_Precision()
     * @model dataType="org.eclipse.uml2.types.Real" required="true" ordered="false"
     * @generated
     */
    double getPrecision();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency#getPrecision <em>Precision</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Precision</em>' attribute.
     * @see #getPrecision()
     * @generated
     */
    void setPrecision(double value);

} // DaFrequency
