/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Capability;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Intent;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Objective;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Origin;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Persistency;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.PhaseCreation;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.PhenomCause;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.SysBoundaries;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Fault</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getOccurrenceRate <em>Occurrence Rate</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getLatency <em>Latency</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getOccurrenceProb <em>Occurrence Prob</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getOccurrenceDist <em>Occurrence Dist</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getDuration <em>Duration</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getEffectE <em>Effect E</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getDimension <em>Dimension</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getPersistency <em>Persistency</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getPhaseCreation <em>Phase Creation</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getSysBoundaries <em>Sys Boundaries</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getPhenomCause <em>Phenom Cause</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getObjective <em>Objective</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getIntent <em>Intent</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getCapability <em>Capability</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getEffectF <em>Effect F</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getEffectH <em>Effect H</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault()
 * @model
 * @generated
 */
public interface DaFault extends EObject {
    /**
     * Returns the value of the '<em><b>Occurrence Rate</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Occurrence Rate</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Occurrence Rate</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_OccurrenceRate()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaFrequency> getOccurrenceRate();

    /**
     * Returns the value of the '<em><b>Latency</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Latency</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Latency</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_Latency()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Duration> getLatency();

    /**
     * Returns the value of the '<em><b>Occurrence Prob</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Occurrence Prob</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Occurrence Prob</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_OccurrenceProb()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getOccurrenceProb();

    /**
     * Returns the value of the '<em><b>Occurrence Dist</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Occurrence Dist</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Occurrence Dist</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_OccurrenceDist()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_CommonType> getOccurrenceDist();

    /**
     * Returns the value of the '<em><b>Duration</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Duration</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Duration</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_Duration()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Duration> getDuration();

    /**
     * Returns the value of the '<em><b>Effect E</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaError}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Effect E</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Effect E</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_EffectE()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaError> getEffectE();

    /**
     * Returns the value of the '<em><b>Dimension</b></em>' attribute list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Origin}.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Origin}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Dimension</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Dimension</em>' attribute list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Origin
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_Dimension()
     * @model default="hw" ordered="false"
     * @generated
     */
    EList<Origin> getDimension();

    /**
     * Returns the value of the '<em><b>Persistency</b></em>' attribute.
     * The default value is <code>"transient"</code>.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Persistency}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Persistency</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Persistency</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Persistency
     * @see #setPersistency(Persistency)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_Persistency()
     * @model default="transient" unique="false" ordered="false"
     * @generated
     */
    Persistency getPersistency();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault#getPersistency <em>Persistency</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Persistency</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Persistency
     * @see #getPersistency()
     * @generated
     */
    void setPersistency(Persistency value);

    /**
     * Returns the value of the '<em><b>Phase Creation</b></em>' attribute list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.PhaseCreation}.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.PhaseCreation}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Phase Creation</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Phase Creation</em>' attribute list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.PhaseCreation
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_PhaseCreation()
     * @model default="development" ordered="false"
     * @generated
     */
    EList<PhaseCreation> getPhaseCreation();

    /**
     * Returns the value of the '<em><b>Sys Boundaries</b></em>' attribute list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.SysBoundaries}.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.SysBoundaries}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Sys Boundaries</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Sys Boundaries</em>' attribute list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.SysBoundaries
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_SysBoundaries()
     * @model default="internal" ordered="false"
     * @generated
     */
    EList<SysBoundaries> getSysBoundaries();

    /**
     * Returns the value of the '<em><b>Phenom Cause</b></em>' attribute list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.PhenomCause}.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.PhenomCause}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Phenom Cause</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Phenom Cause</em>' attribute list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.PhenomCause
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_PhenomCause()
     * @model default="natural" ordered="false"
     * @generated
     */
    EList<PhenomCause> getPhenomCause();

    /**
     * Returns the value of the '<em><b>Objective</b></em>' attribute list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Objective}.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Objective}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Objective</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Objective</em>' attribute list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Objective
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_Objective()
     * @model default="malicious" ordered="false"
     * @generated
     */
    EList<Objective> getObjective();

    /**
     * Returns the value of the '<em><b>Intent</b></em>' attribute list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Intent}.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Intent}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Intent</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Intent</em>' attribute list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Intent
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_Intent()
     * @model default="deliberate" ordered="false"
     * @generated
     */
    EList<Intent> getIntent();

    /**
     * Returns the value of the '<em><b>Capability</b></em>' attribute list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Capability}.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Capability}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Capability</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Capability</em>' attribute list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Capability
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_Capability()
     * @model default="accidental" ordered="false"
     * @generated
     */
    EList<Capability> getCapability();

    /**
     * Returns the value of the '<em><b>Effect F</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Effect F</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Effect F</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_EffectF()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaFailure> getEffectF();

    /**
     * Returns the value of the '<em><b>Effect H</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Effect H</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Effect H</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFault_EffectH()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaHazard> getEffectH();

} // DaFault
