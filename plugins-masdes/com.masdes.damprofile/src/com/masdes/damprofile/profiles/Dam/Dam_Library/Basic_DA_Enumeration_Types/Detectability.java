/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Detectability</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Basic_DA_Enumeration_TypesPackage#getDetectability()
 * @model
 * @generated
 */
public enum Detectability implements Enumerator {
    /**
     * The '<em><b>Signaled</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #SIGNALED_VALUE
     * @generated
     * @ordered
     */
    SIGNALED(0, "signaled", "signaled"),

    /**
     * The '<em><b>Unsignaled</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #UNSIGNALED_VALUE
     * @generated
     * @ordered
     */
    UNSIGNALED(1, "unsignaled", "unsignaled");

    /**
     * The '<em><b>Signaled</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Signaled</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #SIGNALED
     * @model name="signaled"
     * @generated
     * @ordered
     */
    public static final int SIGNALED_VALUE = 0;

    /**
     * The '<em><b>Unsignaled</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Unsignaled</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #UNSIGNALED
     * @model name="unsignaled"
     * @generated
     * @ordered
     */
    public static final int UNSIGNALED_VALUE = 1;

    /**
     * An array of all the '<em><b>Detectability</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static final Detectability[] VALUES_ARRAY =
        new Detectability[] {
            SIGNALED,
            UNSIGNALED,
        };

    /**
     * A public read-only list of all the '<em><b>Detectability</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final List<Detectability> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Detectability</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static Detectability get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            Detectability result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Detectability</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static Detectability getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            Detectability result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Detectability</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static Detectability get(int value) {
        switch (value) {
            case SIGNALED_VALUE: return SIGNALED;
            case UNSIGNALED_VALUE: return UNSIGNALED;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private Detectability(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public int getValue() {
      return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getName() {
      return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getLiteral() {
      return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }
    
} //Detectability
