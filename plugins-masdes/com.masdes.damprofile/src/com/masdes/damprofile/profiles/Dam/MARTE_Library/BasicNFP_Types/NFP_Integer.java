/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NFP Integer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_Integer()
 * @model
 * @generated
 */
public interface NFP_Integer extends NFP_CommonType {
    /**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(int)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_Integer_Value()
     * @model unique="false" dataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Integer" ordered="false"
     * @generated
     */
    int getValue();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer#getValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see #getValue()
     * @generated
     */
    void setValue(int value);

} // NFP_Integer
