/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.util;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.*;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage
 * @generated
 */
public class Basic_Data_TypesAdapterFactory extends AdapterFactoryImpl {
    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static Basic_Data_TypesPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Basic_Data_TypesAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = Basic_Data_TypesPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject)object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected Basic_Data_TypesSwitch<Adapter> modelSwitch =
        new Basic_Data_TypesSwitch<Adapter>() {
            @Override
            public Adapter caseDaLevel(DaLevel object) {
                return createDaLevelAdapter();
            }
            @Override
            public Adapter caseDaCriticalLevel(DaCriticalLevel object) {
                return createDaCriticalLevelAdapter();
            }
            @Override
            public Adapter caseDaCurrency(DaCurrency object) {
                return createDaCurrencyAdapter();
            }
            @Override
            public Adapter caseDaLikelihood(DaLikelihood object) {
                return createDaLikelihoodAdapter();
            }
            @Override
            public Adapter caseDaFrequency(DaFrequency object) {
                return createDaFrequencyAdapter();
            }
            @Override
            public Adapter caseNFP_CommonType(NFP_CommonType object) {
                return createNFP_CommonTypeAdapter();
            }
            @Override
            public Adapter caseNFP_Real(NFP_Real object) {
                return createNFP_RealAdapter();
            }
            @Override
            public Adapter defaultCase(EObject object) {
                return createEObjectAdapter();
            }
        };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject)target);
    }


    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel <em>Da Level</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel
     * @generated
     */
    public Adapter createDaLevelAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel <em>Da Critical Level</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel
     * @generated
     */
    public Adapter createDaCriticalLevelAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency <em>Da Currency</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency
     * @generated
     */
    public Adapter createDaCurrencyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLikelihood <em>Da Likelihood</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLikelihood
     * @generated
     */
    public Adapter createDaLikelihoodAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency <em>Da Frequency</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency
     * @generated
     */
    public Adapter createDaFrequencyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType <em>NFP Common Type</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType
     * @generated
     */
    public Adapter createNFP_CommonTypeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real <em>NFP Real</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real
     * @generated
     */
    public Adapter createNFP_RealAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} //Basic_Data_TypesAdapterFactory
