/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NFP Boolean</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean#isValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_Boolean()
 * @model
 * @generated
 */
public interface NFP_Boolean extends NFP_CommonType {
    /**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(boolean)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_Boolean_Value()
     * @model unique="false" dataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Boolean" ordered="false"
     * @generated
     */
    boolean isValue();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean#isValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see #isValue()
     * @generated
     */
    void setValue(boolean value);

} // NFP_Boolean
