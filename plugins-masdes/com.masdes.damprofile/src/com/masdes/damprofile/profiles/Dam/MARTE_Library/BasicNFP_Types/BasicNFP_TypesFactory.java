/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage
 * @generated
 */
public interface BasicNFP_TypesFactory extends EFactory {
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    BasicNFP_TypesFactory eINSTANCE = com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesFactoryImpl.init();

    /**
     * Returns a new object of class '<em>NFP Real</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>NFP Real</em>'.
     * @generated
     */
    NFP_Real createNFP_Real();

    /**
     * Returns a new object of class '<em>NFP Percentage</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>NFP Percentage</em>'.
     * @generated
     */
    NFP_Percentage createNFP_Percentage();

    /**
     * Returns a new object of class '<em>NFP Duration</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>NFP Duration</em>'.
     * @generated
     */
    NFP_Duration createNFP_Duration();

    /**
     * Returns a new object of class '<em>NFP Integer</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>NFP Integer</em>'.
     * @generated
     */
    NFP_Integer createNFP_Integer();

    /**
     * Returns a new object of class '<em>NFP Boolean</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>NFP Boolean</em>'.
     * @generated
     */
    NFP_Boolean createNFP_Boolean();

    /**
     * Returns a new object of class '<em>NFP Common Type</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>NFP Common Type</em>'.
     * @generated
     */
    NFP_CommonType createNFP_CommonType();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
    BasicNFP_TypesPackage getBasicNFP_TypesPackage();

} //BasicNFP_TypesFactory
