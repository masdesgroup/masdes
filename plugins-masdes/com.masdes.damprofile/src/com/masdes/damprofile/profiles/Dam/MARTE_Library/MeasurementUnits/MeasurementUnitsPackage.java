/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsFactory
 * @model kind="package"
 * @generated
 */
public interface MeasurementUnitsPackage extends EPackage {
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "MeasurementUnits";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "http://com.masdes.damprofile/libraries/MeasurementUnits/1.0";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "Dam.MARTE_Library.MeasurementUnits";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    MeasurementUnitsPackage eINSTANCE = com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl.init();

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.TimeUnitKind <em>Time Unit Kind</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.TimeUnitKind
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl#getTimeUnitKind()
     * @generated
     */
    int TIME_UNIT_KIND = 0;


    /**
     * Returns the meta object for enum '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.TimeUnitKind <em>Time Unit Kind</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Time Unit Kind</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.TimeUnitKind
     * @generated
     */
    EEnum getTimeUnitKind();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    MeasurementUnitsFactory getMeasurementUnitsFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.TimeUnitKind <em>Time Unit Kind</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.TimeUnitKind
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl#getTimeUnitKind()
         * @generated
         */
        EEnum TIME_UNIT_KIND = eINSTANCE.getTimeUnitKind();

    }

} //MeasurementUnitsPackage
