/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesFactory
 * @model kind="package"
 * @generated
 */
public interface BasicNFP_TypesPackage extends EPackage {
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "BasicNFP_Types";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "http://com.masdes.damprofile/libraries/BasicNFP_Types/1.0";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "Dam.MARTE_Library.BasicNFP_Types";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    BasicNFP_TypesPackage eINSTANCE = com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl.init();

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_CommonTypeImpl <em>NFP Common Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_CommonTypeImpl
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_CommonType()
     * @generated
     */
    int NFP_COMMON_TYPE = 5;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_COMMON_TYPE__EXPR = 0;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_COMMON_TYPE__SOURCE = 1;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_COMMON_TYPE__STAT_Q = 2;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_COMMON_TYPE__DIR = 3;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_COMMON_TYPE__MODE = 4;

    /**
     * The number of structural features of the '<em>NFP Common Type</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_COMMON_TYPE_FEATURE_COUNT = 5;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_RealImpl <em>NFP Real</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_RealImpl
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_Real()
     * @generated
     */
    int NFP_REAL = 0;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_REAL__EXPR = NFP_COMMON_TYPE__EXPR;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_REAL__SOURCE = NFP_COMMON_TYPE__SOURCE;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_REAL__STAT_Q = NFP_COMMON_TYPE__STAT_Q;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_REAL__DIR = NFP_COMMON_TYPE__DIR;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_REAL__MODE = NFP_COMMON_TYPE__MODE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_REAL__VALUE = NFP_COMMON_TYPE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>NFP Real</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_REAL_FEATURE_COUNT = NFP_COMMON_TYPE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_PercentageImpl <em>NFP Percentage</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_PercentageImpl
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_Percentage()
     * @generated
     */
    int NFP_PERCENTAGE = 1;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_PERCENTAGE__EXPR = NFP_REAL__EXPR;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_PERCENTAGE__SOURCE = NFP_REAL__SOURCE;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_PERCENTAGE__STAT_Q = NFP_REAL__STAT_Q;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_PERCENTAGE__DIR = NFP_REAL__DIR;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_PERCENTAGE__MODE = NFP_REAL__MODE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_PERCENTAGE__VALUE = NFP_REAL__VALUE;

    /**
     * The feature id for the '<em><b>Unit</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_PERCENTAGE__UNIT = NFP_REAL_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>NFP Percentage</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_PERCENTAGE_FEATURE_COUNT = NFP_REAL_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_DurationImpl <em>NFP Duration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_DurationImpl
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_Duration()
     * @generated
     */
    int NFP_DURATION = 2;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__EXPR = NFP_REAL__EXPR;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__SOURCE = NFP_REAL__SOURCE;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__STAT_Q = NFP_REAL__STAT_Q;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__DIR = NFP_REAL__DIR;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__MODE = NFP_REAL__MODE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__VALUE = NFP_REAL__VALUE;

    /**
     * The feature id for the '<em><b>Unit</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__UNIT = NFP_REAL_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Clock</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__CLOCK = NFP_REAL_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Precision</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__PRECISION = NFP_REAL_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Worst</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__WORST = NFP_REAL_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Best</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION__BEST = NFP_REAL_FEATURE_COUNT + 4;

    /**
     * The number of structural features of the '<em>NFP Duration</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_DURATION_FEATURE_COUNT = NFP_REAL_FEATURE_COUNT + 5;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_IntegerImpl <em>NFP Integer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_IntegerImpl
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_Integer()
     * @generated
     */
    int NFP_INTEGER = 3;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_INTEGER__EXPR = NFP_COMMON_TYPE__EXPR;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_INTEGER__SOURCE = NFP_COMMON_TYPE__SOURCE;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_INTEGER__STAT_Q = NFP_COMMON_TYPE__STAT_Q;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_INTEGER__DIR = NFP_COMMON_TYPE__DIR;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_INTEGER__MODE = NFP_COMMON_TYPE__MODE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_INTEGER__VALUE = NFP_COMMON_TYPE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>NFP Integer</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_INTEGER_FEATURE_COUNT = NFP_COMMON_TYPE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_BooleanImpl <em>NFP Boolean</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_BooleanImpl
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_Boolean()
     * @generated
     */
    int NFP_BOOLEAN = 4;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_BOOLEAN__EXPR = NFP_COMMON_TYPE__EXPR;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_BOOLEAN__SOURCE = NFP_COMMON_TYPE__SOURCE;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_BOOLEAN__STAT_Q = NFP_COMMON_TYPE__STAT_Q;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_BOOLEAN__DIR = NFP_COMMON_TYPE__DIR;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_BOOLEAN__MODE = NFP_COMMON_TYPE__MODE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_BOOLEAN__VALUE = NFP_COMMON_TYPE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>NFP Boolean</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int NFP_BOOLEAN_FEATURE_COUNT = NFP_COMMON_TYPE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.SourceKind <em>Source Kind</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.SourceKind
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getSourceKind()
     * @generated
     */
    int SOURCE_KIND = 6;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind <em>Statistical Qualifier Kind</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getStatisticalQualifierKind()
     * @generated
     */
    int STATISTICAL_QUALIFIER_KIND = 7;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.DirectionKind <em>Direction Kind</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.DirectionKind
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getDirectionKind()
     * @generated
     */
    int DIRECTION_KIND = 8;


    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real <em>NFP Real</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>NFP Real</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real
     * @generated
     */
    EClass getNFP_Real();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real#getValue()
     * @see #getNFP_Real()
     * @generated
     */
    EAttribute getNFP_Real_Value();

    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage <em>NFP Percentage</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>NFP Percentage</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage
     * @generated
     */
    EClass getNFP_Percentage();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage#getUnit <em>Unit</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Unit</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage#getUnit()
     * @see #getNFP_Percentage()
     * @generated
     */
    EAttribute getNFP_Percentage_Unit();

    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration <em>NFP Duration</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>NFP Duration</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration
     * @generated
     */
    EClass getNFP_Duration();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getUnit <em>Unit</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Unit</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getUnit()
     * @see #getNFP_Duration()
     * @generated
     */
    EAttribute getNFP_Duration_Unit();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getClock <em>Clock</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Clock</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getClock()
     * @see #getNFP_Duration()
     * @generated
     */
    EAttribute getNFP_Duration_Clock();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getPrecision <em>Precision</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Precision</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getPrecision()
     * @see #getNFP_Duration()
     * @generated
     */
    EAttribute getNFP_Duration_Precision();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getWorst <em>Worst</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Worst</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getWorst()
     * @see #getNFP_Duration()
     * @generated
     */
    EAttribute getNFP_Duration_Worst();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getBest <em>Best</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Best</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getBest()
     * @see #getNFP_Duration()
     * @generated
     */
    EAttribute getNFP_Duration_Best();

    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer <em>NFP Integer</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>NFP Integer</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer
     * @generated
     */
    EClass getNFP_Integer();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer#getValue()
     * @see #getNFP_Integer()
     * @generated
     */
    EAttribute getNFP_Integer_Value();

    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean <em>NFP Boolean</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>NFP Boolean</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean
     * @generated
     */
    EClass getNFP_Boolean();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean#isValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean#isValue()
     * @see #getNFP_Boolean()
     * @generated
     */
    EAttribute getNFP_Boolean_Value();

    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType <em>NFP Common Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>NFP Common Type</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType
     * @generated
     */
    EClass getNFP_CommonType();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getExpr <em>Expr</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Expr</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getExpr()
     * @see #getNFP_CommonType()
     * @generated
     */
    EAttribute getNFP_CommonType_Expr();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getSource <em>Source</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Source</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getSource()
     * @see #getNFP_CommonType()
     * @generated
     */
    EAttribute getNFP_CommonType_Source();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getStatQ <em>Stat Q</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Stat Q</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getStatQ()
     * @see #getNFP_CommonType()
     * @generated
     */
    EAttribute getNFP_CommonType_StatQ();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getDir <em>Dir</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Dir</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getDir()
     * @see #getNFP_CommonType()
     * @generated
     */
    EAttribute getNFP_CommonType_Dir();

    /**
     * Returns the meta object for the attribute list '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getMode <em>Mode</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute list '<em>Mode</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType#getMode()
     * @see #getNFP_CommonType()
     * @generated
     */
    EAttribute getNFP_CommonType_Mode();

    /**
     * Returns the meta object for enum '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.SourceKind <em>Source Kind</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Source Kind</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.SourceKind
     * @generated
     */
    EEnum getSourceKind();

    /**
     * Returns the meta object for enum '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind <em>Statistical Qualifier Kind</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Statistical Qualifier Kind</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind
     * @generated
     */
    EEnum getStatisticalQualifierKind();

    /**
     * Returns the meta object for enum '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.DirectionKind <em>Direction Kind</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Direction Kind</em>'.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.DirectionKind
     * @generated
     */
    EEnum getDirectionKind();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    BasicNFP_TypesFactory getBasicNFP_TypesFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_RealImpl <em>NFP Real</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_RealImpl
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_Real()
         * @generated
         */
        EClass NFP_REAL = eINSTANCE.getNFP_Real();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_REAL__VALUE = eINSTANCE.getNFP_Real_Value();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_PercentageImpl <em>NFP Percentage</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_PercentageImpl
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_Percentage()
         * @generated
         */
        EClass NFP_PERCENTAGE = eINSTANCE.getNFP_Percentage();

        /**
         * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_PERCENTAGE__UNIT = eINSTANCE.getNFP_Percentage_Unit();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_DurationImpl <em>NFP Duration</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_DurationImpl
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_Duration()
         * @generated
         */
        EClass NFP_DURATION = eINSTANCE.getNFP_Duration();

        /**
         * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_DURATION__UNIT = eINSTANCE.getNFP_Duration_Unit();

        /**
         * The meta object literal for the '<em><b>Clock</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_DURATION__CLOCK = eINSTANCE.getNFP_Duration_Clock();

        /**
         * The meta object literal for the '<em><b>Precision</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_DURATION__PRECISION = eINSTANCE.getNFP_Duration_Precision();

        /**
         * The meta object literal for the '<em><b>Worst</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_DURATION__WORST = eINSTANCE.getNFP_Duration_Worst();

        /**
         * The meta object literal for the '<em><b>Best</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_DURATION__BEST = eINSTANCE.getNFP_Duration_Best();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_IntegerImpl <em>NFP Integer</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_IntegerImpl
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_Integer()
         * @generated
         */
        EClass NFP_INTEGER = eINSTANCE.getNFP_Integer();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_INTEGER__VALUE = eINSTANCE.getNFP_Integer_Value();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_BooleanImpl <em>NFP Boolean</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_BooleanImpl
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_Boolean()
         * @generated
         */
        EClass NFP_BOOLEAN = eINSTANCE.getNFP_Boolean();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_BOOLEAN__VALUE = eINSTANCE.getNFP_Boolean_Value();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_CommonTypeImpl <em>NFP Common Type</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.NFP_CommonTypeImpl
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getNFP_CommonType()
         * @generated
         */
        EClass NFP_COMMON_TYPE = eINSTANCE.getNFP_CommonType();

        /**
         * The meta object literal for the '<em><b>Expr</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_COMMON_TYPE__EXPR = eINSTANCE.getNFP_CommonType_Expr();

        /**
         * The meta object literal for the '<em><b>Source</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_COMMON_TYPE__SOURCE = eINSTANCE.getNFP_CommonType_Source();

        /**
         * The meta object literal for the '<em><b>Stat Q</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_COMMON_TYPE__STAT_Q = eINSTANCE.getNFP_CommonType_StatQ();

        /**
         * The meta object literal for the '<em><b>Dir</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_COMMON_TYPE__DIR = eINSTANCE.getNFP_CommonType_Dir();

        /**
         * The meta object literal for the '<em><b>Mode</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute NFP_COMMON_TYPE__MODE = eINSTANCE.getNFP_CommonType_Mode();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.SourceKind <em>Source Kind</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.SourceKind
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getSourceKind()
         * @generated
         */
        EEnum SOURCE_KIND = eINSTANCE.getSourceKind();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind <em>Statistical Qualifier Kind</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getStatisticalQualifierKind()
         * @generated
         */
        EEnum STATISTICAL_QUALIFIER_KIND = eINSTANCE.getStatisticalQualifierKind();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.DirectionKind <em>Direction Kind</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.DirectionKind
         * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl#getDirectionKind()
         * @generated
         */
        EEnum DIRECTION_KIND = eINSTANCE.getDirectionKind();

    }

} //BasicNFP_TypesPackage
