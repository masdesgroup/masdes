/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.FactorOrigin;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Guideword;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLikelihood;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Hazard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getOrigin <em>Origin</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getSeverity <em>Severity</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getOccurrenceProb <em>Occurrence Prob</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getLikelihood <em>Likelihood</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getLevel <em>Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getLatency <em>Latency</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getAccidentLikelihood <em>Accident Likelihood</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getRisk <em>Risk</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getCost <em>Cost</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getGuideword <em>Guideword</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getAccident <em>Accident</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getCauseF <em>Cause F</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getCauseE <em>Cause E</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getDescription <em>Description</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard()
 * @model
 * @generated
 */
public interface DaHazard extends EObject {
    /**
     * Returns the value of the '<em><b>Origin</b></em>' attribute.
     * The default value is <code>"endogenous"</code>.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.FactorOrigin}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Origin</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Origin</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.FactorOrigin
     * @see #setOrigin(FactorOrigin)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_Origin()
     * @model default="endogenous" unique="false" ordered="false"
     * @generated
     */
    FactorOrigin getOrigin();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getOrigin <em>Origin</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Origin</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.FactorOrigin
     * @see #getOrigin()
     * @generated
     */
    void setOrigin(FactorOrigin value);

    /**
     * Returns the value of the '<em><b>Severity</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Severity</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Severity</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_Severity()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaCriticalLevel> getSeverity();

    /**
     * Returns the value of the '<em><b>Occurrence Prob</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Occurrence Prob</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Occurrence Prob</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_OccurrenceProb()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getOccurrenceProb();

    /**
     * Returns the value of the '<em><b>Likelihood</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLikelihood}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Likelihood</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Likelihood</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_Likelihood()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLikelihood> getLikelihood();

    /**
     * Returns the value of the '<em><b>Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_Level()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getLevel();

    /**
     * Returns the value of the '<em><b>Latency</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Latency</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Latency</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_Latency()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Duration> getLatency();

    /**
     * Returns the value of the '<em><b>Accident Likelihood</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLikelihood}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Accident Likelihood</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Accident Likelihood</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_AccidentLikelihood()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLikelihood> getAccidentLikelihood();

    /**
     * Returns the value of the '<em><b>Risk</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Risk</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Risk</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_Risk()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getRisk();

    /**
     * Returns the value of the '<em><b>Cost</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Cost</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Cost</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_Cost()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaCurrency> getCost();

    /**
     * Returns the value of the '<em><b>Guideword</b></em>' attribute.
     * The default value is <code>"value"</code>.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Guideword}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Guideword</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Guideword</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Guideword
     * @see #setGuideword(Guideword)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_Guideword()
     * @model default="value" unique="false" ordered="false"
     * @generated
     */
    Guideword getGuideword();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getGuideword <em>Guideword</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Guideword</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Guideword
     * @see #getGuideword()
     * @generated
     */
    void setGuideword(Guideword value);

    /**
     * Returns the value of the '<em><b>Accident</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Accident</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Accident</em>' attribute.
     * @see #setAccident(String)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_Accident()
     * @model unique="false" dataType="org.eclipse.uml2.types.String" ordered="false"
     * @generated
     */
    String getAccident();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getAccident <em>Accident</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Accident</em>' attribute.
     * @see #getAccident()
     * @generated
     */
    void setAccident(String value);

    /**
     * Returns the value of the '<em><b>Cause F</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Cause F</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Cause F</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_CauseF()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaFault> getCauseF();

    /**
     * Returns the value of the '<em><b>Cause E</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaError}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Cause E</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Cause E</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_CauseE()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaError> getCauseE();

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaHazard_Description()
     * @model unique="false" dataType="org.eclipse.uml2.types.String" ordered="false"
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard#getDescription <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

} // DaHazard
