/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.impl;

import com.masdes.damprofile.profiles.Core.CorePackage;

import com.masdes.damprofile.profiles.Core.impl.CorePackageImpl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Basic_DA_Enumeration_TypesPackage;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.impl.Basic_DA_Enumeration_TypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.impl.Complex_Data_TypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.impl.MARTE_DataTypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesFactory;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl;

import com.masdes.damprofile.profiles.Maintenance.MaintenancePackage;

import com.masdes.damprofile.profiles.Maintenance.impl.MaintenancePackageImpl;

import com.masdes.damprofile.profiles.Redundancy.RedundancyPackage;

import com.masdes.damprofile.profiles.Redundancy.impl.RedundancyPackageImpl;

import com.masdes.damprofile.profiles.Threats.ThreatsPackage;

import com.masdes.damprofile.profiles.Threats.impl.ThreatsPackageImpl;

import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MARTE_PrimitivesTypesPackageImpl extends EPackageImpl implements MARTE_PrimitivesTypesPackage {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EDataType realEDataType = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EDataType booleanEDataType = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EDataType integerEDataType = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EDataType stringEDataType = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private MARTE_PrimitivesTypesPackageImpl() {
        super(eNS_URI, MARTE_PrimitivesTypesFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>This method is used to initialize {@link MARTE_PrimitivesTypesPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static MARTE_PrimitivesTypesPackage init() {
        if (isInited) return (MARTE_PrimitivesTypesPackage)EPackage.Registry.INSTANCE.getEPackage(MARTE_PrimitivesTypesPackage.eNS_URI);

        // Obtain or create and register package
        MARTE_PrimitivesTypesPackageImpl theMARTE_PrimitivesTypesPackage = (MARTE_PrimitivesTypesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MARTE_PrimitivesTypesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MARTE_PrimitivesTypesPackageImpl());

        isInited = true;

        // Initialize simple dependencies
        MARTEPackage.eINSTANCE.eClass();
        MeasurementUnitsPackage.eINSTANCE.eClass();
        GRM_BasicTypesPackage.eINSTANCE.eClass();
        MARTE_DataTypesPackage.eINSTANCE.eClass();
        BasicNFP_TypesPackage.eINSTANCE.eClass();
        TimeTypesLibraryPackage.eINSTANCE.eClass();
        TimeLibraryPackage.eINSTANCE.eClass();
        RS_LibraryPackage.eINSTANCE.eClass();
        org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        Basic_DA_Enumeration_TypesPackageImpl theBasic_DA_Enumeration_TypesPackage = (Basic_DA_Enumeration_TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Basic_DA_Enumeration_TypesPackage.eNS_URI) instanceof Basic_DA_Enumeration_TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Basic_DA_Enumeration_TypesPackage.eNS_URI) : Basic_DA_Enumeration_TypesPackage.eINSTANCE);
        Complex_Data_TypesPackageImpl theComplex_Data_TypesPackage = (Complex_Data_TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Complex_Data_TypesPackage.eNS_URI) instanceof Complex_Data_TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Complex_Data_TypesPackage.eNS_URI) : Complex_Data_TypesPackage.eINSTANCE);
        Basic_Data_TypesPackageImpl theBasic_Data_TypesPackage = (Basic_Data_TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Basic_Data_TypesPackage.eNS_URI) instanceof Basic_Data_TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Basic_Data_TypesPackage.eNS_URI) : Basic_Data_TypesPackage.eINSTANCE);
        MARTE_DataTypesPackageImpl theMARTE_DataTypesPackage_1 = (MARTE_DataTypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage.eNS_URI) instanceof MARTE_DataTypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage.eNS_URI) : com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage.eINSTANCE);
        MeasurementUnitsPackageImpl theMeasurementUnitsPackage_1 = (MeasurementUnitsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage.eNS_URI) instanceof MeasurementUnitsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage.eNS_URI) : com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage.eINSTANCE);
        BasicNFP_TypesPackageImpl theBasicNFP_TypesPackage_1 = (BasicNFP_TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage.eNS_URI) instanceof BasicNFP_TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage.eNS_URI) : com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage.eINSTANCE);
        CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
        RedundancyPackageImpl theRedundancyPackage = (RedundancyPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RedundancyPackage.eNS_URI) instanceof RedundancyPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RedundancyPackage.eNS_URI) : RedundancyPackage.eINSTANCE);
        ThreatsPackageImpl theThreatsPackage = (ThreatsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ThreatsPackage.eNS_URI) instanceof ThreatsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ThreatsPackage.eNS_URI) : ThreatsPackage.eINSTANCE);
        MaintenancePackageImpl theMaintenancePackage = (MaintenancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MaintenancePackage.eNS_URI) instanceof MaintenancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MaintenancePackage.eNS_URI) : MaintenancePackage.eINSTANCE);

        // Create package meta-data objects
        theMARTE_PrimitivesTypesPackage.createPackageContents();
        theBasic_DA_Enumeration_TypesPackage.createPackageContents();
        theComplex_Data_TypesPackage.createPackageContents();
        theBasic_Data_TypesPackage.createPackageContents();
        theMARTE_DataTypesPackage_1.createPackageContents();
        theMeasurementUnitsPackage_1.createPackageContents();
        theBasicNFP_TypesPackage_1.createPackageContents();
        theCorePackage.createPackageContents();
        theRedundancyPackage.createPackageContents();
        theThreatsPackage.createPackageContents();
        theMaintenancePackage.createPackageContents();

        // Initialize created meta-data
        theMARTE_PrimitivesTypesPackage.initializePackageContents();
        theBasic_DA_Enumeration_TypesPackage.initializePackageContents();
        theComplex_Data_TypesPackage.initializePackageContents();
        theBasic_Data_TypesPackage.initializePackageContents();
        theMARTE_DataTypesPackage_1.initializePackageContents();
        theMeasurementUnitsPackage_1.initializePackageContents();
        theBasicNFP_TypesPackage_1.initializePackageContents();
        theCorePackage.initializePackageContents();
        theRedundancyPackage.initializePackageContents();
        theThreatsPackage.initializePackageContents();
        theMaintenancePackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theMARTE_PrimitivesTypesPackage.freeze();

  
        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(MARTE_PrimitivesTypesPackage.eNS_URI, theMARTE_PrimitivesTypesPackage);
        return theMARTE_PrimitivesTypesPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EDataType getReal() {
        return realEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EDataType getBoolean() {
        return booleanEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EDataType getInteger() {
        return integerEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EDataType getString() {
        return stringEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public MARTE_PrimitivesTypesFactory getMARTE_PrimitivesTypesFactory() {
        return (MARTE_PrimitivesTypesFactory)getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create data types
        realEDataType = createEDataType(REAL);
        booleanEDataType = createEDataType(BOOLEAN);
        integerEDataType = createEDataType(INTEGER);
        stringEDataType = createEDataType(STRING);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Initialize data types
        initEDataType(realEDataType, String.class, "Real", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(booleanEDataType, boolean.class, "Boolean", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(integerEDataType, int.class, "Integer", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

        // Create resource
        createResource(eNS_URI);
    }

} //MARTE_PrimitivesTypesPackageImpl
