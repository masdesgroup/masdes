/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BasicNFP_TypesFactoryImpl extends EFactoryImpl implements BasicNFP_TypesFactory {
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static BasicNFP_TypesFactory init() {
        try {
            BasicNFP_TypesFactory theBasicNFP_TypesFactory = (BasicNFP_TypesFactory)EPackage.Registry.INSTANCE.getEFactory(BasicNFP_TypesPackage.eNS_URI);
            if (theBasicNFP_TypesFactory != null) {
                return theBasicNFP_TypesFactory;
            }
        }
        catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new BasicNFP_TypesFactoryImpl();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public BasicNFP_TypesFactoryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case BasicNFP_TypesPackage.NFP_REAL: return createNFP_Real();
            case BasicNFP_TypesPackage.NFP_PERCENTAGE: return createNFP_Percentage();
            case BasicNFP_TypesPackage.NFP_DURATION: return createNFP_Duration();
            case BasicNFP_TypesPackage.NFP_INTEGER: return createNFP_Integer();
            case BasicNFP_TypesPackage.NFP_BOOLEAN: return createNFP_Boolean();
            case BasicNFP_TypesPackage.NFP_COMMON_TYPE: return createNFP_CommonType();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID()) {
            case BasicNFP_TypesPackage.SOURCE_KIND:
                return createSourceKindFromString(eDataType, initialValue);
            case BasicNFP_TypesPackage.STATISTICAL_QUALIFIER_KIND:
                return createStatisticalQualifierKindFromString(eDataType, initialValue);
            case BasicNFP_TypesPackage.DIRECTION_KIND:
                return createDirectionKindFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID()) {
            case BasicNFP_TypesPackage.SOURCE_KIND:
                return convertSourceKindToString(eDataType, instanceValue);
            case BasicNFP_TypesPackage.STATISTICAL_QUALIFIER_KIND:
                return convertStatisticalQualifierKindToString(eDataType, instanceValue);
            case BasicNFP_TypesPackage.DIRECTION_KIND:
                return convertDirectionKindToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NFP_Real createNFP_Real() {
        NFP_RealImpl nfP_Real = new NFP_RealImpl();
        return nfP_Real;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NFP_Percentage createNFP_Percentage() {
        NFP_PercentageImpl nfP_Percentage = new NFP_PercentageImpl();
        return nfP_Percentage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NFP_Duration createNFP_Duration() {
        NFP_DurationImpl nfP_Duration = new NFP_DurationImpl();
        return nfP_Duration;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NFP_Integer createNFP_Integer() {
        NFP_IntegerImpl nfP_Integer = new NFP_IntegerImpl();
        return nfP_Integer;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NFP_Boolean createNFP_Boolean() {
        NFP_BooleanImpl nfP_Boolean = new NFP_BooleanImpl();
        return nfP_Boolean;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NFP_CommonType createNFP_CommonType() {
        NFP_CommonTypeImpl nfP_CommonType = new NFP_CommonTypeImpl();
        return nfP_CommonType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public SourceKind createSourceKindFromString(EDataType eDataType, String initialValue) {
        SourceKind result = SourceKind.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertSourceKindToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public StatisticalQualifierKind createStatisticalQualifierKindFromString(EDataType eDataType, String initialValue) {
        StatisticalQualifierKind result = StatisticalQualifierKind.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertStatisticalQualifierKindToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DirectionKind createDirectionKindFromString(EDataType eDataType, String initialValue) {
        DirectionKind result = DirectionKind.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertDirectionKindToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public BasicNFP_TypesPackage getBasicNFP_TypesPackage() {
        return (BasicNFP_TypesPackage)getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
    @Deprecated
    public static BasicNFP_TypesPackage getPackage() {
        return BasicNFP_TypesPackage.eINSTANCE;
    }

} //BasicNFP_TypesFactoryImpl
