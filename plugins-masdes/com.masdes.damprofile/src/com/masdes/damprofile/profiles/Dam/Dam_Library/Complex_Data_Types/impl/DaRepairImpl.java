/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.impl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRepair;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Repair</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.impl.DaRepairImpl#getRate <em>Rate</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.impl.DaRepairImpl#getMTTR <em>MTTR</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.impl.DaRepairImpl#getDistribution <em>Distribution</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaRepairImpl extends EObjectImpl implements DaRepair {
    /**
     * The cached value of the '{@link #getRate() <em>Rate</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getRate()
     * @generated
     * @ordered
     */
    protected EList<DaFrequency> rate;

    /**
     * The cached value of the '{@link #getMTTR() <em>MTTR</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMTTR()
     * @generated
     * @ordered
     */
    protected EList<NFP_Duration> mttr;

    /**
     * The cached value of the '{@link #getDistribution() <em>Distribution</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDistribution()
     * @generated
     * @ordered
     */
    protected EList<NFP_CommonType> distribution;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaRepairImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return Complex_Data_TypesPackage.Literals.DA_REPAIR;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaFrequency> getRate() {
        if (rate == null) {
            rate = new EObjectContainmentEList<DaFrequency>(DaFrequency.class, this, Complex_Data_TypesPackage.DA_REPAIR__RATE);
        }
        return rate;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Duration> getMTTR() {
        if (mttr == null) {
            mttr = new EObjectContainmentEList<NFP_Duration>(NFP_Duration.class, this, Complex_Data_TypesPackage.DA_REPAIR__MTTR);
        }
        return mttr;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_CommonType> getDistribution() {
        if (distribution == null) {
            distribution = new EObjectContainmentEList<NFP_CommonType>(NFP_CommonType.class, this, Complex_Data_TypesPackage.DA_REPAIR__DISTRIBUTION);
        }
        return distribution;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case Complex_Data_TypesPackage.DA_REPAIR__RATE:
                return ((InternalEList<?>)getRate()).basicRemove(otherEnd, msgs);
            case Complex_Data_TypesPackage.DA_REPAIR__MTTR:
                return ((InternalEList<?>)getMTTR()).basicRemove(otherEnd, msgs);
            case Complex_Data_TypesPackage.DA_REPAIR__DISTRIBUTION:
                return ((InternalEList<?>)getDistribution()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case Complex_Data_TypesPackage.DA_REPAIR__RATE:
                return getRate();
            case Complex_Data_TypesPackage.DA_REPAIR__MTTR:
                return getMTTR();
            case Complex_Data_TypesPackage.DA_REPAIR__DISTRIBUTION:
                return getDistribution();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case Complex_Data_TypesPackage.DA_REPAIR__RATE:
                getRate().clear();
                getRate().addAll((Collection<? extends DaFrequency>)newValue);
                return;
            case Complex_Data_TypesPackage.DA_REPAIR__MTTR:
                getMTTR().clear();
                getMTTR().addAll((Collection<? extends NFP_Duration>)newValue);
                return;
            case Complex_Data_TypesPackage.DA_REPAIR__DISTRIBUTION:
                getDistribution().clear();
                getDistribution().addAll((Collection<? extends NFP_CommonType>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case Complex_Data_TypesPackage.DA_REPAIR__RATE:
                getRate().clear();
                return;
            case Complex_Data_TypesPackage.DA_REPAIR__MTTR:
                getMTTR().clear();
                return;
            case Complex_Data_TypesPackage.DA_REPAIR__DISTRIBUTION:
                getDistribution().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case Complex_Data_TypesPackage.DA_REPAIR__RATE:
                return rate != null && !rate.isEmpty();
            case Complex_Data_TypesPackage.DA_REPAIR__MTTR:
                return mttr != null && !mttr.isEmpty();
            case Complex_Data_TypesPackage.DA_REPAIR__DISTRIBUTION:
                return distribution != null && !distribution.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //DaRepairImpl
