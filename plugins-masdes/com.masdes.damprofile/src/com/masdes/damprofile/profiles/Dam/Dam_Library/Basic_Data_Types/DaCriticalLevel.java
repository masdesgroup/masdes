/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.CriticalLevel;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Critical Level</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage#getDaCriticalLevel()
 * @model
 * @generated
 */
public interface DaCriticalLevel extends NFP_CommonType {
    /**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * The default value is <code>"minor"</code>.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.CriticalLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Value</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.CriticalLevel
     * @see #setValue(CriticalLevel)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage#getDaCriticalLevel_Value()
     * @model default="minor" unique="false" required="true" ordered="false"
     * @generated
     */
    CriticalLevel getValue();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel#getValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.CriticalLevel
     * @see #getValue()
     * @generated
     */
    void setValue(CriticalLevel value);

} // DaCriticalLevel
