/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage
 * @generated
 */
public interface MARTE_PrimitivesTypesFactory extends EFactory {
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    MARTE_PrimitivesTypesFactory eINSTANCE = com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.impl.MARTE_PrimitivesTypesFactoryImpl.init();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
    MARTE_PrimitivesTypesPackage getMARTE_PrimitivesTypesPackage();

} //MARTE_PrimitivesTypesFactory
