/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Basic_Data_TypesFactoryImpl extends EFactoryImpl implements Basic_Data_TypesFactory {
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static Basic_Data_TypesFactory init() {
        try {
            Basic_Data_TypesFactory theBasic_Data_TypesFactory = (Basic_Data_TypesFactory)EPackage.Registry.INSTANCE.getEFactory(Basic_Data_TypesPackage.eNS_URI);
            if (theBasic_Data_TypesFactory != null) {
                return theBasic_Data_TypesFactory;
            }
        }
        catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new Basic_Data_TypesFactoryImpl();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Basic_Data_TypesFactoryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case Basic_Data_TypesPackage.DA_LEVEL: return createDaLevel();
            case Basic_Data_TypesPackage.DA_CRITICAL_LEVEL: return createDaCriticalLevel();
            case Basic_Data_TypesPackage.DA_CURRENCY: return createDaCurrency();
            case Basic_Data_TypesPackage.DA_LIKELIHOOD: return createDaLikelihood();
            case Basic_Data_TypesPackage.DA_FREQUENCY: return createDaFrequency();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DaLevel createDaLevel() {
        DaLevelImpl daLevel = new DaLevelImpl();
        return daLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DaCriticalLevel createDaCriticalLevel() {
        DaCriticalLevelImpl daCriticalLevel = new DaCriticalLevelImpl();
        return daCriticalLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DaCurrency createDaCurrency() {
        DaCurrencyImpl daCurrency = new DaCurrencyImpl();
        return daCurrency;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DaLikelihood createDaLikelihood() {
        DaLikelihoodImpl daLikelihood = new DaLikelihoodImpl();
        return daLikelihood;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DaFrequency createDaFrequency() {
        DaFrequencyImpl daFrequency = new DaFrequencyImpl();
        return daFrequency;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Basic_Data_TypesPackage getBasic_Data_TypesPackage() {
        return (Basic_Data_TypesPackage)getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
    @Deprecated
    public static Basic_Data_TypesPackage getPackage() {
        return Basic_Data_TypesPackage.eINSTANCE;
    }

} //Basic_Data_TypesFactoryImpl
