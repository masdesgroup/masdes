/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.impl;

import com.masdes.damprofile.profiles.Core.CorePackage;

import com.masdes.damprofile.profiles.Core.impl.CorePackageImpl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Basic_DA_Enumeration_TypesPackage;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.impl.Basic_DA_Enumeration_TypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.impl.Complex_Data_TypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesFactory;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.DirectionKind;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.SourceKind;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.impl.MARTE_DataTypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.impl.MARTE_PrimitivesTypesPackageImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl;

import com.masdes.damprofile.profiles.Maintenance.MaintenancePackage;

import com.masdes.damprofile.profiles.Maintenance.impl.MaintenancePackageImpl;

import com.masdes.damprofile.profiles.Redundancy.RedundancyPackage;

import com.masdes.damprofile.profiles.Redundancy.impl.RedundancyPackageImpl;

import com.masdes.damprofile.profiles.Threats.ThreatsPackage;

import com.masdes.damprofile.profiles.Threats.impl.ThreatsPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.uml2.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BasicNFP_TypesPackageImpl extends EPackageImpl implements BasicNFP_TypesPackage {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nfP_RealEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nfP_PercentageEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nfP_DurationEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nfP_IntegerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nfP_BooleanEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nfP_CommonTypeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum sourceKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum statisticalQualifierKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum directionKindEEnum = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private BasicNFP_TypesPackageImpl() {
        super(eNS_URI, BasicNFP_TypesFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>This method is used to initialize {@link BasicNFP_TypesPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static BasicNFP_TypesPackage init() {
        if (isInited) return (BasicNFP_TypesPackage)EPackage.Registry.INSTANCE.getEPackage(BasicNFP_TypesPackage.eNS_URI);

        // Obtain or create and register package
        BasicNFP_TypesPackageImpl theBasicNFP_TypesPackage = (BasicNFP_TypesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BasicNFP_TypesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BasicNFP_TypesPackageImpl());

        isInited = true;

        // Initialize simple dependencies
        MARTEPackage.eINSTANCE.eClass();
        MeasurementUnitsPackage.eINSTANCE.eClass();
        GRM_BasicTypesPackage.eINSTANCE.eClass();
        MARTE_DataTypesPackage.eINSTANCE.eClass();
        org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage.eINSTANCE.eClass();
        TimeTypesLibraryPackage.eINSTANCE.eClass();
        TimeLibraryPackage.eINSTANCE.eClass();
        RS_LibraryPackage.eINSTANCE.eClass();
        MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        Basic_DA_Enumeration_TypesPackageImpl theBasic_DA_Enumeration_TypesPackage = (Basic_DA_Enumeration_TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Basic_DA_Enumeration_TypesPackage.eNS_URI) instanceof Basic_DA_Enumeration_TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Basic_DA_Enumeration_TypesPackage.eNS_URI) : Basic_DA_Enumeration_TypesPackage.eINSTANCE);
        Complex_Data_TypesPackageImpl theComplex_Data_TypesPackage = (Complex_Data_TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Complex_Data_TypesPackage.eNS_URI) instanceof Complex_Data_TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Complex_Data_TypesPackage.eNS_URI) : Complex_Data_TypesPackage.eINSTANCE);
        Basic_Data_TypesPackageImpl theBasic_Data_TypesPackage = (Basic_Data_TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Basic_Data_TypesPackage.eNS_URI) instanceof Basic_Data_TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Basic_Data_TypesPackage.eNS_URI) : Basic_Data_TypesPackage.eINSTANCE);
        MARTE_PrimitivesTypesPackageImpl theMARTE_PrimitivesTypesPackage_1 = (MARTE_PrimitivesTypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage.eNS_URI) instanceof MARTE_PrimitivesTypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage.eNS_URI) : com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage.eINSTANCE);
        MARTE_DataTypesPackageImpl theMARTE_DataTypesPackage_1 = (MARTE_DataTypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage.eNS_URI) instanceof MARTE_DataTypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage.eNS_URI) : com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage.eINSTANCE);
        MeasurementUnitsPackageImpl theMeasurementUnitsPackage_1 = (MeasurementUnitsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage.eNS_URI) instanceof MeasurementUnitsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage.eNS_URI) : com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage.eINSTANCE);
        CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
        RedundancyPackageImpl theRedundancyPackage = (RedundancyPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RedundancyPackage.eNS_URI) instanceof RedundancyPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RedundancyPackage.eNS_URI) : RedundancyPackage.eINSTANCE);
        ThreatsPackageImpl theThreatsPackage = (ThreatsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ThreatsPackage.eNS_URI) instanceof ThreatsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ThreatsPackage.eNS_URI) : ThreatsPackage.eINSTANCE);
        MaintenancePackageImpl theMaintenancePackage = (MaintenancePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MaintenancePackage.eNS_URI) instanceof MaintenancePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MaintenancePackage.eNS_URI) : MaintenancePackage.eINSTANCE);

        // Create package meta-data objects
        theBasicNFP_TypesPackage.createPackageContents();
        theBasic_DA_Enumeration_TypesPackage.createPackageContents();
        theComplex_Data_TypesPackage.createPackageContents();
        theBasic_Data_TypesPackage.createPackageContents();
        theMARTE_PrimitivesTypesPackage_1.createPackageContents();
        theMARTE_DataTypesPackage_1.createPackageContents();
        theMeasurementUnitsPackage_1.createPackageContents();
        theCorePackage.createPackageContents();
        theRedundancyPackage.createPackageContents();
        theThreatsPackage.createPackageContents();
        theMaintenancePackage.createPackageContents();

        // Initialize created meta-data
        theBasicNFP_TypesPackage.initializePackageContents();
        theBasic_DA_Enumeration_TypesPackage.initializePackageContents();
        theComplex_Data_TypesPackage.initializePackageContents();
        theBasic_Data_TypesPackage.initializePackageContents();
        theMARTE_PrimitivesTypesPackage_1.initializePackageContents();
        theMARTE_DataTypesPackage_1.initializePackageContents();
        theMeasurementUnitsPackage_1.initializePackageContents();
        theCorePackage.initializePackageContents();
        theRedundancyPackage.initializePackageContents();
        theThreatsPackage.initializePackageContents();
        theMaintenancePackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theBasicNFP_TypesPackage.freeze();

  
        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(BasicNFP_TypesPackage.eNS_URI, theBasicNFP_TypesPackage);
        return theBasicNFP_TypesPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getNFP_Real() {
        return nfP_RealEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_Real_Value() {
        return (EAttribute)nfP_RealEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getNFP_Percentage() {
        return nfP_PercentageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_Percentage_Unit() {
        return (EAttribute)nfP_PercentageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getNFP_Duration() {
        return nfP_DurationEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_Duration_Unit() {
        return (EAttribute)nfP_DurationEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_Duration_Clock() {
        return (EAttribute)nfP_DurationEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_Duration_Precision() {
        return (EAttribute)nfP_DurationEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_Duration_Worst() {
        return (EAttribute)nfP_DurationEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_Duration_Best() {
        return (EAttribute)nfP_DurationEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getNFP_Integer() {
        return nfP_IntegerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_Integer_Value() {
        return (EAttribute)nfP_IntegerEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getNFP_Boolean() {
        return nfP_BooleanEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_Boolean_Value() {
        return (EAttribute)nfP_BooleanEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getNFP_CommonType() {
        return nfP_CommonTypeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_CommonType_Expr() {
        return (EAttribute)nfP_CommonTypeEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_CommonType_Source() {
        return (EAttribute)nfP_CommonTypeEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_CommonType_StatQ() {
        return (EAttribute)nfP_CommonTypeEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_CommonType_Dir() {
        return (EAttribute)nfP_CommonTypeEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getNFP_CommonType_Mode() {
        return (EAttribute)nfP_CommonTypeEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EEnum getSourceKind() {
        return sourceKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EEnum getStatisticalQualifierKind() {
        return statisticalQualifierKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EEnum getDirectionKind() {
        return directionKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public BasicNFP_TypesFactory getBasicNFP_TypesFactory() {
        return (BasicNFP_TypesFactory)getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        nfP_RealEClass = createEClass(NFP_REAL);
        createEAttribute(nfP_RealEClass, NFP_REAL__VALUE);

        nfP_PercentageEClass = createEClass(NFP_PERCENTAGE);
        createEAttribute(nfP_PercentageEClass, NFP_PERCENTAGE__UNIT);

        nfP_DurationEClass = createEClass(NFP_DURATION);
        createEAttribute(nfP_DurationEClass, NFP_DURATION__UNIT);
        createEAttribute(nfP_DurationEClass, NFP_DURATION__CLOCK);
        createEAttribute(nfP_DurationEClass, NFP_DURATION__PRECISION);
        createEAttribute(nfP_DurationEClass, NFP_DURATION__WORST);
        createEAttribute(nfP_DurationEClass, NFP_DURATION__BEST);

        nfP_IntegerEClass = createEClass(NFP_INTEGER);
        createEAttribute(nfP_IntegerEClass, NFP_INTEGER__VALUE);

        nfP_BooleanEClass = createEClass(NFP_BOOLEAN);
        createEAttribute(nfP_BooleanEClass, NFP_BOOLEAN__VALUE);

        nfP_CommonTypeEClass = createEClass(NFP_COMMON_TYPE);
        createEAttribute(nfP_CommonTypeEClass, NFP_COMMON_TYPE__EXPR);
        createEAttribute(nfP_CommonTypeEClass, NFP_COMMON_TYPE__SOURCE);
        createEAttribute(nfP_CommonTypeEClass, NFP_COMMON_TYPE__STAT_Q);
        createEAttribute(nfP_CommonTypeEClass, NFP_COMMON_TYPE__DIR);
        createEAttribute(nfP_CommonTypeEClass, NFP_COMMON_TYPE__MODE);

        // Create enums
        sourceKindEEnum = createEEnum(SOURCE_KIND);
        statisticalQualifierKindEEnum = createEEnum(STATISTICAL_QUALIFIER_KIND);
        directionKindEEnum = createEEnum(DIRECTION_KIND);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage theMARTE_PrimitivesTypesPackage_1 = (com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage)EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage.eNS_URI);
        com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage theMeasurementUnitsPackage_1 = (com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage)EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage.eNS_URI);
        com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage theMARTE_DataTypesPackage_1 = (com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage)EPackage.Registry.INSTANCE.getEPackage(com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage.eNS_URI);
        TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        nfP_RealEClass.getESuperTypes().add(this.getNFP_CommonType());
        nfP_PercentageEClass.getESuperTypes().add(this.getNFP_Real());
        nfP_DurationEClass.getESuperTypes().add(this.getNFP_Real());
        nfP_IntegerEClass.getESuperTypes().add(this.getNFP_CommonType());
        nfP_BooleanEClass.getESuperTypes().add(this.getNFP_CommonType());

        // Initialize classes and features; add operations and parameters
        initEClass(nfP_RealEClass, NFP_Real.class, "NFP_Real", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getNFP_Real_Value(), theMARTE_PrimitivesTypesPackage_1.getReal(), "value", null, 0, 1, NFP_Real.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(nfP_PercentageEClass, NFP_Percentage.class, "NFP_Percentage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getNFP_Percentage_Unit(), theMARTE_PrimitivesTypesPackage_1.getString(), "unit", "%", 0, 1, NFP_Percentage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(nfP_DurationEClass, NFP_Duration.class, "NFP_Duration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getNFP_Duration_Unit(), theMeasurementUnitsPackage_1.getTimeUnitKind(), "unit", null, 0, 1, NFP_Duration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
        initEAttribute(getNFP_Duration_Clock(), theMARTE_PrimitivesTypesPackage_1.getString(), "clock", null, 0, 1, NFP_Duration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
        initEAttribute(getNFP_Duration_Precision(), theMARTE_PrimitivesTypesPackage_1.getReal(), "precision", null, 0, 1, NFP_Duration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
        initEAttribute(getNFP_Duration_Worst(), theMARTE_PrimitivesTypesPackage_1.getReal(), "worst", null, 0, 1, NFP_Duration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
        initEAttribute(getNFP_Duration_Best(), theMARTE_PrimitivesTypesPackage_1.getReal(), "best", null, 0, 1, NFP_Duration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(nfP_IntegerEClass, NFP_Integer.class, "NFP_Integer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getNFP_Integer_Value(), theMARTE_PrimitivesTypesPackage_1.getInteger(), "value", null, 0, 1, NFP_Integer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(nfP_BooleanEClass, NFP_Boolean.class, "NFP_Boolean", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getNFP_Boolean_Value(), theMARTE_PrimitivesTypesPackage_1.getBoolean(), "value", null, 0, 1, NFP_Boolean.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        initEClass(nfP_CommonTypeEClass, NFP_CommonType.class, "NFP_CommonType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getNFP_CommonType_Expr(), theMARTE_DataTypesPackage_1.getVSL_Expression(), "expr", null, 0, 1, NFP_CommonType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
        initEAttribute(getNFP_CommonType_Source(), this.getSourceKind(), "source", null, 0, 1, NFP_CommonType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
        initEAttribute(getNFP_CommonType_StatQ(), this.getStatisticalQualifierKind(), "statQ", null, 0, 1, NFP_CommonType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
        initEAttribute(getNFP_CommonType_Dir(), this.getDirectionKind(), "dir", null, 0, 1, NFP_CommonType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
        initEAttribute(getNFP_CommonType_Mode(), theTypesPackage.getString(), "mode", null, 0, -1, NFP_CommonType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

        EOperation op = addEOperation(nfP_CommonTypeEClass, null, "bernoulli", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "prob", 1, 1, IS_UNIQUE, !IS_ORDERED);

        op = addEOperation(nfP_CommonTypeEClass, null, "binomial", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "prob", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getInteger(), "trials", 1, 1, IS_UNIQUE, !IS_ORDERED);

        op = addEOperation(nfP_CommonTypeEClass, null, "exp", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "mean", 1, 1, IS_UNIQUE, !IS_ORDERED);

        op = addEOperation(nfP_CommonTypeEClass, null, "gamma", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getInteger(), "k", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "mean", 1, 1, IS_UNIQUE, !IS_ORDERED);

        op = addEOperation(nfP_CommonTypeEClass, null, "normal", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "mean", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "standDev", 1, 1, IS_UNIQUE, !IS_ORDERED);

        op = addEOperation(nfP_CommonTypeEClass, null, "poisson", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "mean", 1, 1, IS_UNIQUE, !IS_ORDERED);

        op = addEOperation(nfP_CommonTypeEClass, null, "uniform", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "min", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "max", 1, 1, IS_UNIQUE, !IS_ORDERED);

        op = addEOperation(nfP_CommonTypeEClass, null, "geometric", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "p", 1, 1, IS_UNIQUE, !IS_ORDERED);

        op = addEOperation(nfP_CommonTypeEClass, null, "triangular", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "min", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "max", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "mode", 1, 1, IS_UNIQUE, !IS_ORDERED);

        op = addEOperation(nfP_CommonTypeEClass, null, "logarithmic", 1, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, theMARTE_PrimitivesTypesPackage_1.getReal(), "theta", 1, 1, IS_UNIQUE, !IS_ORDERED);

        // Initialize enums and add enum literals
        initEEnum(sourceKindEEnum, SourceKind.class, "SourceKind");
        addEEnumLiteral(sourceKindEEnum, SourceKind.EST);
        addEEnumLiteral(sourceKindEEnum, SourceKind.MEAS);
        addEEnumLiteral(sourceKindEEnum, SourceKind.CALC);
        addEEnumLiteral(sourceKindEEnum, SourceKind.REQ);

        initEEnum(statisticalQualifierKindEEnum, StatisticalQualifierKind.class, "StatisticalQualifierKind");
        addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.MAX);
        addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.MIN);
        addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.MEAN);
        addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.RANGE);
        addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.PERCENT);
        addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.DISTRIB);
        addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.DETERM);
        addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.OTHER);
        addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.VARIANCE);

        initEEnum(directionKindEEnum, DirectionKind.class, "DirectionKind");
        addEEnumLiteral(directionKindEEnum, DirectionKind.INCR);
        addEEnumLiteral(directionKindEEnum, DirectionKind.DECR);

        // Create resource
        createResource(eNS_URI);
    }

} //BasicNFP_TypesPackageImpl
