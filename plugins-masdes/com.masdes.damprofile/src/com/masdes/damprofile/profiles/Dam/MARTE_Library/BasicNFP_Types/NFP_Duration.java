/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.TimeUnitKind;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NFP Duration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getUnit <em>Unit</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getClock <em>Clock</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getPrecision <em>Precision</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getWorst <em>Worst</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getBest <em>Best</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_Duration()
 * @model
 * @generated
 */
public interface NFP_Duration extends NFP_Real {
    /**
     * Returns the value of the '<em><b>Unit</b></em>' attribute.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.TimeUnitKind}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Unit</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Unit</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.TimeUnitKind
     * @see #setUnit(TimeUnitKind)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_Duration_Unit()
     * @model unique="false" ordered="false"
     * @generated
     */
    TimeUnitKind getUnit();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getUnit <em>Unit</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Unit</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.MeasurementUnits.TimeUnitKind
     * @see #getUnit()
     * @generated
     */
    void setUnit(TimeUnitKind value);

    /**
     * Returns the value of the '<em><b>Clock</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Clock</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Clock</em>' attribute.
     * @see #setClock(String)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_Duration_Clock()
     * @model unique="false" dataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.String" ordered="false"
     * @generated
     */
    String getClock();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getClock <em>Clock</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Clock</em>' attribute.
     * @see #getClock()
     * @generated
     */
    void setClock(String value);

    /**
     * Returns the value of the '<em><b>Precision</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Precision</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Precision</em>' attribute.
     * @see #setPrecision(String)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_Duration_Precision()
     * @model unique="false" dataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" ordered="false"
     * @generated
     */
    String getPrecision();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getPrecision <em>Precision</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Precision</em>' attribute.
     * @see #getPrecision()
     * @generated
     */
    void setPrecision(String value);

    /**
     * Returns the value of the '<em><b>Worst</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Worst</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Worst</em>' attribute.
     * @see #setWorst(String)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_Duration_Worst()
     * @model unique="false" dataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" ordered="false"
     * @generated
     */
    String getWorst();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getWorst <em>Worst</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Worst</em>' attribute.
     * @see #getWorst()
     * @generated
     */
    void setWorst(String value);

    /**
     * Returns the value of the '<em><b>Best</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Best</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Best</em>' attribute.
     * @see #setBest(String)
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#getNFP_Duration_Best()
     * @model unique="false" dataType="com.masdes.damprofile.profiles.Dam.MARTE_Library.MARTE_PrimitivesTypes.Real" ordered="false"
     * @generated
     */
    String getBest();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration#getBest <em>Best</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Best</em>' attribute.
     * @see #getBest()
     * @generated
     */
    void setBest(String value);

} // NFP_Duration
