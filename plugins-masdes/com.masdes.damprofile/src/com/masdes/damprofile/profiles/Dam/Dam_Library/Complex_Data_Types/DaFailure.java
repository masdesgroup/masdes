/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Consistency;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Detectability;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Domain;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Failure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getOccurrenceRate <em>Occurrence Rate</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getMTTF <em>MTTF</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getMTBF <em>MTBF</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getOccurrenceProb <em>Occurrence Prob</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getOccurrenceDist <em>Occurrence Dist</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getRisk <em>Risk</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getCauseF <em>Cause F</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getCauseE <em>Cause E</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getDescription <em>Description</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getDomain <em>Domain</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getConsequence <em>Consequence</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getCost <em>Cost</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getConsistency <em>Consistency</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getDetectability <em>Detectability</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure()
 * @model
 * @generated
 */
public interface DaFailure extends EObject {
    /**
     * Returns the value of the '<em><b>Occurrence Rate</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Occurrence Rate</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Occurrence Rate</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_OccurrenceRate()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaFrequency> getOccurrenceRate();

    /**
     * Returns the value of the '<em><b>MTTF</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>MTTF</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>MTTF</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_MTTF()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Duration> getMTTF();

    /**
     * Returns the value of the '<em><b>MTBF</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>MTBF</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>MTBF</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_MTBF()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Duration> getMTBF();

    /**
     * Returns the value of the '<em><b>Occurrence Prob</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Occurrence Prob</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Occurrence Prob</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_OccurrenceProb()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getOccurrenceProb();

    /**
     * Returns the value of the '<em><b>Occurrence Dist</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Occurrence Dist</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Occurrence Dist</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_OccurrenceDist()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_CommonType> getOccurrenceDist();

    /**
     * Returns the value of the '<em><b>Risk</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Risk</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Risk</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_Risk()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getRisk();

    /**
     * Returns the value of the '<em><b>Condition</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Condition</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Condition</em>' attribute.
     * @see #setCondition(String)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_Condition()
     * @model unique="false" dataType="org.eclipse.uml2.types.String" ordered="false"
     * @generated
     */
    String getCondition();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getCondition <em>Condition</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Condition</em>' attribute.
     * @see #getCondition()
     * @generated
     */
    void setCondition(String value);

    /**
     * Returns the value of the '<em><b>Cause F</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Cause F</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Cause F</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_CauseF()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaFault> getCauseF();

    /**
     * Returns the value of the '<em><b>Cause E</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaError}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Cause E</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Cause E</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_CauseE()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaError> getCauseE();

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_Description()
     * @model unique="false" dataType="org.eclipse.uml2.types.String" ordered="false"
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getDescription <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

    /**
     * Returns the value of the '<em><b>Domain</b></em>' attribute.
     * The default value is <code>"content"</code>.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Domain}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Domain</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Domain</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Domain
     * @see #setDomain(Domain)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_Domain()
     * @model default="content" unique="false" ordered="false"
     * @generated
     */
    Domain getDomain();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getDomain <em>Domain</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Domain</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Domain
     * @see #getDomain()
     * @generated
     */
    void setDomain(Domain value);

    /**
     * Returns the value of the '<em><b>Consequence</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Consequence</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Consequence</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_Consequence()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaCriticalLevel> getConsequence();

    /**
     * Returns the value of the '<em><b>Cost</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Cost</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Cost</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_Cost()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaCurrency> getCost();

    /**
     * Returns the value of the '<em><b>Consistency</b></em>' attribute.
     * The default value is <code>"consistent"</code>.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Consistency}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Consistency</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Consistency</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Consistency
     * @see #setConsistency(Consistency)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_Consistency()
     * @model default="consistent" unique="false" ordered="false"
     * @generated
     */
    Consistency getConsistency();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getConsistency <em>Consistency</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Consistency</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Consistency
     * @see #getConsistency()
     * @generated
     */
    void setConsistency(Consistency value);

    /**
     * Returns the value of the '<em><b>Detectability</b></em>' attribute.
     * The default value is <code>"signaled"</code>.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Detectability}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Detectability</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Detectability</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Detectability
     * @see #setDetectability(Detectability)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaFailure_Detectability()
     * @model default="signaled" unique="false" ordered="false"
     * @generated
     */
    Detectability getDetectability();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure#getDetectability <em>Detectability</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Detectability</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Detectability
     * @see #getDetectability()
     * @generated
     */
    void setDetectability(Detectability value);

} // DaFailure
