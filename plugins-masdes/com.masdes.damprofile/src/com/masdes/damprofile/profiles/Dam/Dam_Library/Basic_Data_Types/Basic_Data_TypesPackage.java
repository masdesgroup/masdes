/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesFactory
 * @model kind="package"
 * @generated
 */
public interface Basic_Data_TypesPackage extends EPackage {
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNAME = "Basic_Data_Types";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_URI = "http://com.masdes.damprofile/libraries/Basic_Data_Types/1.0";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    String eNS_PREFIX = "Dam.Dam_Library.Basic_Data_Types";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    Basic_Data_TypesPackage eINSTANCE = com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl.init();

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaLevelImpl <em>Da Level</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaLevelImpl
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl#getDaLevel()
     * @generated
     */
    int DA_LEVEL = 0;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LEVEL__EXPR = BasicNFP_TypesPackage.NFP_COMMON_TYPE__EXPR;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LEVEL__SOURCE = BasicNFP_TypesPackage.NFP_COMMON_TYPE__SOURCE;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LEVEL__STAT_Q = BasicNFP_TypesPackage.NFP_COMMON_TYPE__STAT_Q;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LEVEL__DIR = BasicNFP_TypesPackage.NFP_COMMON_TYPE__DIR;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LEVEL__MODE = BasicNFP_TypesPackage.NFP_COMMON_TYPE__MODE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LEVEL__VALUE = BasicNFP_TypesPackage.NFP_COMMON_TYPE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Da Level</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LEVEL_FEATURE_COUNT = BasicNFP_TypesPackage.NFP_COMMON_TYPE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaCriticalLevelImpl <em>Da Critical Level</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaCriticalLevelImpl
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl#getDaCriticalLevel()
     * @generated
     */
    int DA_CRITICAL_LEVEL = 1;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CRITICAL_LEVEL__EXPR = BasicNFP_TypesPackage.NFP_COMMON_TYPE__EXPR;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CRITICAL_LEVEL__SOURCE = BasicNFP_TypesPackage.NFP_COMMON_TYPE__SOURCE;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CRITICAL_LEVEL__STAT_Q = BasicNFP_TypesPackage.NFP_COMMON_TYPE__STAT_Q;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CRITICAL_LEVEL__DIR = BasicNFP_TypesPackage.NFP_COMMON_TYPE__DIR;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CRITICAL_LEVEL__MODE = BasicNFP_TypesPackage.NFP_COMMON_TYPE__MODE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CRITICAL_LEVEL__VALUE = BasicNFP_TypesPackage.NFP_COMMON_TYPE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Da Critical Level</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CRITICAL_LEVEL_FEATURE_COUNT = BasicNFP_TypesPackage.NFP_COMMON_TYPE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaCurrencyImpl <em>Da Currency</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaCurrencyImpl
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl#getDaCurrency()
     * @generated
     */
    int DA_CURRENCY = 2;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CURRENCY__EXPR = BasicNFP_TypesPackage.NFP_COMMON_TYPE__EXPR;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CURRENCY__SOURCE = BasicNFP_TypesPackage.NFP_COMMON_TYPE__SOURCE;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CURRENCY__STAT_Q = BasicNFP_TypesPackage.NFP_COMMON_TYPE__STAT_Q;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CURRENCY__DIR = BasicNFP_TypesPackage.NFP_COMMON_TYPE__DIR;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CURRENCY__MODE = BasicNFP_TypesPackage.NFP_COMMON_TYPE__MODE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CURRENCY__VALUE = BasicNFP_TypesPackage.NFP_COMMON_TYPE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Da Currency</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_CURRENCY_FEATURE_COUNT = BasicNFP_TypesPackage.NFP_COMMON_TYPE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaLikelihoodImpl <em>Da Likelihood</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaLikelihoodImpl
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl#getDaLikelihood()
     * @generated
     */
    int DA_LIKELIHOOD = 3;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LIKELIHOOD__EXPR = BasicNFP_TypesPackage.NFP_COMMON_TYPE__EXPR;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LIKELIHOOD__SOURCE = BasicNFP_TypesPackage.NFP_COMMON_TYPE__SOURCE;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LIKELIHOOD__STAT_Q = BasicNFP_TypesPackage.NFP_COMMON_TYPE__STAT_Q;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LIKELIHOOD__DIR = BasicNFP_TypesPackage.NFP_COMMON_TYPE__DIR;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LIKELIHOOD__MODE = BasicNFP_TypesPackage.NFP_COMMON_TYPE__MODE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LIKELIHOOD__VALUE = BasicNFP_TypesPackage.NFP_COMMON_TYPE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Da Likelihood</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_LIKELIHOOD_FEATURE_COUNT = BasicNFP_TypesPackage.NFP_COMMON_TYPE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaFrequencyImpl <em>Da Frequency</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaFrequencyImpl
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl#getDaFrequency()
     * @generated
     */
    int DA_FREQUENCY = 4;

    /**
     * The feature id for the '<em><b>Expr</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_FREQUENCY__EXPR = BasicNFP_TypesPackage.NFP_REAL__EXPR;

    /**
     * The feature id for the '<em><b>Source</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_FREQUENCY__SOURCE = BasicNFP_TypesPackage.NFP_REAL__SOURCE;

    /**
     * The feature id for the '<em><b>Stat Q</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_FREQUENCY__STAT_Q = BasicNFP_TypesPackage.NFP_REAL__STAT_Q;

    /**
     * The feature id for the '<em><b>Dir</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_FREQUENCY__DIR = BasicNFP_TypesPackage.NFP_REAL__DIR;

    /**
     * The feature id for the '<em><b>Mode</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_FREQUENCY__MODE = BasicNFP_TypesPackage.NFP_REAL__MODE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_FREQUENCY__VALUE = BasicNFP_TypesPackage.NFP_REAL__VALUE;

    /**
     * The feature id for the '<em><b>Unit</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_FREQUENCY__UNIT = BasicNFP_TypesPackage.NFP_REAL_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Precision</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_FREQUENCY__PRECISION = BasicNFP_TypesPackage.NFP_REAL_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Da Frequency</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int DA_FREQUENCY_FEATURE_COUNT = BasicNFP_TypesPackage.NFP_REAL_FEATURE_COUNT + 2;


    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel <em>Da Level</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Da Level</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel
     * @generated
     */
    EClass getDaLevel();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel#getValue()
     * @see #getDaLevel()
     * @generated
     */
    EAttribute getDaLevel_Value();

    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel <em>Da Critical Level</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Da Critical Level</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel
     * @generated
     */
    EClass getDaCriticalLevel();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCriticalLevel#getValue()
     * @see #getDaCriticalLevel()
     * @generated
     */
    EAttribute getDaCriticalLevel_Value();

    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency <em>Da Currency</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Da Currency</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency
     * @generated
     */
    EClass getDaCurrency();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaCurrency#getValue()
     * @see #getDaCurrency()
     * @generated
     */
    EAttribute getDaCurrency_Value();

    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLikelihood <em>Da Likelihood</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Da Likelihood</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLikelihood
     * @generated
     */
    EClass getDaLikelihood();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLikelihood#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLikelihood#getValue()
     * @see #getDaLikelihood()
     * @generated
     */
    EAttribute getDaLikelihood_Value();

    /**
     * Returns the meta object for class '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency <em>Da Frequency</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Da Frequency</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency
     * @generated
     */
    EClass getDaFrequency();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency#getUnit <em>Unit</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Unit</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency#getUnit()
     * @see #getDaFrequency()
     * @generated
     */
    EAttribute getDaFrequency_Unit();

    /**
     * Returns the meta object for the attribute '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency#getPrecision <em>Precision</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Precision</em>'.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency#getPrecision()
     * @see #getDaFrequency()
     * @generated
     */
    EAttribute getDaFrequency_Precision();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
    Basic_Data_TypesFactory getBasic_Data_TypesFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     *   <li>each class,</li>
     *   <li>each feature of each class,</li>
     *   <li>each enum,</li>
     *   <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaLevelImpl <em>Da Level</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaLevelImpl
         * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl#getDaLevel()
         * @generated
         */
        EClass DA_LEVEL = eINSTANCE.getDaLevel();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute DA_LEVEL__VALUE = eINSTANCE.getDaLevel_Value();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaCriticalLevelImpl <em>Da Critical Level</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaCriticalLevelImpl
         * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl#getDaCriticalLevel()
         * @generated
         */
        EClass DA_CRITICAL_LEVEL = eINSTANCE.getDaCriticalLevel();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute DA_CRITICAL_LEVEL__VALUE = eINSTANCE.getDaCriticalLevel_Value();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaCurrencyImpl <em>Da Currency</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaCurrencyImpl
         * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl#getDaCurrency()
         * @generated
         */
        EClass DA_CURRENCY = eINSTANCE.getDaCurrency();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute DA_CURRENCY__VALUE = eINSTANCE.getDaCurrency_Value();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaLikelihoodImpl <em>Da Likelihood</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaLikelihoodImpl
         * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl#getDaLikelihood()
         * @generated
         */
        EClass DA_LIKELIHOOD = eINSTANCE.getDaLikelihood();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute DA_LIKELIHOOD__VALUE = eINSTANCE.getDaLikelihood_Value();

        /**
         * The meta object literal for the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaFrequencyImpl <em>Da Frequency</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.DaFrequencyImpl
         * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesPackageImpl#getDaFrequency()
         * @generated
         */
        EClass DA_FREQUENCY = eINSTANCE.getDaFrequency();

        /**
         * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute DA_FREQUENCY__UNIT = eINSTANCE.getDaFrequency_Unit();

        /**
         * The meta object literal for the '<em><b>Precision</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute DA_FREQUENCY__PRECISION = eINSTANCE.getDaFrequency_Precision();

    }

} //Basic_Data_TypesPackage
