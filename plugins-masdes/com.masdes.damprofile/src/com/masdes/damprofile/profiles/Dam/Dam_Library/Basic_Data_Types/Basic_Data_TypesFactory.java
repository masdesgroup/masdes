/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage
 * @generated
 */
public interface Basic_Data_TypesFactory extends EFactory {
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    Basic_Data_TypesFactory eINSTANCE = com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.impl.Basic_Data_TypesFactoryImpl.init();

    /**
     * Returns a new object of class '<em>Da Level</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Da Level</em>'.
     * @generated
     */
    DaLevel createDaLevel();

    /**
     * Returns a new object of class '<em>Da Critical Level</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Da Critical Level</em>'.
     * @generated
     */
    DaCriticalLevel createDaCriticalLevel();

    /**
     * Returns a new object of class '<em>Da Currency</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Da Currency</em>'.
     * @generated
     */
    DaCurrency createDaCurrency();

    /**
     * Returns a new object of class '<em>Da Likelihood</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Da Likelihood</em>'.
     * @generated
     */
    DaLikelihood createDaLikelihood();

    /**
     * Returns a new object of class '<em>Da Frequency</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Da Frequency</em>'.
     * @generated
     */
    DaFrequency createDaFrequency();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
    Basic_Data_TypesPackage getBasic_Data_TypesPackage();

} //Basic_Data_TypesFactory
