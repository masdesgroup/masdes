/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.util;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.*;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage
 * @generated
 */
public class Basic_Data_TypesSwitch<T> extends Switch<T> {
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static Basic_Data_TypesPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Basic_Data_TypesSwitch() {
        if (modelPackage == null) {
            modelPackage = Basic_Data_TypesPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @parameter ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case Basic_Data_TypesPackage.DA_LEVEL: {
                DaLevel daLevel = (DaLevel)theEObject;
                T result = caseDaLevel(daLevel);
                if (result == null) result = caseNFP_CommonType(daLevel);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case Basic_Data_TypesPackage.DA_CRITICAL_LEVEL: {
                DaCriticalLevel daCriticalLevel = (DaCriticalLevel)theEObject;
                T result = caseDaCriticalLevel(daCriticalLevel);
                if (result == null) result = caseNFP_CommonType(daCriticalLevel);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case Basic_Data_TypesPackage.DA_CURRENCY: {
                DaCurrency daCurrency = (DaCurrency)theEObject;
                T result = caseDaCurrency(daCurrency);
                if (result == null) result = caseNFP_CommonType(daCurrency);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case Basic_Data_TypesPackage.DA_LIKELIHOOD: {
                DaLikelihood daLikelihood = (DaLikelihood)theEObject;
                T result = caseDaLikelihood(daLikelihood);
                if (result == null) result = caseNFP_CommonType(daLikelihood);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case Basic_Data_TypesPackage.DA_FREQUENCY: {
                DaFrequency daFrequency = (DaFrequency)theEObject;
                T result = caseDaFrequency(daFrequency);
                if (result == null) result = caseNFP_Real(daFrequency);
                if (result == null) result = caseNFP_CommonType(daFrequency);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Da Level</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Da Level</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDaLevel(DaLevel object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Da Critical Level</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Da Critical Level</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDaCriticalLevel(DaCriticalLevel object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Da Currency</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Da Currency</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDaCurrency(DaCurrency object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Da Likelihood</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Da Likelihood</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDaLikelihood(DaLikelihood object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Da Frequency</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Da Frequency</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDaFrequency(DaFrequency object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>NFP Common Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>NFP Common Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNFP_CommonType(NFP_CommonType object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>NFP Real</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>NFP Real</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNFP_Real(NFP_Real object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase(EObject object) {
        return null;
    }

} //Basic_Data_TypesSwitch
