/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.util;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage
 * @generated
 */
public class BasicNFP_TypesSwitch<T> extends Switch<T> {
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static BasicNFP_TypesPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public BasicNFP_TypesSwitch() {
        if (modelPackage == null) {
            modelPackage = BasicNFP_TypesPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @parameter ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case BasicNFP_TypesPackage.NFP_REAL: {
                NFP_Real nfP_Real = (NFP_Real)theEObject;
                T result = caseNFP_Real(nfP_Real);
                if (result == null) result = caseNFP_CommonType(nfP_Real);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case BasicNFP_TypesPackage.NFP_PERCENTAGE: {
                NFP_Percentage nfP_Percentage = (NFP_Percentage)theEObject;
                T result = caseNFP_Percentage(nfP_Percentage);
                if (result == null) result = caseNFP_Real(nfP_Percentage);
                if (result == null) result = caseNFP_CommonType(nfP_Percentage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case BasicNFP_TypesPackage.NFP_DURATION: {
                NFP_Duration nfP_Duration = (NFP_Duration)theEObject;
                T result = caseNFP_Duration(nfP_Duration);
                if (result == null) result = caseNFP_Real(nfP_Duration);
                if (result == null) result = caseNFP_CommonType(nfP_Duration);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case BasicNFP_TypesPackage.NFP_INTEGER: {
                NFP_Integer nfP_Integer = (NFP_Integer)theEObject;
                T result = caseNFP_Integer(nfP_Integer);
                if (result == null) result = caseNFP_CommonType(nfP_Integer);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case BasicNFP_TypesPackage.NFP_BOOLEAN: {
                NFP_Boolean nfP_Boolean = (NFP_Boolean)theEObject;
                T result = caseNFP_Boolean(nfP_Boolean);
                if (result == null) result = caseNFP_CommonType(nfP_Boolean);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case BasicNFP_TypesPackage.NFP_COMMON_TYPE: {
                NFP_CommonType nfP_CommonType = (NFP_CommonType)theEObject;
                T result = caseNFP_CommonType(nfP_CommonType);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>NFP Real</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>NFP Real</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNFP_Real(NFP_Real object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>NFP Percentage</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>NFP Percentage</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNFP_Percentage(NFP_Percentage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>NFP Duration</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>NFP Duration</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNFP_Duration(NFP_Duration object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>NFP Integer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>NFP Integer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNFP_Integer(NFP_Integer object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>NFP Boolean</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>NFP Boolean</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNFP_Boolean(NFP_Boolean object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>NFP Common Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>NFP Common Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNFP_CommonType(NFP_CommonType object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase(EObject object) {
        return null;
    }

} //BasicNFP_TypesSwitch
