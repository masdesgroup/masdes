/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Rec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRec#getRate <em>Rate</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRec#getDuration <em>Duration</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRec#getDistribution <em>Distribution</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRec#getCoverageFactor <em>Coverage Factor</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaRec()
 * @model
 * @generated
 */
public interface DaRec extends EObject {
    /**
     * Returns the value of the '<em><b>Rate</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaFrequency}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Rate</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Rate</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaRec_Rate()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaFrequency> getRate();

    /**
     * Returns the value of the '<em><b>Duration</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Duration</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Duration</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaRec_Duration()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Duration> getDuration();

    /**
     * Returns the value of the '<em><b>Distribution</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Distribution</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Distribution</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaRec_Distribution()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_CommonType> getDistribution();

    /**
     * Returns the value of the '<em><b>Coverage Factor</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Coverage Factor</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Coverage Factor</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage#getDaRec_CoverageFactor()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getCoverageFactor();

} // DaRec
