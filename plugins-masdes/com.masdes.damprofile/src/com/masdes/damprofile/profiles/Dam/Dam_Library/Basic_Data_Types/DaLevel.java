/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Level;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Level</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage#getDaLevel()
 * @model
 * @generated
 */
public interface DaLevel extends NFP_CommonType {
    /**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * The default value is <code>"veryHigh"</code>.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Level}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Value</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Level
     * @see #setValue(Level)
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.Basic_Data_TypesPackage#getDaLevel_Value()
     * @model default="veryHigh" required="true" ordered="false"
     * @generated
     */
    Level getValue();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel#getValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Level
     * @see #getValue()
     * @generated
     */
    void setValue(Level value);

} // DaLevel
