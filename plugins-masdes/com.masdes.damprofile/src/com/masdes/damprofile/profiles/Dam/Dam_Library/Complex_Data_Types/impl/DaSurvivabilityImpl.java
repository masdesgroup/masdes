/**
 */
package com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.impl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.MetricName;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.Complex_Data_TypesPackage;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaSurvivability;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Survivability</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.impl.DaSurvivabilityImpl#getMetric <em>Metric</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.impl.DaSurvivabilityImpl#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaSurvivabilityImpl extends EObjectImpl implements DaSurvivability {
    /**
     * The default value of the '{@link #getMetric() <em>Metric</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMetric()
     * @generated
     * @ordered
     */
    protected static final MetricName METRIC_EDEFAULT = MetricName.SS_AVAIL;

    /**
     * The cached value of the '{@link #getMetric() <em>Metric</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMetric()
     * @generated
     * @ordered
     */
    protected MetricName metric = METRIC_EDEFAULT;

    /**
     * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected EList<NFP_CommonType> value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaSurvivabilityImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return Complex_Data_TypesPackage.Literals.DA_SURVIVABILITY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public MetricName getMetric() {
        return metric;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setMetric(MetricName newMetric) {
        MetricName oldMetric = metric;
        metric = newMetric == null ? METRIC_EDEFAULT : newMetric;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, Complex_Data_TypesPackage.DA_SURVIVABILITY__METRIC, oldMetric, metric));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_CommonType> getValue() {
        if (value == null) {
            value = new EObjectContainmentEList<NFP_CommonType>(NFP_CommonType.class, this, Complex_Data_TypesPackage.DA_SURVIVABILITY__VALUE);
        }
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case Complex_Data_TypesPackage.DA_SURVIVABILITY__VALUE:
                return ((InternalEList<?>)getValue()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case Complex_Data_TypesPackage.DA_SURVIVABILITY__METRIC:
                return getMetric();
            case Complex_Data_TypesPackage.DA_SURVIVABILITY__VALUE:
                return getValue();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case Complex_Data_TypesPackage.DA_SURVIVABILITY__METRIC:
                setMetric((MetricName)newValue);
                return;
            case Complex_Data_TypesPackage.DA_SURVIVABILITY__VALUE:
                getValue().clear();
                getValue().addAll((Collection<? extends NFP_CommonType>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case Complex_Data_TypesPackage.DA_SURVIVABILITY__METRIC:
                setMetric(METRIC_EDEFAULT);
                return;
            case Complex_Data_TypesPackage.DA_SURVIVABILITY__VALUE:
                getValue().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case Complex_Data_TypesPackage.DA_SURVIVABILITY__METRIC:
                return metric != METRIC_EDEFAULT;
            case Complex_Data_TypesPackage.DA_SURVIVABILITY__VALUE:
                return value != null && !value.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (metric: ");
        result.append(metric);
        result.append(')');
        return result.toString();
    }

} //DaSurvivabilityImpl
