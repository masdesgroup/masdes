/**
 */
package com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.util;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage
 * @generated
 */
public class BasicNFP_TypesAdapterFactory extends AdapterFactoryImpl {
    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static BasicNFP_TypesPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public BasicNFP_TypesAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = BasicNFP_TypesPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject)object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected BasicNFP_TypesSwitch<Adapter> modelSwitch =
        new BasicNFP_TypesSwitch<Adapter>() {
            @Override
            public Adapter caseNFP_Real(NFP_Real object) {
                return createNFP_RealAdapter();
            }
            @Override
            public Adapter caseNFP_Percentage(NFP_Percentage object) {
                return createNFP_PercentageAdapter();
            }
            @Override
            public Adapter caseNFP_Duration(NFP_Duration object) {
                return createNFP_DurationAdapter();
            }
            @Override
            public Adapter caseNFP_Integer(NFP_Integer object) {
                return createNFP_IntegerAdapter();
            }
            @Override
            public Adapter caseNFP_Boolean(NFP_Boolean object) {
                return createNFP_BooleanAdapter();
            }
            @Override
            public Adapter caseNFP_CommonType(NFP_CommonType object) {
                return createNFP_CommonTypeAdapter();
            }
            @Override
            public Adapter defaultCase(EObject object) {
                return createEObjectAdapter();
            }
        };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject)target);
    }


    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real <em>NFP Real</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real
     * @generated
     */
    public Adapter createNFP_RealAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage <em>NFP Percentage</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage
     * @generated
     */
    public Adapter createNFP_PercentageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration <em>NFP Duration</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Duration
     * @generated
     */
    public Adapter createNFP_DurationAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer <em>NFP Integer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer
     * @generated
     */
    public Adapter createNFP_IntegerAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean <em>NFP Boolean</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean
     * @generated
     */
    public Adapter createNFP_BooleanAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType <em>NFP Common Type</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType
     * @generated
     */
    public Adapter createNFP_CommonTypeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} //BasicNFP_TypesAdapterFactory
