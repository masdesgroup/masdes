/**
 */
package com.masdes.damprofile.profiles.Core;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRec;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaScenario;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getInstAvail <em>Inst Avail</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getUnreliability <em>Unreliability</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getReliability <em>Reliability</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getMissionTime <em>Mission Time</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getExecProb <em>Exec Prob</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getComplexity <em>Complexity</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getSsAvail <em>Ss Avail</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getRecovery <em>Recovery</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getAvailLevel <em>Avail Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getReliabLevel <em>Reliab Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getSafetyLevel <em>Safety Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getConfLevel <em>Conf Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getIntegLevel <em>Integ Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getFailure <em>Failure</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaService#getHazard <em>Hazard</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService()
 * @model
 * @generated
 */
public interface DaService extends GaScenario {
    /**
     * Returns the value of the '<em><b>Inst Avail</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Inst Avail</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Inst Avail</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_InstAvail()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_CommonType> getInstAvail();

    /**
     * Returns the value of the '<em><b>Unreliability</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Unreliability</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Unreliability</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_Unreliability()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_CommonType> getUnreliability();

    /**
     * Returns the value of the '<em><b>Reliability</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Reliability</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Reliability</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_Reliability()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_CommonType> getReliability();

    /**
     * Returns the value of the '<em><b>Mission Time</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Mission Time</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Mission Time</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_MissionTime()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_CommonType> getMissionTime();

    /**
     * Returns the value of the '<em><b>Exec Prob</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Exec Prob</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Exec Prob</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_ExecProb()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getExecProb();

    /**
     * Returns the value of the '<em><b>Complexity</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Complexity</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Complexity</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_Complexity()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getComplexity();

    /**
     * Returns the value of the '<em><b>Ss Avail</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ss Avail</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Ss Avail</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_SsAvail()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Percentage> getSsAvail();

    /**
     * Returns the value of the '<em><b>Recovery</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRec}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Recovery</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Recovery</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_Recovery()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaRec> getRecovery();

    /**
     * Returns the value of the '<em><b>Avail Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Avail Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Avail Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_AvailLevel()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLevel> getAvailLevel();

    /**
     * Returns the value of the '<em><b>Reliab Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Reliab Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Reliab Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_ReliabLevel()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLevel> getReliabLevel();

    /**
     * Returns the value of the '<em><b>Safety Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Safety Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Safety Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_SafetyLevel()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLevel> getSafetyLevel();

    /**
     * Returns the value of the '<em><b>Conf Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Conf Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Conf Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_ConfLevel()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLevel> getConfLevel();

    /**
     * Returns the value of the '<em><b>Integ Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Integ Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Integ Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_IntegLevel()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLevel> getIntegLevel();

    /**
     * Returns the value of the '<em><b>Failure</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Failure</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Failure</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_Failure()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaFailure> getFailure();

    /**
     * Returns the value of the '<em><b>Hazard</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Hazard</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Hazard</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaService_Hazard()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaHazard> getHazard();

} // DaService
