/**
 */
package com.masdes.damprofile.profiles.Core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Recovery</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaRecovery()
 * @model
 * @generated
 */
public interface DaRecovery extends DaService {
} // DaRecovery
