/**
 */
package com.masdes.damprofile.profiles.Core.impl;

import com.masdes.damprofile.profiles.Core.CorePackage;
import com.masdes.damprofile.profiles.Core.DaRecognition;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Recognition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DaRecognitionImpl extends DaServiceImpl implements DaRecognition {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaRecognitionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.DA_RECOGNITION;
    }

} //DaRecognitionImpl
