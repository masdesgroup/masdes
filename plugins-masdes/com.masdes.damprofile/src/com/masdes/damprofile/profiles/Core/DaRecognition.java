/**
 */
package com.masdes.damprofile.profiles.Core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Recognition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaRecognition()
 * @model
 * @generated
 */
public interface DaRecognition extends DaService {
} // DaRecognition
