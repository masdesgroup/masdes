/**
 */
package com.masdes.damprofile.profiles.Core;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Service Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getBase_Classifier <em>Base Classifier</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getBase_Lifeline <em>Base Lifeline</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getBase_Interaction <em>Base Interaction</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getBase_InstanceSpecification <em>Base Instance Specification</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getRequest <em>Request</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getAccessProb <em>Access Prob</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getServiceProb <em>Service Prob</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaServiceRequest()
 * @model
 * @generated
 */
public interface DaServiceRequest extends EObject {
    /**
     * Returns the value of the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Classifier</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Base Classifier</em>' reference.
     * @see #setBase_Classifier(Classifier)
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaServiceRequest_Base_Classifier()
     * @model required="true" ordered="false"
     * @generated
     */
    Classifier getBase_Classifier();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getBase_Classifier <em>Base Classifier</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Base Classifier</em>' reference.
     * @see #getBase_Classifier()
     * @generated
     */
    void setBase_Classifier(Classifier value);

    /**
     * Returns the value of the '<em><b>Base Lifeline</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Lifeline</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Base Lifeline</em>' reference.
     * @see #setBase_Lifeline(Lifeline)
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaServiceRequest_Base_Lifeline()
     * @model required="true" ordered="false"
     * @generated
     */
    Lifeline getBase_Lifeline();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getBase_Lifeline <em>Base Lifeline</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Base Lifeline</em>' reference.
     * @see #getBase_Lifeline()
     * @generated
     */
    void setBase_Lifeline(Lifeline value);

    /**
     * Returns the value of the '<em><b>Base Interaction</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Interaction</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Base Interaction</em>' reference.
     * @see #setBase_Interaction(Interaction)
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaServiceRequest_Base_Interaction()
     * @model required="true" ordered="false"
     * @generated
     */
    Interaction getBase_Interaction();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getBase_Interaction <em>Base Interaction</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Base Interaction</em>' reference.
     * @see #getBase_Interaction()
     * @generated
     */
    void setBase_Interaction(Interaction value);

    /**
     * Returns the value of the '<em><b>Base Instance Specification</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Instance Specification</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Base Instance Specification</em>' reference.
     * @see #setBase_InstanceSpecification(InstanceSpecification)
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaServiceRequest_Base_InstanceSpecification()
     * @model required="true" ordered="false"
     * @generated
     */
    InstanceSpecification getBase_InstanceSpecification();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Core.DaServiceRequest#getBase_InstanceSpecification <em>Base Instance Specification</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Base Instance Specification</em>' reference.
     * @see #getBase_InstanceSpecification()
     * @generated
     */
    void setBase_InstanceSpecification(InstanceSpecification value);

    /**
     * Returns the value of the '<em><b>Request</b></em>' reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Core.DaService}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Request</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Request</em>' reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaServiceRequest_Request()
     * @model
     * @generated
     */
    EList<DaService> getRequest();

    /**
     * Returns the value of the '<em><b>Access Prob</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Access Prob</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Access Prob</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaServiceRequest_AccessProb()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getAccessProb();

    /**
     * Returns the value of the '<em><b>Service Prob</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Service Prob</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Service Prob</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaServiceRequest_ServiceProb()
     * @model containment="true"
     * @generated
     */
    EList<NFP_Real> getServiceProb();

} // DaServiceRequest
