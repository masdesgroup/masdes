/**
 */
package com.masdes.damprofile.profiles.Core.impl;

import com.masdes.damprofile.profiles.Core.CorePackage;
import com.masdes.damprofile.profiles.Core.DaRecovery;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Recovery</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DaRecoveryImpl extends DaServiceImpl implements DaRecovery {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaRecoveryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.DA_RECOVERY;
    }

} //DaRecoveryImpl
