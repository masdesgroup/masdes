/**
 */
package com.masdes.damprofile.profiles.Core;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Resistance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaResistance#getCoverage <em>Coverage</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaResistance()
 * @model
 * @generated
 */
public interface DaResistance extends DaService {
    /**
     * Returns the value of the '<em><b>Coverage</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Coverage</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Coverage</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaResistance_Coverage()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Percentage> getCoverage();

} // DaResistance
