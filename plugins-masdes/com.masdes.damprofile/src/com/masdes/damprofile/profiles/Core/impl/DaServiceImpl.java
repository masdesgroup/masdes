/**
 */
package com.masdes.damprofile.profiles.Core.impl;

import com.masdes.damprofile.profiles.Core.CorePackage;
import com.masdes.damprofile.profiles.Core.DaService;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRec;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaScenarioImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Service</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getInstAvail <em>Inst Avail</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getUnreliability <em>Unreliability</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getReliability <em>Reliability</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getMissionTime <em>Mission Time</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getExecProb <em>Exec Prob</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getComplexity <em>Complexity</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getSsAvail <em>Ss Avail</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getRecovery <em>Recovery</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getAvailLevel <em>Avail Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getReliabLevel <em>Reliab Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getSafetyLevel <em>Safety Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getConfLevel <em>Conf Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getIntegLevel <em>Integ Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getFailure <em>Failure</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaServiceImpl#getHazard <em>Hazard</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaServiceImpl extends GaScenarioImpl implements DaService {
    /**
     * The cached value of the '{@link #getInstAvail() <em>Inst Avail</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getInstAvail()
     * @generated
     * @ordered
     */
    protected EList<NFP_CommonType> instAvail;

    /**
     * The cached value of the '{@link #getUnreliability() <em>Unreliability</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getUnreliability()
     * @generated
     * @ordered
     */
    protected EList<NFP_CommonType> unreliability;

    /**
     * The cached value of the '{@link #getReliability() <em>Reliability</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReliability()
     * @generated
     * @ordered
     */
    protected EList<NFP_CommonType> reliability;

    /**
     * The cached value of the '{@link #getMissionTime() <em>Mission Time</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMissionTime()
     * @generated
     * @ordered
     */
    protected EList<NFP_CommonType> missionTime;

    /**
     * The cached value of the '{@link #getExecProb() <em>Exec Prob</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getExecProb()
     * @generated
     * @ordered
     */
    protected EList<NFP_Real> execProb;

    /**
     * The cached value of the '{@link #getComplexity() <em>Complexity</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getComplexity()
     * @generated
     * @ordered
     */
    protected EList<NFP_Real> complexity;

    /**
     * The cached value of the '{@link #getSsAvail() <em>Ss Avail</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSsAvail()
     * @generated
     * @ordered
     */
    protected EList<NFP_Percentage> ssAvail;

    /**
     * The cached value of the '{@link #getRecovery() <em>Recovery</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getRecovery()
     * @generated
     * @ordered
     */
    protected EList<DaRec> recovery;

    /**
     * The cached value of the '{@link #getAvailLevel() <em>Avail Level</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getAvailLevel()
     * @generated
     * @ordered
     */
    protected EList<DaLevel> availLevel;

    /**
     * The cached value of the '{@link #getReliabLevel() <em>Reliab Level</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReliabLevel()
     * @generated
     * @ordered
     */
    protected EList<DaLevel> reliabLevel;

    /**
     * The cached value of the '{@link #getSafetyLevel() <em>Safety Level</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSafetyLevel()
     * @generated
     * @ordered
     */
    protected EList<DaLevel> safetyLevel;

    /**
     * The cached value of the '{@link #getConfLevel() <em>Conf Level</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getConfLevel()
     * @generated
     * @ordered
     */
    protected EList<DaLevel> confLevel;

    /**
     * The cached value of the '{@link #getIntegLevel() <em>Integ Level</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getIntegLevel()
     * @generated
     * @ordered
     */
    protected EList<DaLevel> integLevel;

    /**
     * The cached value of the '{@link #getFailure() <em>Failure</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFailure()
     * @generated
     * @ordered
     */
    protected EList<DaFailure> failure;

    /**
     * The cached value of the '{@link #getHazard() <em>Hazard</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getHazard()
     * @generated
     * @ordered
     */
    protected EList<DaHazard> hazard;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaServiceImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.DA_SERVICE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_CommonType> getInstAvail() {
        if (instAvail == null) {
            instAvail = new EObjectContainmentEList<NFP_CommonType>(NFP_CommonType.class, this, CorePackage.DA_SERVICE__INST_AVAIL);
        }
        return instAvail;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_CommonType> getUnreliability() {
        if (unreliability == null) {
            unreliability = new EObjectContainmentEList<NFP_CommonType>(NFP_CommonType.class, this, CorePackage.DA_SERVICE__UNRELIABILITY);
        }
        return unreliability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_CommonType> getReliability() {
        if (reliability == null) {
            reliability = new EObjectContainmentEList<NFP_CommonType>(NFP_CommonType.class, this, CorePackage.DA_SERVICE__RELIABILITY);
        }
        return reliability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_CommonType> getMissionTime() {
        if (missionTime == null) {
            missionTime = new EObjectContainmentEList<NFP_CommonType>(NFP_CommonType.class, this, CorePackage.DA_SERVICE__MISSION_TIME);
        }
        return missionTime;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Real> getExecProb() {
        if (execProb == null) {
            execProb = new EObjectContainmentEList<NFP_Real>(NFP_Real.class, this, CorePackage.DA_SERVICE__EXEC_PROB);
        }
        return execProb;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Real> getComplexity() {
        if (complexity == null) {
            complexity = new EObjectContainmentEList<NFP_Real>(NFP_Real.class, this, CorePackage.DA_SERVICE__COMPLEXITY);
        }
        return complexity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Percentage> getSsAvail() {
        if (ssAvail == null) {
            ssAvail = new EObjectContainmentEList<NFP_Percentage>(NFP_Percentage.class, this, CorePackage.DA_SERVICE__SS_AVAIL);
        }
        return ssAvail;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaRec> getRecovery() {
        if (recovery == null) {
            recovery = new EObjectContainmentEList<DaRec>(DaRec.class, this, CorePackage.DA_SERVICE__RECOVERY);
        }
        return recovery;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaLevel> getAvailLevel() {
        if (availLevel == null) {
            availLevel = new EObjectContainmentEList<DaLevel>(DaLevel.class, this, CorePackage.DA_SERVICE__AVAIL_LEVEL);
        }
        return availLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaLevel> getReliabLevel() {
        if (reliabLevel == null) {
            reliabLevel = new EObjectContainmentEList<DaLevel>(DaLevel.class, this, CorePackage.DA_SERVICE__RELIAB_LEVEL);
        }
        return reliabLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaLevel> getSafetyLevel() {
        if (safetyLevel == null) {
            safetyLevel = new EObjectContainmentEList<DaLevel>(DaLevel.class, this, CorePackage.DA_SERVICE__SAFETY_LEVEL);
        }
        return safetyLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaLevel> getConfLevel() {
        if (confLevel == null) {
            confLevel = new EObjectContainmentEList<DaLevel>(DaLevel.class, this, CorePackage.DA_SERVICE__CONF_LEVEL);
        }
        return confLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaLevel> getIntegLevel() {
        if (integLevel == null) {
            integLevel = new EObjectContainmentEList<DaLevel>(DaLevel.class, this, CorePackage.DA_SERVICE__INTEG_LEVEL);
        }
        return integLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaFailure> getFailure() {
        if (failure == null) {
            failure = new EObjectContainmentEList<DaFailure>(DaFailure.class, this, CorePackage.DA_SERVICE__FAILURE);
        }
        return failure;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaHazard> getHazard() {
        if (hazard == null) {
            hazard = new EObjectContainmentEList<DaHazard>(DaHazard.class, this, CorePackage.DA_SERVICE__HAZARD);
        }
        return hazard;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.DA_SERVICE__INST_AVAIL:
                return ((InternalEList<?>)getInstAvail()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__UNRELIABILITY:
                return ((InternalEList<?>)getUnreliability()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__RELIABILITY:
                return ((InternalEList<?>)getReliability()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__MISSION_TIME:
                return ((InternalEList<?>)getMissionTime()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__EXEC_PROB:
                return ((InternalEList<?>)getExecProb()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__COMPLEXITY:
                return ((InternalEList<?>)getComplexity()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__SS_AVAIL:
                return ((InternalEList<?>)getSsAvail()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__RECOVERY:
                return ((InternalEList<?>)getRecovery()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__AVAIL_LEVEL:
                return ((InternalEList<?>)getAvailLevel()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__RELIAB_LEVEL:
                return ((InternalEList<?>)getReliabLevel()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__SAFETY_LEVEL:
                return ((InternalEList<?>)getSafetyLevel()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__CONF_LEVEL:
                return ((InternalEList<?>)getConfLevel()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__INTEG_LEVEL:
                return ((InternalEList<?>)getIntegLevel()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__FAILURE:
                return ((InternalEList<?>)getFailure()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_SERVICE__HAZARD:
                return ((InternalEList<?>)getHazard()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.DA_SERVICE__INST_AVAIL:
                return getInstAvail();
            case CorePackage.DA_SERVICE__UNRELIABILITY:
                return getUnreliability();
            case CorePackage.DA_SERVICE__RELIABILITY:
                return getReliability();
            case CorePackage.DA_SERVICE__MISSION_TIME:
                return getMissionTime();
            case CorePackage.DA_SERVICE__EXEC_PROB:
                return getExecProb();
            case CorePackage.DA_SERVICE__COMPLEXITY:
                return getComplexity();
            case CorePackage.DA_SERVICE__SS_AVAIL:
                return getSsAvail();
            case CorePackage.DA_SERVICE__RECOVERY:
                return getRecovery();
            case CorePackage.DA_SERVICE__AVAIL_LEVEL:
                return getAvailLevel();
            case CorePackage.DA_SERVICE__RELIAB_LEVEL:
                return getReliabLevel();
            case CorePackage.DA_SERVICE__SAFETY_LEVEL:
                return getSafetyLevel();
            case CorePackage.DA_SERVICE__CONF_LEVEL:
                return getConfLevel();
            case CorePackage.DA_SERVICE__INTEG_LEVEL:
                return getIntegLevel();
            case CorePackage.DA_SERVICE__FAILURE:
                return getFailure();
            case CorePackage.DA_SERVICE__HAZARD:
                return getHazard();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.DA_SERVICE__INST_AVAIL:
                getInstAvail().clear();
                getInstAvail().addAll((Collection<? extends NFP_CommonType>)newValue);
                return;
            case CorePackage.DA_SERVICE__UNRELIABILITY:
                getUnreliability().clear();
                getUnreliability().addAll((Collection<? extends NFP_CommonType>)newValue);
                return;
            case CorePackage.DA_SERVICE__RELIABILITY:
                getReliability().clear();
                getReliability().addAll((Collection<? extends NFP_CommonType>)newValue);
                return;
            case CorePackage.DA_SERVICE__MISSION_TIME:
                getMissionTime().clear();
                getMissionTime().addAll((Collection<? extends NFP_CommonType>)newValue);
                return;
            case CorePackage.DA_SERVICE__EXEC_PROB:
                getExecProb().clear();
                getExecProb().addAll((Collection<? extends NFP_Real>)newValue);
                return;
            case CorePackage.DA_SERVICE__COMPLEXITY:
                getComplexity().clear();
                getComplexity().addAll((Collection<? extends NFP_Real>)newValue);
                return;
            case CorePackage.DA_SERVICE__SS_AVAIL:
                getSsAvail().clear();
                getSsAvail().addAll((Collection<? extends NFP_Percentage>)newValue);
                return;
            case CorePackage.DA_SERVICE__RECOVERY:
                getRecovery().clear();
                getRecovery().addAll((Collection<? extends DaRec>)newValue);
                return;
            case CorePackage.DA_SERVICE__AVAIL_LEVEL:
                getAvailLevel().clear();
                getAvailLevel().addAll((Collection<? extends DaLevel>)newValue);
                return;
            case CorePackage.DA_SERVICE__RELIAB_LEVEL:
                getReliabLevel().clear();
                getReliabLevel().addAll((Collection<? extends DaLevel>)newValue);
                return;
            case CorePackage.DA_SERVICE__SAFETY_LEVEL:
                getSafetyLevel().clear();
                getSafetyLevel().addAll((Collection<? extends DaLevel>)newValue);
                return;
            case CorePackage.DA_SERVICE__CONF_LEVEL:
                getConfLevel().clear();
                getConfLevel().addAll((Collection<? extends DaLevel>)newValue);
                return;
            case CorePackage.DA_SERVICE__INTEG_LEVEL:
                getIntegLevel().clear();
                getIntegLevel().addAll((Collection<? extends DaLevel>)newValue);
                return;
            case CorePackage.DA_SERVICE__FAILURE:
                getFailure().clear();
                getFailure().addAll((Collection<? extends DaFailure>)newValue);
                return;
            case CorePackage.DA_SERVICE__HAZARD:
                getHazard().clear();
                getHazard().addAll((Collection<? extends DaHazard>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.DA_SERVICE__INST_AVAIL:
                getInstAvail().clear();
                return;
            case CorePackage.DA_SERVICE__UNRELIABILITY:
                getUnreliability().clear();
                return;
            case CorePackage.DA_SERVICE__RELIABILITY:
                getReliability().clear();
                return;
            case CorePackage.DA_SERVICE__MISSION_TIME:
                getMissionTime().clear();
                return;
            case CorePackage.DA_SERVICE__EXEC_PROB:
                getExecProb().clear();
                return;
            case CorePackage.DA_SERVICE__COMPLEXITY:
                getComplexity().clear();
                return;
            case CorePackage.DA_SERVICE__SS_AVAIL:
                getSsAvail().clear();
                return;
            case CorePackage.DA_SERVICE__RECOVERY:
                getRecovery().clear();
                return;
            case CorePackage.DA_SERVICE__AVAIL_LEVEL:
                getAvailLevel().clear();
                return;
            case CorePackage.DA_SERVICE__RELIAB_LEVEL:
                getReliabLevel().clear();
                return;
            case CorePackage.DA_SERVICE__SAFETY_LEVEL:
                getSafetyLevel().clear();
                return;
            case CorePackage.DA_SERVICE__CONF_LEVEL:
                getConfLevel().clear();
                return;
            case CorePackage.DA_SERVICE__INTEG_LEVEL:
                getIntegLevel().clear();
                return;
            case CorePackage.DA_SERVICE__FAILURE:
                getFailure().clear();
                return;
            case CorePackage.DA_SERVICE__HAZARD:
                getHazard().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.DA_SERVICE__INST_AVAIL:
                return instAvail != null && !instAvail.isEmpty();
            case CorePackage.DA_SERVICE__UNRELIABILITY:
                return unreliability != null && !unreliability.isEmpty();
            case CorePackage.DA_SERVICE__RELIABILITY:
                return reliability != null && !reliability.isEmpty();
            case CorePackage.DA_SERVICE__MISSION_TIME:
                return missionTime != null && !missionTime.isEmpty();
            case CorePackage.DA_SERVICE__EXEC_PROB:
                return execProb != null && !execProb.isEmpty();
            case CorePackage.DA_SERVICE__COMPLEXITY:
                return complexity != null && !complexity.isEmpty();
            case CorePackage.DA_SERVICE__SS_AVAIL:
                return ssAvail != null && !ssAvail.isEmpty();
            case CorePackage.DA_SERVICE__RECOVERY:
                return recovery != null && !recovery.isEmpty();
            case CorePackage.DA_SERVICE__AVAIL_LEVEL:
                return availLevel != null && !availLevel.isEmpty();
            case CorePackage.DA_SERVICE__RELIAB_LEVEL:
                return reliabLevel != null && !reliabLevel.isEmpty();
            case CorePackage.DA_SERVICE__SAFETY_LEVEL:
                return safetyLevel != null && !safetyLevel.isEmpty();
            case CorePackage.DA_SERVICE__CONF_LEVEL:
                return confLevel != null && !confLevel.isEmpty();
            case CorePackage.DA_SERVICE__INTEG_LEVEL:
                return integLevel != null && !integLevel.isEmpty();
            case CorePackage.DA_SERVICE__FAILURE:
                return failure != null && !failure.isEmpty();
            case CorePackage.DA_SERVICE__HAZARD:
                return hazard != null && !hazard.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //DaServiceImpl
