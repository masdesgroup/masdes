/**
 */
package com.masdes.damprofile.profiles.Core.impl;

import com.masdes.damprofile.profiles.Core.CorePackage;
import com.masdes.damprofile.profiles.Core.DaComponent;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Origin;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaError;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRepair;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import com.masdes.damprofile.profiles.Redundancy.DaSpare;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.ResourceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getUnreliability <em>Unreliability</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getReliability <em>Reliability</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getMissionTime <em>Mission Time</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#isStateful <em>Stateful</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getOrigin <em>Origin</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getComplexity <em>Complexity</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getSsAvail <em>Ss Avail</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getFailureCoverage <em>Failure Coverage</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getPrecPermFault <em>Prec Perm Fault</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getAvailLevel <em>Avail Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getReliabLevel <em>Reliab Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getSafetyLevel <em>Safety Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getConfLevel <em>Conf Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getIntegLevel <em>Integ Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getError <em>Error</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getFault <em>Fault</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getRepair <em>Repair</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getFailure <em>Failure</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getHazard <em>Hazard</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.impl.DaComponentImpl#getSubstitutedBy <em>Substituted By</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaComponentImpl extends ResourceImpl implements DaComponent {
    /**
     * The cached value of the '{@link #getUnreliability() <em>Unreliability</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getUnreliability()
     * @generated
     * @ordered
     */
    protected EList<NFP_CommonType> unreliability;

    /**
     * The cached value of the '{@link #getReliability() <em>Reliability</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReliability()
     * @generated
     * @ordered
     */
    protected EList<NFP_CommonType> reliability;

    /**
     * The cached value of the '{@link #getMissionTime() <em>Mission Time</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMissionTime()
     * @generated
     * @ordered
     */
    protected EList<NFP_CommonType> missionTime;

    /**
     * The default value of the '{@link #isStateful() <em>Stateful</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isStateful()
     * @generated
     * @ordered
     */
    protected static final boolean STATEFUL_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isStateful() <em>Stateful</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isStateful()
     * @generated
     * @ordered
     */
    protected boolean stateful = STATEFUL_EDEFAULT;

    /**
     * The default value of the '{@link #getOrigin() <em>Origin</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getOrigin()
     * @generated
     * @ordered
     */
    protected static final Origin ORIGIN_EDEFAULT = Origin.HW;

    /**
     * The cached value of the '{@link #getOrigin() <em>Origin</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getOrigin()
     * @generated
     * @ordered
     */
    protected Origin origin = ORIGIN_EDEFAULT;

    /**
     * The cached value of the '{@link #getComplexity() <em>Complexity</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getComplexity()
     * @generated
     * @ordered
     */
    protected EList<NFP_Real> complexity;

    /**
     * The cached value of the '{@link #getSsAvail() <em>Ss Avail</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSsAvail()
     * @generated
     * @ordered
     */
    protected EList<NFP_Percentage> ssAvail;

    /**
     * The cached value of the '{@link #getFailureCoverage() <em>Failure Coverage</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFailureCoverage()
     * @generated
     * @ordered
     */
    protected EList<NFP_Percentage> failureCoverage;

    /**
     * The cached value of the '{@link #getPrecPermFault() <em>Prec Perm Fault</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getPrecPermFault()
     * @generated
     * @ordered
     */
    protected EList<NFP_Percentage> precPermFault;

    /**
     * The cached value of the '{@link #getAvailLevel() <em>Avail Level</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getAvailLevel()
     * @generated
     * @ordered
     */
    protected EList<DaLevel> availLevel;

    /**
     * The cached value of the '{@link #getReliabLevel() <em>Reliab Level</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReliabLevel()
     * @generated
     * @ordered
     */
    protected EList<DaLevel> reliabLevel;

    /**
     * The cached value of the '{@link #getSafetyLevel() <em>Safety Level</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSafetyLevel()
     * @generated
     * @ordered
     */
    protected EList<DaLevel> safetyLevel;

    /**
     * The cached value of the '{@link #getConfLevel() <em>Conf Level</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getConfLevel()
     * @generated
     * @ordered
     */
    protected EList<DaLevel> confLevel;

    /**
     * The cached value of the '{@link #getIntegLevel() <em>Integ Level</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getIntegLevel()
     * @generated
     * @ordered
     */
    protected EList<DaLevel> integLevel;

    /**
     * The cached value of the '{@link #getError() <em>Error</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getError()
     * @generated
     * @ordered
     */
    protected DaError error;

    /**
     * The cached value of the '{@link #getFault() <em>Fault</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFault()
     * @generated
     * @ordered
     */
    protected EList<DaFault> fault;

    /**
     * The cached value of the '{@link #getRepair() <em>Repair</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getRepair()
     * @generated
     * @ordered
     */
    protected EList<DaRepair> repair;

    /**
     * The cached value of the '{@link #getFailure() <em>Failure</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFailure()
     * @generated
     * @ordered
     */
    protected EList<DaFailure> failure;

    /**
     * The cached value of the '{@link #getHazard() <em>Hazard</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getHazard()
     * @generated
     * @ordered
     */
    protected EList<DaHazard> hazard;

    /**
     * The cached value of the '{@link #getSubstitutedBy() <em>Substituted By</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSubstitutedBy()
     * @generated
     * @ordered
     */
    protected EList<DaSpare> substitutedBy;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaComponentImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CorePackage.Literals.DA_COMPONENT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_CommonType> getUnreliability() {
        if (unreliability == null) {
            unreliability = new EObjectContainmentEList<NFP_CommonType>(NFP_CommonType.class, this, CorePackage.DA_COMPONENT__UNRELIABILITY);
        }
        return unreliability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_CommonType> getReliability() {
        if (reliability == null) {
            reliability = new EObjectContainmentEList<NFP_CommonType>(NFP_CommonType.class, this, CorePackage.DA_COMPONENT__RELIABILITY);
        }
        return reliability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_CommonType> getMissionTime() {
        if (missionTime == null) {
            missionTime = new EObjectContainmentEList<NFP_CommonType>(NFP_CommonType.class, this, CorePackage.DA_COMPONENT__MISSION_TIME);
        }
        return missionTime;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public boolean isStateful() {
        return stateful;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setStateful(boolean newStateful) {
        boolean oldStateful = stateful;
        stateful = newStateful;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DA_COMPONENT__STATEFUL, oldStateful, stateful));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Origin getOrigin() {
        return origin;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setOrigin(Origin newOrigin) {
        Origin oldOrigin = origin;
        origin = newOrigin == null ? ORIGIN_EDEFAULT : newOrigin;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DA_COMPONENT__ORIGIN, oldOrigin, origin));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Real> getComplexity() {
        if (complexity == null) {
            complexity = new EObjectContainmentEList<NFP_Real>(NFP_Real.class, this, CorePackage.DA_COMPONENT__COMPLEXITY);
        }
        return complexity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Percentage> getSsAvail() {
        if (ssAvail == null) {
            ssAvail = new EObjectContainmentEList<NFP_Percentage>(NFP_Percentage.class, this, CorePackage.DA_COMPONENT__SS_AVAIL);
        }
        return ssAvail;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Percentage> getFailureCoverage() {
        if (failureCoverage == null) {
            failureCoverage = new EObjectContainmentEList<NFP_Percentage>(NFP_Percentage.class, this, CorePackage.DA_COMPONENT__FAILURE_COVERAGE);
        }
        return failureCoverage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Percentage> getPrecPermFault() {
        if (precPermFault == null) {
            precPermFault = new EObjectContainmentEList<NFP_Percentage>(NFP_Percentage.class, this, CorePackage.DA_COMPONENT__PREC_PERM_FAULT);
        }
        return precPermFault;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaLevel> getAvailLevel() {
        if (availLevel == null) {
            availLevel = new EObjectContainmentEList<DaLevel>(DaLevel.class, this, CorePackage.DA_COMPONENT__AVAIL_LEVEL);
        }
        return availLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaLevel> getReliabLevel() {
        if (reliabLevel == null) {
            reliabLevel = new EObjectContainmentEList<DaLevel>(DaLevel.class, this, CorePackage.DA_COMPONENT__RELIAB_LEVEL);
        }
        return reliabLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaLevel> getSafetyLevel() {
        if (safetyLevel == null) {
            safetyLevel = new EObjectContainmentEList<DaLevel>(DaLevel.class, this, CorePackage.DA_COMPONENT__SAFETY_LEVEL);
        }
        return safetyLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaLevel> getConfLevel() {
        if (confLevel == null) {
            confLevel = new EObjectContainmentEList<DaLevel>(DaLevel.class, this, CorePackage.DA_COMPONENT__CONF_LEVEL);
        }
        return confLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaLevel> getIntegLevel() {
        if (integLevel == null) {
            integLevel = new EObjectContainmentEList<DaLevel>(DaLevel.class, this, CorePackage.DA_COMPONENT__INTEG_LEVEL);
        }
        return integLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DaError getError() {
        return error;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetError(DaError newError, NotificationChain msgs) {
        DaError oldError = error;
        error = newError;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.DA_COMPONENT__ERROR, oldError, newError);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setError(DaError newError) {
        if (newError != error) {
            NotificationChain msgs = null;
            if (error != null)
                msgs = ((InternalEObject)error).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.DA_COMPONENT__ERROR, null, msgs);
            if (newError != null)
                msgs = ((InternalEObject)newError).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.DA_COMPONENT__ERROR, null, msgs);
            msgs = basicSetError(newError, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DA_COMPONENT__ERROR, newError, newError));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaFault> getFault() {
        if (fault == null) {
            fault = new EObjectContainmentEList<DaFault>(DaFault.class, this, CorePackage.DA_COMPONENT__FAULT);
        }
        return fault;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaRepair> getRepair() {
        if (repair == null) {
            repair = new EObjectContainmentEList<DaRepair>(DaRepair.class, this, CorePackage.DA_COMPONENT__REPAIR);
        }
        return repair;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaFailure> getFailure() {
        if (failure == null) {
            failure = new EObjectContainmentEList<DaFailure>(DaFailure.class, this, CorePackage.DA_COMPONENT__FAILURE);
        }
        return failure;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaHazard> getHazard() {
        if (hazard == null) {
            hazard = new EObjectContainmentEList<DaHazard>(DaHazard.class, this, CorePackage.DA_COMPONENT__HAZARD);
        }
        return hazard;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaSpare> getSubstitutedBy() {
        if (substitutedBy == null) {
            substitutedBy = new EObjectResolvingEList<DaSpare>(DaSpare.class, this, CorePackage.DA_COMPONENT__SUBSTITUTED_BY);
        }
        return substitutedBy;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case CorePackage.DA_COMPONENT__UNRELIABILITY:
                return ((InternalEList<?>)getUnreliability()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__RELIABILITY:
                return ((InternalEList<?>)getReliability()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__MISSION_TIME:
                return ((InternalEList<?>)getMissionTime()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__COMPLEXITY:
                return ((InternalEList<?>)getComplexity()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__SS_AVAIL:
                return ((InternalEList<?>)getSsAvail()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__FAILURE_COVERAGE:
                return ((InternalEList<?>)getFailureCoverage()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__PREC_PERM_FAULT:
                return ((InternalEList<?>)getPrecPermFault()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__AVAIL_LEVEL:
                return ((InternalEList<?>)getAvailLevel()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__RELIAB_LEVEL:
                return ((InternalEList<?>)getReliabLevel()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__SAFETY_LEVEL:
                return ((InternalEList<?>)getSafetyLevel()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__CONF_LEVEL:
                return ((InternalEList<?>)getConfLevel()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__INTEG_LEVEL:
                return ((InternalEList<?>)getIntegLevel()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__ERROR:
                return basicSetError(null, msgs);
            case CorePackage.DA_COMPONENT__FAULT:
                return ((InternalEList<?>)getFault()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__REPAIR:
                return ((InternalEList<?>)getRepair()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__FAILURE:
                return ((InternalEList<?>)getFailure()).basicRemove(otherEnd, msgs);
            case CorePackage.DA_COMPONENT__HAZARD:
                return ((InternalEList<?>)getHazard()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case CorePackage.DA_COMPONENT__UNRELIABILITY:
                return getUnreliability();
            case CorePackage.DA_COMPONENT__RELIABILITY:
                return getReliability();
            case CorePackage.DA_COMPONENT__MISSION_TIME:
                return getMissionTime();
            case CorePackage.DA_COMPONENT__STATEFUL:
                return isStateful();
            case CorePackage.DA_COMPONENT__ORIGIN:
                return getOrigin();
            case CorePackage.DA_COMPONENT__COMPLEXITY:
                return getComplexity();
            case CorePackage.DA_COMPONENT__SS_AVAIL:
                return getSsAvail();
            case CorePackage.DA_COMPONENT__FAILURE_COVERAGE:
                return getFailureCoverage();
            case CorePackage.DA_COMPONENT__PREC_PERM_FAULT:
                return getPrecPermFault();
            case CorePackage.DA_COMPONENT__AVAIL_LEVEL:
                return getAvailLevel();
            case CorePackage.DA_COMPONENT__RELIAB_LEVEL:
                return getReliabLevel();
            case CorePackage.DA_COMPONENT__SAFETY_LEVEL:
                return getSafetyLevel();
            case CorePackage.DA_COMPONENT__CONF_LEVEL:
                return getConfLevel();
            case CorePackage.DA_COMPONENT__INTEG_LEVEL:
                return getIntegLevel();
            case CorePackage.DA_COMPONENT__ERROR:
                return getError();
            case CorePackage.DA_COMPONENT__FAULT:
                return getFault();
            case CorePackage.DA_COMPONENT__REPAIR:
                return getRepair();
            case CorePackage.DA_COMPONENT__FAILURE:
                return getFailure();
            case CorePackage.DA_COMPONENT__HAZARD:
                return getHazard();
            case CorePackage.DA_COMPONENT__SUBSTITUTED_BY:
                return getSubstitutedBy();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case CorePackage.DA_COMPONENT__UNRELIABILITY:
                getUnreliability().clear();
                getUnreliability().addAll((Collection<? extends NFP_CommonType>)newValue);
                return;
            case CorePackage.DA_COMPONENT__RELIABILITY:
                getReliability().clear();
                getReliability().addAll((Collection<? extends NFP_CommonType>)newValue);
                return;
            case CorePackage.DA_COMPONENT__MISSION_TIME:
                getMissionTime().clear();
                getMissionTime().addAll((Collection<? extends NFP_CommonType>)newValue);
                return;
            case CorePackage.DA_COMPONENT__STATEFUL:
                setStateful((Boolean)newValue);
                return;
            case CorePackage.DA_COMPONENT__ORIGIN:
                setOrigin((Origin)newValue);
                return;
            case CorePackage.DA_COMPONENT__COMPLEXITY:
                getComplexity().clear();
                getComplexity().addAll((Collection<? extends NFP_Real>)newValue);
                return;
            case CorePackage.DA_COMPONENT__SS_AVAIL:
                getSsAvail().clear();
                getSsAvail().addAll((Collection<? extends NFP_Percentage>)newValue);
                return;
            case CorePackage.DA_COMPONENT__FAILURE_COVERAGE:
                getFailureCoverage().clear();
                getFailureCoverage().addAll((Collection<? extends NFP_Percentage>)newValue);
                return;
            case CorePackage.DA_COMPONENT__PREC_PERM_FAULT:
                getPrecPermFault().clear();
                getPrecPermFault().addAll((Collection<? extends NFP_Percentage>)newValue);
                return;
            case CorePackage.DA_COMPONENT__AVAIL_LEVEL:
                getAvailLevel().clear();
                getAvailLevel().addAll((Collection<? extends DaLevel>)newValue);
                return;
            case CorePackage.DA_COMPONENT__RELIAB_LEVEL:
                getReliabLevel().clear();
                getReliabLevel().addAll((Collection<? extends DaLevel>)newValue);
                return;
            case CorePackage.DA_COMPONENT__SAFETY_LEVEL:
                getSafetyLevel().clear();
                getSafetyLevel().addAll((Collection<? extends DaLevel>)newValue);
                return;
            case CorePackage.DA_COMPONENT__CONF_LEVEL:
                getConfLevel().clear();
                getConfLevel().addAll((Collection<? extends DaLevel>)newValue);
                return;
            case CorePackage.DA_COMPONENT__INTEG_LEVEL:
                getIntegLevel().clear();
                getIntegLevel().addAll((Collection<? extends DaLevel>)newValue);
                return;
            case CorePackage.DA_COMPONENT__ERROR:
                setError((DaError)newValue);
                return;
            case CorePackage.DA_COMPONENT__FAULT:
                getFault().clear();
                getFault().addAll((Collection<? extends DaFault>)newValue);
                return;
            case CorePackage.DA_COMPONENT__REPAIR:
                getRepair().clear();
                getRepair().addAll((Collection<? extends DaRepair>)newValue);
                return;
            case CorePackage.DA_COMPONENT__FAILURE:
                getFailure().clear();
                getFailure().addAll((Collection<? extends DaFailure>)newValue);
                return;
            case CorePackage.DA_COMPONENT__HAZARD:
                getHazard().clear();
                getHazard().addAll((Collection<? extends DaHazard>)newValue);
                return;
            case CorePackage.DA_COMPONENT__SUBSTITUTED_BY:
                getSubstitutedBy().clear();
                getSubstitutedBy().addAll((Collection<? extends DaSpare>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case CorePackage.DA_COMPONENT__UNRELIABILITY:
                getUnreliability().clear();
                return;
            case CorePackage.DA_COMPONENT__RELIABILITY:
                getReliability().clear();
                return;
            case CorePackage.DA_COMPONENT__MISSION_TIME:
                getMissionTime().clear();
                return;
            case CorePackage.DA_COMPONENT__STATEFUL:
                setStateful(STATEFUL_EDEFAULT);
                return;
            case CorePackage.DA_COMPONENT__ORIGIN:
                setOrigin(ORIGIN_EDEFAULT);
                return;
            case CorePackage.DA_COMPONENT__COMPLEXITY:
                getComplexity().clear();
                return;
            case CorePackage.DA_COMPONENT__SS_AVAIL:
                getSsAvail().clear();
                return;
            case CorePackage.DA_COMPONENT__FAILURE_COVERAGE:
                getFailureCoverage().clear();
                return;
            case CorePackage.DA_COMPONENT__PREC_PERM_FAULT:
                getPrecPermFault().clear();
                return;
            case CorePackage.DA_COMPONENT__AVAIL_LEVEL:
                getAvailLevel().clear();
                return;
            case CorePackage.DA_COMPONENT__RELIAB_LEVEL:
                getReliabLevel().clear();
                return;
            case CorePackage.DA_COMPONENT__SAFETY_LEVEL:
                getSafetyLevel().clear();
                return;
            case CorePackage.DA_COMPONENT__CONF_LEVEL:
                getConfLevel().clear();
                return;
            case CorePackage.DA_COMPONENT__INTEG_LEVEL:
                getIntegLevel().clear();
                return;
            case CorePackage.DA_COMPONENT__ERROR:
                setError((DaError)null);
                return;
            case CorePackage.DA_COMPONENT__FAULT:
                getFault().clear();
                return;
            case CorePackage.DA_COMPONENT__REPAIR:
                getRepair().clear();
                return;
            case CorePackage.DA_COMPONENT__FAILURE:
                getFailure().clear();
                return;
            case CorePackage.DA_COMPONENT__HAZARD:
                getHazard().clear();
                return;
            case CorePackage.DA_COMPONENT__SUBSTITUTED_BY:
                getSubstitutedBy().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case CorePackage.DA_COMPONENT__UNRELIABILITY:
                return unreliability != null && !unreliability.isEmpty();
            case CorePackage.DA_COMPONENT__RELIABILITY:
                return reliability != null && !reliability.isEmpty();
            case CorePackage.DA_COMPONENT__MISSION_TIME:
                return missionTime != null && !missionTime.isEmpty();
            case CorePackage.DA_COMPONENT__STATEFUL:
                return stateful != STATEFUL_EDEFAULT;
            case CorePackage.DA_COMPONENT__ORIGIN:
                return origin != ORIGIN_EDEFAULT;
            case CorePackage.DA_COMPONENT__COMPLEXITY:
                return complexity != null && !complexity.isEmpty();
            case CorePackage.DA_COMPONENT__SS_AVAIL:
                return ssAvail != null && !ssAvail.isEmpty();
            case CorePackage.DA_COMPONENT__FAILURE_COVERAGE:
                return failureCoverage != null && !failureCoverage.isEmpty();
            case CorePackage.DA_COMPONENT__PREC_PERM_FAULT:
                return precPermFault != null && !precPermFault.isEmpty();
            case CorePackage.DA_COMPONENT__AVAIL_LEVEL:
                return availLevel != null && !availLevel.isEmpty();
            case CorePackage.DA_COMPONENT__RELIAB_LEVEL:
                return reliabLevel != null && !reliabLevel.isEmpty();
            case CorePackage.DA_COMPONENT__SAFETY_LEVEL:
                return safetyLevel != null && !safetyLevel.isEmpty();
            case CorePackage.DA_COMPONENT__CONF_LEVEL:
                return confLevel != null && !confLevel.isEmpty();
            case CorePackage.DA_COMPONENT__INTEG_LEVEL:
                return integLevel != null && !integLevel.isEmpty();
            case CorePackage.DA_COMPONENT__ERROR:
                return error != null;
            case CorePackage.DA_COMPONENT__FAULT:
                return fault != null && !fault.isEmpty();
            case CorePackage.DA_COMPONENT__REPAIR:
                return repair != null && !repair.isEmpty();
            case CorePackage.DA_COMPONENT__FAILURE:
                return failure != null && !failure.isEmpty();
            case CorePackage.DA_COMPONENT__HAZARD:
                return hazard != null && !hazard.isEmpty();
            case CorePackage.DA_COMPONENT__SUBSTITUTED_BY:
                return substitutedBy != null && !substitutedBy.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (stateful: ");
        result.append(stateful);
        result.append(", origin: ");
        result.append(origin);
        result.append(')');
        return result.toString();
    }

} //DaComponentImpl
