/**
 */
package com.masdes.damprofile.profiles.Core;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Origin;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaError;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRepair;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import com.masdes.damprofile.profiles.Redundancy.DaSpare;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getUnreliability <em>Unreliability</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getReliability <em>Reliability</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getMissionTime <em>Mission Time</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#isStateful <em>Stateful</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getOrigin <em>Origin</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getComplexity <em>Complexity</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getSsAvail <em>Ss Avail</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getFailureCoverage <em>Failure Coverage</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getPrecPermFault <em>Prec Perm Fault</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getAvailLevel <em>Avail Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getReliabLevel <em>Reliab Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getSafetyLevel <em>Safety Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getConfLevel <em>Conf Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getIntegLevel <em>Integ Level</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getError <em>Error</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getFault <em>Fault</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getRepair <em>Repair</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getFailure <em>Failure</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getHazard <em>Hazard</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Core.DaComponent#getSubstitutedBy <em>Substituted By</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent()
 * @model
 * @generated
 */
public interface DaComponent extends Resource {
    /**
     * Returns the value of the '<em><b>Unreliability</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Unreliability</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Unreliability</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_Unreliability()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_CommonType> getUnreliability();

    /**
     * Returns the value of the '<em><b>Reliability</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Reliability</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Reliability</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_Reliability()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_CommonType> getReliability();

    /**
     * Returns the value of the '<em><b>Mission Time</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_CommonType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Mission Time</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Mission Time</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_MissionTime()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_CommonType> getMissionTime();

    /**
     * Returns the value of the '<em><b>Stateful</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Stateful</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Stateful</em>' attribute.
     * @see #setStateful(boolean)
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_Stateful()
     * @model dataType="org.eclipse.uml2.types.Boolean" ordered="false"
     * @generated
     */
    boolean isStateful();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Core.DaComponent#isStateful <em>Stateful</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Stateful</em>' attribute.
     * @see #isStateful()
     * @generated
     */
    void setStateful(boolean value);

    /**
     * Returns the value of the '<em><b>Origin</b></em>' attribute.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Origin}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Origin</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Origin</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Origin
     * @see #setOrigin(Origin)
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_Origin()
     * @model unique="false" ordered="false"
     * @generated
     */
    Origin getOrigin();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Core.DaComponent#getOrigin <em>Origin</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Origin</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.Origin
     * @see #getOrigin()
     * @generated
     */
    void setOrigin(Origin value);

    /**
     * Returns the value of the '<em><b>Complexity</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Complexity</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Complexity</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_Complexity()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getComplexity();

    /**
     * Returns the value of the '<em><b>Ss Avail</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ss Avail</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Ss Avail</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_SsAvail()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Percentage> getSsAvail();

    /**
     * Returns the value of the '<em><b>Failure Coverage</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Failure Coverage</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Failure Coverage</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_FailureCoverage()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Percentage> getFailureCoverage();

    /**
     * Returns the value of the '<em><b>Prec Perm Fault</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Prec Perm Fault</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Prec Perm Fault</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_PrecPermFault()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Percentage> getPrecPermFault();

    /**
     * Returns the value of the '<em><b>Avail Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Avail Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Avail Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_AvailLevel()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLevel> getAvailLevel();

    /**
     * Returns the value of the '<em><b>Reliab Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Reliab Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Reliab Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_ReliabLevel()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLevel> getReliabLevel();

    /**
     * Returns the value of the '<em><b>Safety Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Safety Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Safety Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_SafetyLevel()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLevel> getSafetyLevel();

    /**
     * Returns the value of the '<em><b>Conf Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Conf Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Conf Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_ConfLevel()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLevel> getConfLevel();

    /**
     * Returns the value of the '<em><b>Integ Level</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_Data_Types.DaLevel}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Integ Level</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Integ Level</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_IntegLevel()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaLevel> getIntegLevel();

    /**
     * Returns the value of the '<em><b>Error</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Error</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Error</em>' containment reference.
     * @see #setError(DaError)
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_Error()
     * @model containment="true" ordered="false"
     * @generated
     */
    DaError getError();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Core.DaComponent#getError <em>Error</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Error</em>' containment reference.
     * @see #getError()
     * @generated
     */
    void setError(DaError value);

    /**
     * Returns the value of the '<em><b>Fault</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Fault</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Fault</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_Fault()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaFault> getFault();

    /**
     * Returns the value of the '<em><b>Repair</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaRepair}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Repair</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Repair</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_Repair()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaRepair> getRepair();

    /**
     * Returns the value of the '<em><b>Failure</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Failure</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Failure</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_Failure()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaFailure> getFailure();

    /**
     * Returns the value of the '<em><b>Hazard</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Hazard</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Hazard</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_Hazard()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaHazard> getHazard();

    /**
     * Returns the value of the '<em><b>Substituted By</b></em>' reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Redundancy.DaSpare}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Substituted By</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Substituted By</em>' reference list.
     * @see com.masdes.damprofile.profiles.Core.CorePackage#getDaComponent_SubstitutedBy()
     * @model ordered="false"
     * @generated
     */
    EList<DaSpare> getSubstitutedBy();

} // DaComponent
