/**
 */
package com.masdes.damprofile.profiles.Maintenance;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.SkillType;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Classifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Agent Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaAgentGroup#getBase_Classifier <em>Base Classifier</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaAgentGroup#getCorrectness <em>Correctness</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaAgentGroup#getAgentNumber <em>Agent Number</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaAgentGroup#getSkill <em>Skill</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaAgentGroup()
 * @model
 * @generated
 */
public interface DaAgentGroup extends EObject {
    /**
     * Returns the value of the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Classifier</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Base Classifier</em>' reference.
     * @see #setBase_Classifier(Classifier)
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaAgentGroup_Base_Classifier()
     * @model required="true" ordered="false"
     * @generated
     */
    Classifier getBase_Classifier();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Maintenance.DaAgentGroup#getBase_Classifier <em>Base Classifier</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Base Classifier</em>' reference.
     * @see #getBase_Classifier()
     * @generated
     */
    void setBase_Classifier(Classifier value);

    /**
     * Returns the value of the '<em><b>Correctness</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Correctness</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Correctness</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaAgentGroup_Correctness()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getCorrectness();

    /**
     * Returns the value of the '<em><b>Agent Number</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Agent Number</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Agent Number</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaAgentGroup_AgentNumber()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Integer> getAgentNumber();

    /**
     * Returns the value of the '<em><b>Skill</b></em>' attribute.
     * The default value is <code>"hwTechnician"</code>.
     * The literals are from the enumeration {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.SkillType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Skill</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Skill</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.SkillType
     * @see #setSkill(SkillType)
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaAgentGroup_Skill()
     * @model default="hwTechnician" unique="false" required="true" ordered="false"
     * @generated
     */
    SkillType getSkill();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Maintenance.DaAgentGroup#getSkill <em>Skill</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Skill</em>' attribute.
     * @see com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.SkillType
     * @see #getSkill()
     * @generated
     */
    void setSkill(SkillType value);

} // DaAgentGroup
