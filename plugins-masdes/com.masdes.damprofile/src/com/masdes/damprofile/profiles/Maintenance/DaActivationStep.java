/**
 */
package com.masdes.damprofile.profiles.Maintenance;

import com.masdes.damprofile.profiles.Core.DaStep;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Activation Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaActivationStep#getPrio <em>Prio</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaActivationStep#getFailCause <em>Fail Cause</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaActivationStep#getAgents <em>Agents</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaActivationStep#getPreemtion <em>Preemtion</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaActivationStep()
 * @model
 * @generated
 */
public interface DaActivationStep extends DaStep {
    /**
     * Returns the value of the '<em><b>Prio</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Prio</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Prio</em>' containment reference.
     * @see #setPrio(NFP_Integer)
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaActivationStep_Prio()
     * @model containment="true" ordered="false"
     * @generated
     */
    NFP_Integer getPrio();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Maintenance.DaActivationStep#getPrio <em>Prio</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Prio</em>' containment reference.
     * @see #getPrio()
     * @generated
     */
    void setPrio(NFP_Integer value);

    /**
     * Returns the value of the '<em><b>Fail Cause</b></em>' reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Core.DaStep}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Fail Cause</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Fail Cause</em>' reference list.
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaActivationStep_FailCause()
     * @model ordered="false"
     * @generated
     */
    EList<DaStep> getFailCause();

    /**
     * Returns the value of the '<em><b>Agents</b></em>' reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Maintenance.DaAgentGroup}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Agents</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Agents</em>' reference list.
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaActivationStep_Agents()
     * @model ordered="false"
     * @generated
     */
    EList<DaAgentGroup> getAgents();

    /**
     * Returns the value of the '<em><b>Preemtion</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Preemtion</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Preemtion</em>' containment reference.
     * @see #setPreemtion(NFP_Boolean)
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaActivationStep_Preemtion()
     * @model containment="true" ordered="false"
     * @generated
     */
    NFP_Boolean getPreemtion();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Maintenance.DaActivationStep#getPreemtion <em>Preemtion</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Preemtion</em>' containment reference.
     * @see #getPreemtion()
     * @generated
     */
    void setPreemtion(NFP_Boolean value);

} // DaActivationStep
