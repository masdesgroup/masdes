/**
 */
package com.masdes.damprofile.profiles.Maintenance.impl;

import com.masdes.damprofile.profiles.Core.DaComponent;

import com.masdes.damprofile.profiles.Core.impl.DaStepImpl;

import com.masdes.damprofile.profiles.Maintenance.DaReplacementStep;
import com.masdes.damprofile.profiles.Maintenance.MaintenancePackage;

import com.masdes.damprofile.profiles.Redundancy.DaSpare;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Replacement Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaReplacementStepImpl#getReplace <em>Replace</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaReplacementStepImpl#getWith <em>With</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaReplacementStepImpl extends DaStepImpl implements DaReplacementStep {
    /**
     * The cached value of the '{@link #getReplace() <em>Replace</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReplace()
     * @generated
     * @ordered
     */
    protected EList<DaComponent> replace;

    /**
     * The cached value of the '{@link #getWith() <em>With</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getWith()
     * @generated
     * @ordered
     */
    protected EList<DaSpare> with;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaReplacementStepImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return MaintenancePackage.Literals.DA_REPLACEMENT_STEP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaComponent> getReplace() {
        if (replace == null) {
            replace = new EObjectResolvingEList<DaComponent>(DaComponent.class, this, MaintenancePackage.DA_REPLACEMENT_STEP__REPLACE);
        }
        return replace;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaSpare> getWith() {
        if (with == null) {
            with = new EObjectResolvingEList<DaSpare>(DaSpare.class, this, MaintenancePackage.DA_REPLACEMENT_STEP__WITH);
        }
        return with;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case MaintenancePackage.DA_REPLACEMENT_STEP__REPLACE:
                return getReplace();
            case MaintenancePackage.DA_REPLACEMENT_STEP__WITH:
                return getWith();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case MaintenancePackage.DA_REPLACEMENT_STEP__REPLACE:
                getReplace().clear();
                getReplace().addAll((Collection<? extends DaComponent>)newValue);
                return;
            case MaintenancePackage.DA_REPLACEMENT_STEP__WITH:
                getWith().clear();
                getWith().addAll((Collection<? extends DaSpare>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case MaintenancePackage.DA_REPLACEMENT_STEP__REPLACE:
                getReplace().clear();
                return;
            case MaintenancePackage.DA_REPLACEMENT_STEP__WITH:
                getWith().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case MaintenancePackage.DA_REPLACEMENT_STEP__REPLACE:
                return replace != null && !replace.isEmpty();
            case MaintenancePackage.DA_REPLACEMENT_STEP__WITH:
                return with != null && !with.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //DaReplacementStepImpl
