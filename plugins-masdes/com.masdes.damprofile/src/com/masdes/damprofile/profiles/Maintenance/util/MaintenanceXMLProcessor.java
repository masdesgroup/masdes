/**
 */
package com.masdes.damprofile.profiles.Maintenance.util;

import com.masdes.damprofile.profiles.Maintenance.MaintenancePackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MaintenanceXMLProcessor extends XMLProcessor {

    /**
     * Public constructor to instantiate the helper.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public MaintenanceXMLProcessor() {
        super((EPackage.Registry.INSTANCE));
        MaintenancePackage.eINSTANCE.eClass();
    }
    
    /**
     * Register for "*" and "xml" file extensions the MaintenanceResourceFactoryImpl factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected Map<String, Resource.Factory> getRegistrations() {
        if (registrations == null) {
            super.getRegistrations();
            registrations.put(XML_EXTENSION, new MaintenanceResourceFactoryImpl());
            registrations.put(STAR_EXTENSION, new MaintenanceResourceFactoryImpl());
        }
        return registrations;
    }

} //MaintenanceXMLProcessor
