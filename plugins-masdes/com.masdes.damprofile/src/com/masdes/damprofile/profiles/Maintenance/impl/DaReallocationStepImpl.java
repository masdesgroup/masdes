/**
 */
package com.masdes.damprofile.profiles.Maintenance.impl;

import com.masdes.damprofile.profiles.Core.DaComponent;

import com.masdes.damprofile.profiles.Core.impl.DaStepImpl;

import com.masdes.damprofile.profiles.Maintenance.DaReallocationStep;
import com.masdes.damprofile.profiles.Maintenance.MaintenancePackage;

import com.masdes.damprofile.profiles.Redundancy.DaSpare;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Reallocation Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaReallocationStepImpl#getMap <em>Map</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaReallocationStepImpl#getTo <em>To</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaReallocationStepImpl extends DaStepImpl implements DaReallocationStep {
    /**
     * The cached value of the '{@link #getMap() <em>Map</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getMap()
     * @generated
     * @ordered
     */
    protected EList<DaComponent> map;

    /**
     * The cached value of the '{@link #getTo() <em>To</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getTo()
     * @generated
     * @ordered
     */
    protected EList<DaSpare> to;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaReallocationStepImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return MaintenancePackage.Literals.DA_REALLOCATION_STEP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaComponent> getMap() {
        if (map == null) {
            map = new EObjectResolvingEList<DaComponent>(DaComponent.class, this, MaintenancePackage.DA_REALLOCATION_STEP__MAP);
        }
        return map;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaSpare> getTo() {
        if (to == null) {
            to = new EObjectResolvingEList<DaSpare>(DaSpare.class, this, MaintenancePackage.DA_REALLOCATION_STEP__TO);
        }
        return to;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case MaintenancePackage.DA_REALLOCATION_STEP__MAP:
                return getMap();
            case MaintenancePackage.DA_REALLOCATION_STEP__TO:
                return getTo();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case MaintenancePackage.DA_REALLOCATION_STEP__MAP:
                getMap().clear();
                getMap().addAll((Collection<? extends DaComponent>)newValue);
                return;
            case MaintenancePackage.DA_REALLOCATION_STEP__TO:
                getTo().clear();
                getTo().addAll((Collection<? extends DaSpare>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case MaintenancePackage.DA_REALLOCATION_STEP__MAP:
                getMap().clear();
                return;
            case MaintenancePackage.DA_REALLOCATION_STEP__TO:
                getTo().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case MaintenancePackage.DA_REALLOCATION_STEP__MAP:
                return map != null && !map.isEmpty();
            case MaintenancePackage.DA_REALLOCATION_STEP__TO:
                return to != null && !to.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //DaReallocationStepImpl
