/**
 */
package com.masdes.damprofile.profiles.Maintenance;

import com.masdes.damprofile.profiles.Core.DaComponent;
import com.masdes.damprofile.profiles.Core.DaStep;

import com.masdes.damprofile.profiles.Redundancy.DaSpare;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Replacement Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaReplacementStep#getReplace <em>Replace</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaReplacementStep#getWith <em>With</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaReplacementStep()
 * @model
 * @generated
 */
public interface DaReplacementStep extends DaStep {
    /**
     * Returns the value of the '<em><b>Replace</b></em>' reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Core.DaComponent}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Replace</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Replace</em>' reference list.
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaReplacementStep_Replace()
     * @model
     * @generated
     */
    EList<DaComponent> getReplace();

    /**
     * Returns the value of the '<em><b>With</b></em>' reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Redundancy.DaSpare}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>With</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>With</em>' reference list.
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaReplacementStep_With()
     * @model
     * @generated
     */
    EList<DaSpare> getWith();

} // DaReplacementStep
