/**
 */
package com.masdes.damprofile.profiles.Maintenance.impl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Basic_DA_Enumeration_Types.SkillType;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import com.masdes.damprofile.profiles.Maintenance.DaAgentGroup;
import com.masdes.damprofile.profiles.Maintenance.MaintenancePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.uml2.uml.Classifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Agent Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaAgentGroupImpl#getBase_Classifier <em>Base Classifier</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaAgentGroupImpl#getCorrectness <em>Correctness</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaAgentGroupImpl#getAgentNumber <em>Agent Number</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaAgentGroupImpl#getSkill <em>Skill</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaAgentGroupImpl extends EObjectImpl implements DaAgentGroup {
    /**
     * The cached value of the '{@link #getBase_Classifier() <em>Base Classifier</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getBase_Classifier()
     * @generated
     * @ordered
     */
    protected Classifier base_Classifier;

    /**
     * The cached value of the '{@link #getCorrectness() <em>Correctness</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCorrectness()
     * @generated
     * @ordered
     */
    protected EList<NFP_Real> correctness;

    /**
     * The cached value of the '{@link #getAgentNumber() <em>Agent Number</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getAgentNumber()
     * @generated
     * @ordered
     */
    protected EList<NFP_Integer> agentNumber;

    /**
     * The default value of the '{@link #getSkill() <em>Skill</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSkill()
     * @generated
     * @ordered
     */
    protected static final SkillType SKILL_EDEFAULT = SkillType.HW_TECHNICIAN;

    /**
     * The cached value of the '{@link #getSkill() <em>Skill</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSkill()
     * @generated
     * @ordered
     */
    protected SkillType skill = SKILL_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaAgentGroupImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return MaintenancePackage.Literals.DA_AGENT_GROUP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Classifier getBase_Classifier() {
        if (base_Classifier != null && base_Classifier.eIsProxy()) {
            InternalEObject oldBase_Classifier = (InternalEObject)base_Classifier;
            base_Classifier = (Classifier)eResolveProxy(oldBase_Classifier);
            if (base_Classifier != oldBase_Classifier) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, MaintenancePackage.DA_AGENT_GROUP__BASE_CLASSIFIER, oldBase_Classifier, base_Classifier));
            }
        }
        return base_Classifier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Classifier basicGetBase_Classifier() {
        return base_Classifier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setBase_Classifier(Classifier newBase_Classifier) {
        Classifier oldBase_Classifier = base_Classifier;
        base_Classifier = newBase_Classifier;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MaintenancePackage.DA_AGENT_GROUP__BASE_CLASSIFIER, oldBase_Classifier, base_Classifier));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Real> getCorrectness() {
        if (correctness == null) {
            correctness = new EObjectContainmentEList<NFP_Real>(NFP_Real.class, this, MaintenancePackage.DA_AGENT_GROUP__CORRECTNESS);
        }
        return correctness;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Integer> getAgentNumber() {
        if (agentNumber == null) {
            agentNumber = new EObjectContainmentEList<NFP_Integer>(NFP_Integer.class, this, MaintenancePackage.DA_AGENT_GROUP__AGENT_NUMBER);
        }
        return agentNumber;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public SkillType getSkill() {
        return skill;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setSkill(SkillType newSkill) {
        SkillType oldSkill = skill;
        skill = newSkill == null ? SKILL_EDEFAULT : newSkill;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MaintenancePackage.DA_AGENT_GROUP__SKILL, oldSkill, skill));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case MaintenancePackage.DA_AGENT_GROUP__CORRECTNESS:
                return ((InternalEList<?>)getCorrectness()).basicRemove(otherEnd, msgs);
            case MaintenancePackage.DA_AGENT_GROUP__AGENT_NUMBER:
                return ((InternalEList<?>)getAgentNumber()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case MaintenancePackage.DA_AGENT_GROUP__BASE_CLASSIFIER:
                if (resolve) return getBase_Classifier();
                return basicGetBase_Classifier();
            case MaintenancePackage.DA_AGENT_GROUP__CORRECTNESS:
                return getCorrectness();
            case MaintenancePackage.DA_AGENT_GROUP__AGENT_NUMBER:
                return getAgentNumber();
            case MaintenancePackage.DA_AGENT_GROUP__SKILL:
                return getSkill();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case MaintenancePackage.DA_AGENT_GROUP__BASE_CLASSIFIER:
                setBase_Classifier((Classifier)newValue);
                return;
            case MaintenancePackage.DA_AGENT_GROUP__CORRECTNESS:
                getCorrectness().clear();
                getCorrectness().addAll((Collection<? extends NFP_Real>)newValue);
                return;
            case MaintenancePackage.DA_AGENT_GROUP__AGENT_NUMBER:
                getAgentNumber().clear();
                getAgentNumber().addAll((Collection<? extends NFP_Integer>)newValue);
                return;
            case MaintenancePackage.DA_AGENT_GROUP__SKILL:
                setSkill((SkillType)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case MaintenancePackage.DA_AGENT_GROUP__BASE_CLASSIFIER:
                setBase_Classifier((Classifier)null);
                return;
            case MaintenancePackage.DA_AGENT_GROUP__CORRECTNESS:
                getCorrectness().clear();
                return;
            case MaintenancePackage.DA_AGENT_GROUP__AGENT_NUMBER:
                getAgentNumber().clear();
                return;
            case MaintenancePackage.DA_AGENT_GROUP__SKILL:
                setSkill(SKILL_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case MaintenancePackage.DA_AGENT_GROUP__BASE_CLASSIFIER:
                return base_Classifier != null;
            case MaintenancePackage.DA_AGENT_GROUP__CORRECTNESS:
                return correctness != null && !correctness.isEmpty();
            case MaintenancePackage.DA_AGENT_GROUP__AGENT_NUMBER:
                return agentNumber != null && !agentNumber.isEmpty();
            case MaintenancePackage.DA_AGENT_GROUP__SKILL:
                return skill != SKILL_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (skill: ");
        result.append(skill);
        result.append(')');
        return result.toString();
    }

} //DaAgentGroupImpl
