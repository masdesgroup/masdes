/**
 */
package com.masdes.damprofile.profiles.Maintenance;

import com.masdes.damprofile.profiles.Core.DaComponent;
import com.masdes.damprofile.profiles.Core.DaStep;

import com.masdes.damprofile.profiles.Redundancy.DaSpare;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Reallocation Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaReallocationStep#getMap <em>Map</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.DaReallocationStep#getTo <em>To</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaReallocationStep()
 * @model
 * @generated
 */
public interface DaReallocationStep extends DaStep {
    /**
     * Returns the value of the '<em><b>Map</b></em>' reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Core.DaComponent}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Map</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Map</em>' reference list.
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaReallocationStep_Map()
     * @model
     * @generated
     */
    EList<DaComponent> getMap();

    /**
     * Returns the value of the '<em><b>To</b></em>' reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Redundancy.DaSpare}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>To</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>To</em>' reference list.
     * @see com.masdes.damprofile.profiles.Maintenance.MaintenancePackage#getDaReallocationStep_To()
     * @model
     * @generated
     */
    EList<DaSpare> getTo();

} // DaReallocationStep
