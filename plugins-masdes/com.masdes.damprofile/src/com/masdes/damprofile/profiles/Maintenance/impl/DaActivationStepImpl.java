/**
 */
package com.masdes.damprofile.profiles.Maintenance.impl;

import com.masdes.damprofile.profiles.Core.DaStep;

import com.masdes.damprofile.profiles.Core.impl.DaStepImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Boolean;
import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer;

import com.masdes.damprofile.profiles.Maintenance.DaActivationStep;
import com.masdes.damprofile.profiles.Maintenance.DaAgentGroup;
import com.masdes.damprofile.profiles.Maintenance.MaintenancePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Activation Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaActivationStepImpl#getPrio <em>Prio</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaActivationStepImpl#getFailCause <em>Fail Cause</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaActivationStepImpl#getAgents <em>Agents</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Maintenance.impl.DaActivationStepImpl#getPreemtion <em>Preemtion</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaActivationStepImpl extends DaStepImpl implements DaActivationStep {
    /**
     * The cached value of the '{@link #getPrio() <em>Prio</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getPrio()
     * @generated
     * @ordered
     */
    protected NFP_Integer prio;

    /**
     * The cached value of the '{@link #getFailCause() <em>Fail Cause</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFailCause()
     * @generated
     * @ordered
     */
    protected EList<DaStep> failCause;

    /**
     * The cached value of the '{@link #getAgents() <em>Agents</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getAgents()
     * @generated
     * @ordered
     */
    protected EList<DaAgentGroup> agents;

    /**
     * The cached value of the '{@link #getPreemtion() <em>Preemtion</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getPreemtion()
     * @generated
     * @ordered
     */
    protected NFP_Boolean preemtion;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaActivationStepImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return MaintenancePackage.Literals.DA_ACTIVATION_STEP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NFP_Integer getPrio() {
        return prio;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetPrio(NFP_Integer newPrio, NotificationChain msgs) {
        NFP_Integer oldPrio = prio;
        prio = newPrio;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MaintenancePackage.DA_ACTIVATION_STEP__PRIO, oldPrio, newPrio);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setPrio(NFP_Integer newPrio) {
        if (newPrio != prio) {
            NotificationChain msgs = null;
            if (prio != null)
                msgs = ((InternalEObject)prio).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MaintenancePackage.DA_ACTIVATION_STEP__PRIO, null, msgs);
            if (newPrio != null)
                msgs = ((InternalEObject)newPrio).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MaintenancePackage.DA_ACTIVATION_STEP__PRIO, null, msgs);
            msgs = basicSetPrio(newPrio, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MaintenancePackage.DA_ACTIVATION_STEP__PRIO, newPrio, newPrio));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaStep> getFailCause() {
        if (failCause == null) {
            failCause = new EObjectResolvingEList<DaStep>(DaStep.class, this, MaintenancePackage.DA_ACTIVATION_STEP__FAIL_CAUSE);
        }
        return failCause;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaAgentGroup> getAgents() {
        if (agents == null) {
            agents = new EObjectResolvingEList<DaAgentGroup>(DaAgentGroup.class, this, MaintenancePackage.DA_ACTIVATION_STEP__AGENTS);
        }
        return agents;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NFP_Boolean getPreemtion() {
        return preemtion;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetPreemtion(NFP_Boolean newPreemtion, NotificationChain msgs) {
        NFP_Boolean oldPreemtion = preemtion;
        preemtion = newPreemtion;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MaintenancePackage.DA_ACTIVATION_STEP__PREEMTION, oldPreemtion, newPreemtion);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setPreemtion(NFP_Boolean newPreemtion) {
        if (newPreemtion != preemtion) {
            NotificationChain msgs = null;
            if (preemtion != null)
                msgs = ((InternalEObject)preemtion).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MaintenancePackage.DA_ACTIVATION_STEP__PREEMTION, null, msgs);
            if (newPreemtion != null)
                msgs = ((InternalEObject)newPreemtion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MaintenancePackage.DA_ACTIVATION_STEP__PREEMTION, null, msgs);
            msgs = basicSetPreemtion(newPreemtion, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MaintenancePackage.DA_ACTIVATION_STEP__PREEMTION, newPreemtion, newPreemtion));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case MaintenancePackage.DA_ACTIVATION_STEP__PRIO:
                return basicSetPrio(null, msgs);
            case MaintenancePackage.DA_ACTIVATION_STEP__PREEMTION:
                return basicSetPreemtion(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case MaintenancePackage.DA_ACTIVATION_STEP__PRIO:
                return getPrio();
            case MaintenancePackage.DA_ACTIVATION_STEP__FAIL_CAUSE:
                return getFailCause();
            case MaintenancePackage.DA_ACTIVATION_STEP__AGENTS:
                return getAgents();
            case MaintenancePackage.DA_ACTIVATION_STEP__PREEMTION:
                return getPreemtion();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case MaintenancePackage.DA_ACTIVATION_STEP__PRIO:
                setPrio((NFP_Integer)newValue);
                return;
            case MaintenancePackage.DA_ACTIVATION_STEP__FAIL_CAUSE:
                getFailCause().clear();
                getFailCause().addAll((Collection<? extends DaStep>)newValue);
                return;
            case MaintenancePackage.DA_ACTIVATION_STEP__AGENTS:
                getAgents().clear();
                getAgents().addAll((Collection<? extends DaAgentGroup>)newValue);
                return;
            case MaintenancePackage.DA_ACTIVATION_STEP__PREEMTION:
                setPreemtion((NFP_Boolean)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case MaintenancePackage.DA_ACTIVATION_STEP__PRIO:
                setPrio((NFP_Integer)null);
                return;
            case MaintenancePackage.DA_ACTIVATION_STEP__FAIL_CAUSE:
                getFailCause().clear();
                return;
            case MaintenancePackage.DA_ACTIVATION_STEP__AGENTS:
                getAgents().clear();
                return;
            case MaintenancePackage.DA_ACTIVATION_STEP__PREEMTION:
                setPreemtion((NFP_Boolean)null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case MaintenancePackage.DA_ACTIVATION_STEP__PRIO:
                return prio != null;
            case MaintenancePackage.DA_ACTIVATION_STEP__FAIL_CAUSE:
                return failCause != null && !failCause.isEmpty();
            case MaintenancePackage.DA_ACTIVATION_STEP__AGENTS:
                return agents != null && !agents.isEmpty();
            case MaintenancePackage.DA_ACTIVATION_STEP__PREEMTION:
                return preemtion != null;
        }
        return super.eIsSet(featureID);
    }

} //DaActivationStepImpl
