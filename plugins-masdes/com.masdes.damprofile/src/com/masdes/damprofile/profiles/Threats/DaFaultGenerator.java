/**
 */
package com.masdes.damprofile.profiles.Threats;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaWorkloadGenerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Fault Generator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Threats.DaFaultGenerator#getFault <em>Fault</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Threats.DaFaultGenerator#getNumberOfFaults <em>Number Of Faults</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Threats.ThreatsPackage#getDaFaultGenerator()
 * @model
 * @generated
 */
public interface DaFaultGenerator extends GaWorkloadGenerator {
    /**
     * Returns the value of the '<em><b>Fault</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Fault</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Fault</em>' containment reference.
     * @see #setFault(DaFault)
     * @see com.masdes.damprofile.profiles.Threats.ThreatsPackage#getDaFaultGenerator_Fault()
     * @model containment="true" ordered="false"
     * @generated
     */
    DaFault getFault();

    /**
     * Sets the value of the '{@link com.masdes.damprofile.profiles.Threats.DaFaultGenerator#getFault <em>Fault</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Fault</em>' containment reference.
     * @see #getFault()
     * @generated
     */
    void setFault(DaFault value);

    /**
     * Returns the value of the '<em><b>Number Of Faults</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Number Of Faults</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Number Of Faults</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Threats.ThreatsPackage#getDaFaultGenerator_NumberOfFaults()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Integer> getNumberOfFaults();

} // DaFaultGenerator
