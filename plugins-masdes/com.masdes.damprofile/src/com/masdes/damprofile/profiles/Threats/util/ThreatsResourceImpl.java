/**
 */
package com.masdes.damprofile.profiles.Threats.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Threats.util.ThreatsResourceFactoryImpl
 * @generated
 */
public class ThreatsResourceImpl extends XMLResourceImpl {
    /**
     * Creates an instance of the resource.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param uri the URI of the new resource.
     * @generated
     */
    public ThreatsResourceImpl(URI uri) {
        super(uri);
    }

} //ThreatsResourceImpl
