/**
 */
package com.masdes.damprofile.profiles.Threats.impl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFault;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Integer;

import com.masdes.damprofile.profiles.Threats.DaFaultGenerator;
import com.masdes.damprofile.profiles.Threats.ThreatsPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GaWorkloadGeneratorImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Fault Generator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Threats.impl.DaFaultGeneratorImpl#getFault <em>Fault</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Threats.impl.DaFaultGeneratorImpl#getNumberOfFaults <em>Number Of Faults</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaFaultGeneratorImpl extends GaWorkloadGeneratorImpl implements DaFaultGenerator {
    /**
     * The cached value of the '{@link #getFault() <em>Fault</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFault()
     * @generated
     * @ordered
     */
    protected DaFault fault;

    /**
     * The cached value of the '{@link #getNumberOfFaults() <em>Number Of Faults</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNumberOfFaults()
     * @generated
     * @ordered
     */
    protected EList<NFP_Integer> numberOfFaults;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaFaultGeneratorImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ThreatsPackage.Literals.DA_FAULT_GENERATOR;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DaFault getFault() {
        return fault;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetFault(DaFault newFault, NotificationChain msgs) {
        DaFault oldFault = fault;
        fault = newFault;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ThreatsPackage.DA_FAULT_GENERATOR__FAULT, oldFault, newFault);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setFault(DaFault newFault) {
        if (newFault != fault) {
            NotificationChain msgs = null;
            if (fault != null)
                msgs = ((InternalEObject)fault).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ThreatsPackage.DA_FAULT_GENERATOR__FAULT, null, msgs);
            if (newFault != null)
                msgs = ((InternalEObject)newFault).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ThreatsPackage.DA_FAULT_GENERATOR__FAULT, null, msgs);
            msgs = basicSetFault(newFault, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ThreatsPackage.DA_FAULT_GENERATOR__FAULT, newFault, newFault));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Integer> getNumberOfFaults() {
        if (numberOfFaults == null) {
            numberOfFaults = new EObjectContainmentEList<NFP_Integer>(NFP_Integer.class, this, ThreatsPackage.DA_FAULT_GENERATOR__NUMBER_OF_FAULTS);
        }
        return numberOfFaults;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ThreatsPackage.DA_FAULT_GENERATOR__FAULT:
                return basicSetFault(null, msgs);
            case ThreatsPackage.DA_FAULT_GENERATOR__NUMBER_OF_FAULTS:
                return ((InternalEList<?>)getNumberOfFaults()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ThreatsPackage.DA_FAULT_GENERATOR__FAULT:
                return getFault();
            case ThreatsPackage.DA_FAULT_GENERATOR__NUMBER_OF_FAULTS:
                return getNumberOfFaults();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ThreatsPackage.DA_FAULT_GENERATOR__FAULT:
                setFault((DaFault)newValue);
                return;
            case ThreatsPackage.DA_FAULT_GENERATOR__NUMBER_OF_FAULTS:
                getNumberOfFaults().clear();
                getNumberOfFaults().addAll((Collection<? extends NFP_Integer>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ThreatsPackage.DA_FAULT_GENERATOR__FAULT:
                setFault((DaFault)null);
                return;
            case ThreatsPackage.DA_FAULT_GENERATOR__NUMBER_OF_FAULTS:
                getNumberOfFaults().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ThreatsPackage.DA_FAULT_GENERATOR__FAULT:
                return fault != null;
            case ThreatsPackage.DA_FAULT_GENERATOR__NUMBER_OF_FAULTS:
                return numberOfFaults != null && !numberOfFaults.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //DaFaultGeneratorImpl
