/**
 */
package com.masdes.damprofile.profiles.Redundancy.impl;

import com.masdes.damprofile.profiles.Core.impl.DaComponentImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage;

import com.masdes.damprofile.profiles.Redundancy.DaAdjudicator;
import com.masdes.damprofile.profiles.Redundancy.RedundancyPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Adjudicator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.impl.DaAdjudicatorImpl#getErrorDetecCoverage <em>Error Detec Coverage</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaAdjudicatorImpl extends DaComponentImpl implements DaAdjudicator {
    /**
     * The cached value of the '{@link #getErrorDetecCoverage() <em>Error Detec Coverage</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getErrorDetecCoverage()
     * @generated
     * @ordered
     */
    protected EList<NFP_Percentage> errorDetecCoverage;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaAdjudicatorImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RedundancyPackage.Literals.DA_ADJUDICATOR;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Percentage> getErrorDetecCoverage() {
        if (errorDetecCoverage == null) {
            errorDetecCoverage = new EObjectContainmentEList<NFP_Percentage>(NFP_Percentage.class, this, RedundancyPackage.DA_ADJUDICATOR__ERROR_DETEC_COVERAGE);
        }
        return errorDetecCoverage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RedundancyPackage.DA_ADJUDICATOR__ERROR_DETEC_COVERAGE:
                return ((InternalEList<?>)getErrorDetecCoverage()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RedundancyPackage.DA_ADJUDICATOR__ERROR_DETEC_COVERAGE:
                return getErrorDetecCoverage();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RedundancyPackage.DA_ADJUDICATOR__ERROR_DETEC_COVERAGE:
                getErrorDetecCoverage().clear();
                getErrorDetecCoverage().addAll((Collection<? extends NFP_Percentage>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case RedundancyPackage.DA_ADJUDICATOR__ERROR_DETEC_COVERAGE:
                getErrorDetecCoverage().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RedundancyPackage.DA_ADJUDICATOR__ERROR_DETEC_COVERAGE:
                return errorDetecCoverage != null && !errorDetecCoverage.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //DaAdjudicatorImpl
