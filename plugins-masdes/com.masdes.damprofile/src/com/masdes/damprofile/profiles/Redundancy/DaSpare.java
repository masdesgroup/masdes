/**
 */
package com.masdes.damprofile.profiles.Redundancy;

import com.masdes.damprofile.profiles.Core.DaComponent;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Spare</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.DaSpare#getDormancyFactor <em>Dormancy Factor</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.DaSpare#getSubstituteFor <em>Substitute For</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaSpare()
 * @model
 * @generated
 */
public interface DaSpare extends DaComponent {
    /**
     * Returns the value of the '<em><b>Dormancy Factor</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Dormancy Factor</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Dormancy Factor</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaSpare_DormancyFactor()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Real> getDormancyFactor();

    /**
     * Returns the value of the '<em><b>Substitute For</b></em>' reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Core.DaComponent}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Substitute For</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Substitute For</em>' reference list.
     * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaSpare_SubstituteFor()
     * @model ordered="false"
     * @generated
     */
    EList<DaComponent> getSubstituteFor();

} // DaSpare
