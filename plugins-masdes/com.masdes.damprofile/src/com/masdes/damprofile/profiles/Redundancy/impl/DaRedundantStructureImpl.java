/**
 */
package com.masdes.damprofile.profiles.Redundancy.impl;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard;

import com.masdes.damprofile.profiles.Redundancy.DaRedundantStructure;
import com.masdes.damprofile.profiles.Redundancy.RedundancyPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Redundant Structure</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.impl.DaRedundantStructureImpl#getCommonModeFailure <em>Common Mode Failure</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.impl.DaRedundantStructureImpl#getCommonModeHazard <em>Common Mode Hazard</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.impl.DaRedundantStructureImpl#getFTlevel <em>FTlevel</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaRedundantStructureImpl extends EObjectImpl implements DaRedundantStructure {
    /**
     * The cached value of the '{@link #getCommonModeFailure() <em>Common Mode Failure</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCommonModeFailure()
     * @generated
     * @ordered
     */
    protected EList<DaFailure> commonModeFailure;

    /**
     * The cached value of the '{@link #getCommonModeHazard() <em>Common Mode Hazard</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCommonModeHazard()
     * @generated
     * @ordered
     */
    protected EList<DaHazard> commonModeHazard;

    /**
     * The cached value of the '{@link #getFTlevel() <em>FTlevel</em>}' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFTlevel()
     * @generated
     * @ordered
     */
    protected EList<Integer> fTlevel;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaRedundantStructureImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RedundancyPackage.Literals.DA_REDUNDANT_STRUCTURE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaFailure> getCommonModeFailure() {
        if (commonModeFailure == null) {
            commonModeFailure = new EObjectContainmentEList<DaFailure>(DaFailure.class, this, RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_FAILURE);
        }
        return commonModeFailure;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaHazard> getCommonModeHazard() {
        if (commonModeHazard == null) {
            commonModeHazard = new EObjectContainmentEList<DaHazard>(DaHazard.class, this, RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_HAZARD);
        }
        return commonModeHazard;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<Integer> getFTlevel() {
        if (fTlevel == null) {
            fTlevel = new EDataTypeUniqueEList<Integer>(Integer.class, this, RedundancyPackage.DA_REDUNDANT_STRUCTURE__FTLEVEL);
        }
        return fTlevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_FAILURE:
                return ((InternalEList<?>)getCommonModeFailure()).basicRemove(otherEnd, msgs);
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_HAZARD:
                return ((InternalEList<?>)getCommonModeHazard()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_FAILURE:
                return getCommonModeFailure();
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_HAZARD:
                return getCommonModeHazard();
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__FTLEVEL:
                return getFTlevel();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_FAILURE:
                getCommonModeFailure().clear();
                getCommonModeFailure().addAll((Collection<? extends DaFailure>)newValue);
                return;
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_HAZARD:
                getCommonModeHazard().clear();
                getCommonModeHazard().addAll((Collection<? extends DaHazard>)newValue);
                return;
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__FTLEVEL:
                getFTlevel().clear();
                getFTlevel().addAll((Collection<? extends Integer>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_FAILURE:
                getCommonModeFailure().clear();
                return;
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_HAZARD:
                getCommonModeHazard().clear();
                return;
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__FTLEVEL:
                getFTlevel().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_FAILURE:
                return commonModeFailure != null && !commonModeFailure.isEmpty();
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__COMMON_MODE_HAZARD:
                return commonModeHazard != null && !commonModeHazard.isEmpty();
            case RedundancyPackage.DA_REDUNDANT_STRUCTURE__FTLEVEL:
                return fTlevel != null && !fTlevel.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (FTlevel: ");
        result.append(fTlevel);
        result.append(')');
        return result.toString();
    }

} //DaRedundantStructureImpl
