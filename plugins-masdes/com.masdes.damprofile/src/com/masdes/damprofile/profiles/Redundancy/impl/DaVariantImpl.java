/**
 */
package com.masdes.damprofile.profiles.Redundancy.impl;

import com.masdes.damprofile.profiles.Core.impl.DaComponentImpl;

import com.masdes.damprofile.profiles.Redundancy.DaVariant;
import com.masdes.damprofile.profiles.Redundancy.RedundancyPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Variant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DaVariantImpl extends DaComponentImpl implements DaVariant {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaVariantImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RedundancyPackage.Literals.DA_VARIANT;
    }

} //DaVariantImpl
