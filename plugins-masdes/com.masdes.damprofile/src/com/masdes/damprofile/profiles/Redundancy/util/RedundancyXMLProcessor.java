/**
 */
package com.masdes.damprofile.profiles.Redundancy.util;

import com.masdes.damprofile.profiles.Redundancy.RedundancyPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RedundancyXMLProcessor extends XMLProcessor {

    /**
     * Public constructor to instantiate the helper.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public RedundancyXMLProcessor() {
        super((EPackage.Registry.INSTANCE));
        RedundancyPackage.eINSTANCE.eClass();
    }
    
    /**
     * Register for "*" and "xml" file extensions the RedundancyResourceFactoryImpl factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected Map<String, Resource.Factory> getRegistrations() {
        if (registrations == null) {
            super.getRegistrations();
            registrations.put(XML_EXTENSION, new RedundancyResourceFactoryImpl());
            registrations.put(STAR_EXTENSION, new RedundancyResourceFactoryImpl());
        }
        return registrations;
    }

} //RedundancyXMLProcessor
