/**
 */
package com.masdes.damprofile.profiles.Redundancy;

import com.masdes.damprofile.profiles.Core.DaComponent;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Adjudicator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.DaAdjudicator#getErrorDetecCoverage <em>Error Detec Coverage</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaAdjudicator()
 * @model
 * @generated
 */
public interface DaAdjudicator extends DaComponent {
    /**
     * Returns the value of the '<em><b>Error Detec Coverage</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Percentage}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Error Detec Coverage</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Error Detec Coverage</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaAdjudicator_ErrorDetecCoverage()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<NFP_Percentage> getErrorDetecCoverage();

} // DaAdjudicator
