/**
 */
package com.masdes.damprofile.profiles.Redundancy.impl;

import com.masdes.damprofile.profiles.Core.DaComponent;

import com.masdes.damprofile.profiles.Core.impl.DaComponentImpl;

import com.masdes.damprofile.profiles.Dam.MARTE_Library.BasicNFP_Types.NFP_Real;

import com.masdes.damprofile.profiles.Redundancy.DaSpare;
import com.masdes.damprofile.profiles.Redundancy.RedundancyPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Spare</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.impl.DaSpareImpl#getDormancyFactor <em>Dormancy Factor</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.impl.DaSpareImpl#getSubstituteFor <em>Substitute For</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DaSpareImpl extends DaComponentImpl implements DaSpare {
    /**
     * The cached value of the '{@link #getDormancyFactor() <em>Dormancy Factor</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDormancyFactor()
     * @generated
     * @ordered
     */
    protected EList<NFP_Real> dormancyFactor;

    /**
     * The cached value of the '{@link #getSubstituteFor() <em>Substitute For</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSubstituteFor()
     * @generated
     * @ordered
     */
    protected EList<DaComponent> substituteFor;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaSpareImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RedundancyPackage.Literals.DA_SPARE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<NFP_Real> getDormancyFactor() {
        if (dormancyFactor == null) {
            dormancyFactor = new EObjectContainmentEList<NFP_Real>(NFP_Real.class, this, RedundancyPackage.DA_SPARE__DORMANCY_FACTOR);
        }
        return dormancyFactor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<DaComponent> getSubstituteFor() {
        if (substituteFor == null) {
            substituteFor = new EObjectResolvingEList<DaComponent>(DaComponent.class, this, RedundancyPackage.DA_SPARE__SUBSTITUTE_FOR);
        }
        return substituteFor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RedundancyPackage.DA_SPARE__DORMANCY_FACTOR:
                return ((InternalEList<?>)getDormancyFactor()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RedundancyPackage.DA_SPARE__DORMANCY_FACTOR:
                return getDormancyFactor();
            case RedundancyPackage.DA_SPARE__SUBSTITUTE_FOR:
                return getSubstituteFor();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RedundancyPackage.DA_SPARE__DORMANCY_FACTOR:
                getDormancyFactor().clear();
                getDormancyFactor().addAll((Collection<? extends NFP_Real>)newValue);
                return;
            case RedundancyPackage.DA_SPARE__SUBSTITUTE_FOR:
                getSubstituteFor().clear();
                getSubstituteFor().addAll((Collection<? extends DaComponent>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case RedundancyPackage.DA_SPARE__DORMANCY_FACTOR:
                getDormancyFactor().clear();
                return;
            case RedundancyPackage.DA_SPARE__SUBSTITUTE_FOR:
                getSubstituteFor().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RedundancyPackage.DA_SPARE__DORMANCY_FACTOR:
                return dormancyFactor != null && !dormancyFactor.isEmpty();
            case RedundancyPackage.DA_SPARE__SUBSTITUTE_FOR:
                return substituteFor != null && !substituteFor.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //DaSpareImpl
