/**
 */
package com.masdes.damprofile.profiles.Redundancy;

import com.masdes.damprofile.profiles.Core.DaComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaController()
 * @model
 * @generated
 */
public interface DaController extends DaComponent {
} // DaController
