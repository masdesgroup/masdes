/**
 */
package com.masdes.damprofile.profiles.Redundancy.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see com.masdes.damprofile.profiles.Redundancy.util.RedundancyResourceFactoryImpl
 * @generated
 */
public class RedundancyResourceImpl extends XMLResourceImpl {
    /**
     * Creates an instance of the resource.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param uri the URI of the new resource.
     * @generated
     */
    public RedundancyResourceImpl(URI uri) {
        super(uri);
    }

} //RedundancyResourceImpl
