/**
 */
package com.masdes.damprofile.profiles.Redundancy.impl;

import com.masdes.damprofile.profiles.Core.impl.DaComponentImpl;

import com.masdes.damprofile.profiles.Redundancy.DaController;
import com.masdes.damprofile.profiles.Redundancy.RedundancyPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Da Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DaControllerImpl extends DaComponentImpl implements DaController {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected DaControllerImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RedundancyPackage.Literals.DA_CONTROLLER;
    }

} //DaControllerImpl
