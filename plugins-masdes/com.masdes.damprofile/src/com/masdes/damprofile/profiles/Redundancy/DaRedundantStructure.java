/**
 */
package com.masdes.damprofile.profiles.Redundancy;

import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure;
import com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Redundant Structure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.DaRedundantStructure#getCommonModeFailure <em>Common Mode Failure</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.DaRedundantStructure#getCommonModeHazard <em>Common Mode Hazard</em>}</li>
 *   <li>{@link com.masdes.damprofile.profiles.Redundancy.DaRedundantStructure#getFTlevel <em>FTlevel</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaRedundantStructure()
 * @model
 * @generated
 */
public interface DaRedundantStructure extends EObject {
    /**
     * Returns the value of the '<em><b>Common Mode Failure</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaFailure}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Common Mode Failure</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Common Mode Failure</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaRedundantStructure_CommonModeFailure()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaFailure> getCommonModeFailure();

    /**
     * Returns the value of the '<em><b>Common Mode Hazard</b></em>' containment reference list.
     * The list contents are of type {@link com.masdes.damprofile.profiles.Dam.Dam_Library.Complex_Data_Types.DaHazard}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Common Mode Hazard</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Common Mode Hazard</em>' containment reference list.
     * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaRedundantStructure_CommonModeHazard()
     * @model containment="true" ordered="false"
     * @generated
     */
    EList<DaHazard> getCommonModeHazard();

    /**
     * Returns the value of the '<em><b>FTlevel</b></em>' attribute list.
     * The list contents are of type {@link java.lang.Integer}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>FTlevel</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>FTlevel</em>' attribute list.
     * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaRedundantStructure_FTlevel()
     * @model default="1" dataType="org.eclipse.uml2.types.Integer" ordered="false"
     * @generated
     */
    EList<Integer> getFTlevel();

} // DaRedundantStructure
