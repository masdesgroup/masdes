/**
 */
package com.masdes.damprofile.profiles.Redundancy;

import com.masdes.damprofile.profiles.Core.DaComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Da Variant</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.masdes.damprofile.profiles.Redundancy.RedundancyPackage#getDaVariant()
 * @model
 * @generated
 */
public interface DaVariant extends DaComponent {
} // DaVariant
