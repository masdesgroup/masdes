package com.masdes.sm2ptnet.tools;

/**
 * 
 * @author maria berenguer
 *
 */
public class Resources extends com.masdes.resources.Resources {
    
    
    // ATL
    //****
    
    public static final String METAMODEL_UML = "uml";
    public static final String METAMODEL_XML = "XML";
    
    public static final String MODEL_LP = "logicalProperties";
    public static final String MODEL_OUT = "OUT";
    
    public static final String LIB_UTIL_HELPERS = "UtilHelpers";
    public static final String LIB_PTNET_HELPERS = "PTnetHelpers";
    public static final String LIB_UML_STEREOTYPES_HELPERS = "UmlStereotypesHelpers";
    public static final String LIB_UML_STATE_MACHINE_HELPERS = "UmlStateMachineHelpers";
    public static final String LIB_XML_HELPERS = "XMLHelpers";
    
    
    // SM2PTres
    //*********
    
    public static final String ATL_SM2PTRES = "SM2PTres";
    public static final String SM2PTRES_PROPERTIES_FILE = "SM2PTres.properties";
    
    
}
