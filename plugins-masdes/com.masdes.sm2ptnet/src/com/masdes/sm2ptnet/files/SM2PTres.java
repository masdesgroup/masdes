/*******************************************************************************
 * Copyright (c) 2010, 2012 Obeo.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Obeo - initial API and implementation
 *     María Berenguer - adaptation to sm2ptnet plug-in
 *******************************************************************************/
package com.masdes.sm2ptnet.files;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.m2m.atl.common.ATLExecutionException;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IExtractor;
import org.eclipse.m2m.atl.core.IInjector;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.core.IReferenceModel;
import org.eclipse.m2m.atl.core.ModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFExtractor;
import org.eclipse.m2m.atl.core.emf.EMFInjector;
import org.eclipse.m2m.atl.core.emf.EMFModelFactory;
import org.eclipse.m2m.atl.core.launch.ILauncher;
import org.eclipse.m2m.atl.engine.emfvm.launch.EMFVMLauncher;

import com.masdes.resources.ConsoleDisplayManager;
import com.masdes.sm2ptnet.tools.Resources;

/**
 * Entry point of the 'SM2PTres' transformation module.
 */
public class SM2PTres {

    /**
     * The property file. Stores module list, the metamodel and library locations.
     * @generated
     */
    private Properties properties;
    
    /**
     * The IN model.
     * @generated
     */
    protected IModel inModel;
    
    /**
     * The logicalProperties model.
     * @generated
     */
    protected IModel logicalpropertiesModel;
    
    /**
     * The OUT model.
     * @generated
     */
    protected IModel outModel;    
    
    
    /**
     * The main method.
     * 
     * @param args
     *            are the arguments
     * @generated
     */
    public static void mainSm2ptres( String[] args, Boolean useConsole ) {
            
        if ( useConsole ) {
            
            SM2PTres.main( args );
        } else {
            try {
                if ( args.length < 3 ) {
                    
                } else {
                    
                    SM2PTres runner = new SM2PTres();
                    
                    runner.loadModels( args[0], args[1] );
                    
                    // Launch transformation
                    runner.doSM2PTres(new NullProgressMonitor());
                    
                    // Save transformation results in files
                    runner.saveModels(args[2]);
                }
            } catch ( ATLCoreException e ) {

                // e.printStackTrace();
            } catch ( IOException e ) {

                // e.printStackTrace();
            } catch ( ATLExecutionException e ) {

                // e.printStackTrace();
            } catch ( Exception e ) {

                // e.printStackTrace();
            } 
        }
    }
    
    public static void main( String[] args ) {
        
        // Use ConsoleDisplayMgr as output stream
        ConsoleDisplayManager console = ConsoleDisplayManager.getDefault( Resources.CONSOLE_TITLE );
        
        try {
            if ( args.length < 3 ) {
                
                console.println( 
                        Resources.MSG_ARGUMENTS_NOT_VALID3, 
                        ConsoleDisplayManager.MSG_TYPE_ERROR 
                );
                // System.out.println("Arguments not valid : {IN_model_path, logicalProperties_model_path, OUT_model_path}.");
            } else {
                
                SM2PTres runner = new SM2PTres();
                
                // Load input models
                console.println( 
                        Resources.MSG_LOAD_INPUT_MODEL + args[ 0 ] + Resources.DOTS, 
                        ConsoleDisplayManager.MSG_TYPE_INFORMATION
                );
                
                runner.loadModels( args[0], args[1] );
                
                // Start ATL transformation
                console.println( 
                        Resources.MSG_ATL_START, 
                        ConsoleDisplayManager.MSG_TYPE_INFORMATION
                );
                // Get current time for console information
                long startTime = System.currentTimeMillis();
                // Launch transformation
                runner.doSM2PTres(new NullProgressMonitor());
                // Get elapsed time in milliseconds for console information
                long elapsedTime = System.currentTimeMillis() - startTime;
                float execSeconds = elapsedTime / 1000F;
                console.println( 
                        Resources.MSG_ATL_SUCCESS + execSeconds + Resources.MSG_SECONDS, 
                        ConsoleDisplayManager.MSG_TYPE_INFORMATION
                );
                
                // Save transformation results in files
                runner.saveModels(args[2]);
                console.println( 
                        Resources.MSG_RESULTS_SAVED + args[ 2 ], 
                        ConsoleDisplayManager.MSG_TYPE_INFORMATION
                );
            }
        } catch ( ATLCoreException e ) {
            
            String[] errorMessage = {
                    e.getMessage(),
                    e.getStackTrace().toString()
            };
            console.println( 
                    errorMessage, 
                    ConsoleDisplayManager.MSG_TYPE_ERROR 
            );
            // e.printStackTrace();
        } catch ( IOException e ) {
            
            String[] errorMessage = {
                    e.getMessage(),
                    e.getStackTrace().toString()
            };
            console.println( 
                    errorMessage, 
                    ConsoleDisplayManager.MSG_TYPE_ERROR 
            );
            // e.printStackTrace();
        } catch ( ATLExecutionException e ) {
            
            String[] errorMessage = {
                    e.getMessage(),
                    e.getStackTrace().toString()
            };
            console.println( 
                    errorMessage, 
                    ConsoleDisplayManager.MSG_TYPE_ERROR 
            );
            // e.printStackTrace();
        } catch ( Exception e ) {
            
            String[] errorMessage = {
                    e.getMessage(),
                    e.getStackTrace().toString()
            };
            console.println( 
                    errorMessage, 
                    ConsoleDisplayManager.MSG_TYPE_ERROR 
            );
        }
    }
    
    
    /**
     * Constructor.
     *
     * @generated
     */
    public SM2PTres() 
            throws IOException {
        
        // Load properties
        properties = new Properties();
        properties.load(getFileURL( Resources.SM2PTRES_PROPERTIES_FILE ).openStream() );
        
        EPackage.Registry.INSTANCE.put(
                getMetamodelUri( Resources.EXTENSION_XML ),
                org.eclipse.gmt.modisco.xml.emf.MoDiscoXMLPackage.eINSTANCE
        );
        EPackage.Registry.INSTANCE.put(
                getMetamodelUri( Resources.EXTENSION_UML ),
                org.eclipse.uml2.uml.UMLPackage.eINSTANCE
        );
        Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(
                Resources.EXTENSION_ECORE,
                new EcoreResourceFactoryImpl()
        );
    }
    
    
    /**
     * Load the input and input/output models, initialize output models.
     * 
     * @param inModelPath
     *            the IN model path
     * @param logicalpropertiesModelPath
     *            the logicalProperties model path
     * @throws ATLCoreException
     *             if a problem occurs while loading models
     *
     * @generated
     */
    public void loadModels( String inModelPath, String logicalpropertiesModelPath )
            throws ATLCoreException, IOException, Exception {
        
        
        ModelFactory factory = new EMFModelFactory();
        IInjector injector = new EMFInjector();
        
        IReferenceModel ptnetMetamodel = factory.newReferenceModel();
        injector.inject(
                ptnetMetamodel,
                this.getMetamodelPath( Resources.METAMODEL_PTNET )
        );
        
        IReferenceModel xmlMetamodel = factory.newReferenceModel();
        injector.inject(
                xmlMetamodel,
                this.getMetamodelUri( Resources.METAMODEL_XML ) );
        
        IReferenceModel umlMetamodel = factory.newReferenceModel();
        injector.inject(
                umlMetamodel, 
                this.getMetamodelUri( Resources.METAMODEL_UML ) );
        
        // Inject models
        this.inModel = factory.newModel( umlMetamodel );
        try {
            
            injector.inject( inModel, inModelPath );
        } catch ( Exception e1 ) {

            String[] errorMessage = { 
                    inModelPath, 
                    e1.getMessage(), 
                    e1.getStackTrace().toString() 
            };
            Exception exc1 = new Exception(errorMessage.toString());
            throw exc1;
        }
        
        this.logicalpropertiesModel = factory.newModel( xmlMetamodel );
        try {
            
            injector.inject( logicalpropertiesModel, logicalpropertiesModelPath );
        } catch ( Exception e ) {
            
            String[] warningMessage = { 
                    logicalpropertiesModelPath + " " + Resources.MSG_FILE_NOT_FOUND, 
                    e.getMessage(), 
                    e.getStackTrace().toString() 
            };
            Exception exc = new Exception(warningMessage.toString());
            throw exc;
        }
        this.outModel = factory.newModel( ptnetMetamodel );
    }
    
    
    /**
     * Returns the path of the given metamodel, parameterized from the property file.
     * 
     * @param metamodelName
     *            the metamodel name
     * @return the metamodel file path
     * @throws IOException
     *             if getFileURL throws exception
     */
    protected String getMetamodelPath( String metamodelName ) 
              throws IOException {
        
        return getFileURL( getMetamodelUri( metamodelName ) ).toString();
    }


    /**
     * Save the output and input/output models.
     * 
     * @param outModelPath
     *            the OUT model path
     * @throws ATLCoreException
     *             if a problem occurs while saving models
     *
     * @generated
     */
    public void saveModels( String outModelPath )
            throws ATLCoreException {
        
        IExtractor extractor = new EMFExtractor();
        extractor.extract( outModel, outModelPath );
    }
    
    
    /**
     * Transform the models.
     * 
     * @param monitor
     *            the progress monitor
     * @throws ATLCoreException
     *             if an error occurs during models handling
     * @throws IOException
     *             if a module cannot be read
     * @throws ATLExecutionException
     *             if an error occurs during the execution
     *
     * @generated
     */
    public Object doSM2PTres( IProgressMonitor monitor ) 
            throws ATLCoreException, IOException, ATLExecutionException {
        
        ILauncher launcher = new EMFVMLauncher();
        Map<String, Object> launcherOptions = getOptions();
        
        launcher.initialize( launcherOptions );
        
        launcher.addInModel( 
                inModel, Resources.MODEL_IN, 
                Resources.METAMODEL_UML );
        launcher.addInModel( 
                logicalpropertiesModel, 
                Resources.MODEL_LP, 
                Resources.METAMODEL_XML );
        
        launcher.addOutModel( 
                outModel, 
                Resources.MODEL_OUT, 
                Resources.METAMODEL_PTNET );
        
        launcher.addLibrary( 
                Resources.LIB_XML_HELPERS, 
                getLibraryAsStream( Resources.LIB_XML_HELPERS ) );
        launcher.addLibrary( 
                Resources.LIB_UML_STATE_MACHINE_HELPERS, 
                getLibraryAsStream( Resources.LIB_UML_STATE_MACHINE_HELPERS ) );
        launcher.addLibrary( 
                Resources.LIB_UML_STEREOTYPES_HELPERS, 
                getLibraryAsStream( Resources.LIB_UML_STEREOTYPES_HELPERS ) );
        launcher.addLibrary( 
                Resources.LIB_PTNET_HELPERS, 
                getLibraryAsStream( Resources.LIB_PTNET_HELPERS ) );
        launcher.addLibrary( 
                Resources.LIB_UTIL_HELPERS, 
                getLibraryAsStream( Resources.LIB_UTIL_HELPERS ) );
        
        return launcher.launch( Resources.LAUNCHER_RUN, monitor, launcherOptions, ( Object[] ) getModulesList() );
    }
    
    /**
     * Returns an Array of the module input streams, parameterized by the
     * property file.
     * 
     * @return an Array of the module input streams
     * @throws IOException
     *             if a module cannot be read
     *
     * @generated
     */
    protected InputStream[] getModulesList() 
            throws IOException {
        
        InputStream[] modules = null;
        String modulesProperty = Resources.ATL_SM2PTRES + Resources.PROP_MODULES;
        final String modulesList = properties.getProperty( modulesProperty );
        
        if ( modulesList != null ) {
            
            final String[] moduleNames = modulesList.split( Resources.SEPARATOR_MODULES );
            modules = new InputStream[ moduleNames.length ];
            for (int i = 0; i < moduleNames.length; i++) {
                
                String asmModulePath = new Path(moduleNames[ i ].trim()
                        ).removeFileExtension().addFileExtension( Resources.EXTENSION_ASM 
                                ).toString();
                modules[ i ] = getFileURL( asmModulePath ).openStream();
            }
        }
        return modules;
    }
    
    
    /**
     * Returns the URI of the given metamodel, parameterized from the property file.
     * 
     * @param metamodelName
     *            the metamodel name
     * @return the metamodel URI
     *
     * @generated
     */
    protected String getMetamodelUri( String metamodelName ) {
        
        String metamodelString =
                Resources.ATL_SM2PTRES + Resources.PROP_METAMODELS + Resources.DOT;
        return properties.getProperty( metamodelString + metamodelName );
    }
    
    
    /**
     * Returns the file name of the given library, parameterized from the property file.
     * 
     * @param libraryName
     *            the library name
     * @return the library file name
     *
     * @generated
     */
    protected InputStream getLibraryAsStream( String libraryName ) 
            throws IOException {
        
        final String libraryString = 
                Resources.ATL_SM2PTRES + Resources.PROP_LIBRARIES + Resources.DOT;
        return getFileURL( properties.getProperty( 
                libraryString + libraryName )).openStream();
    }
    
    
    /**
     * Returns the options map, parameterized from the property file.
     * 
     * @return the options map
     *
     * @generated
     */
    protected Map<String, Object> getOptions() {
        
        Map<String, Object> options = new HashMap<String, Object>();
        
        final String option = 
                Resources.ATL_SM2PTRES + Resources.PROP_OPTIONS + Resources.DOT;
        
        for ( Entry<Object, Object> entry : properties.entrySet() ) {
            
            if ( entry.getKey().toString().startsWith( option )) {
                
                options.put(
                        entry.getKey().toString().replaceFirst( option, "" ), 
                        entry.getValue().toString()
                );
            }
        }
        return options;
    }
    
    
    /**
     * Finds the file in the plug-in. Returns the file URL.
     * 
     * @param fileName
     *            the file name
     * @return the file URL
     * @throws IOException
     *             if the file doesn't exist
     * 
     * @generated
     */
    protected static URL getFileURL( String fileName ) 
            throws IOException {
        
        final URL fileURL;
        if ( Resources.isEclipseRunning() ) {
            
            URL resourceURL = SM2PTres.class.getResource( fileName );
            if (resourceURL != null) {
                
                fileURL = FileLocator.toFileURL( resourceURL );
            } else {
                
                fileURL = null;
            }
        } else {
            
            fileURL = SM2PTres.class.getResource( fileName );
        }
        if ( fileURL == null ) {
            
            throw new IOException( fileName + Resources.MSG_FILE_NOT_FOUND );
        } else {
            
            return fileURL;
        }
    }
}
