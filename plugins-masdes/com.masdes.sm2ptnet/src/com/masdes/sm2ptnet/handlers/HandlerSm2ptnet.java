/**
 * @author Maria Berenguer
 */

package com.masdes.sm2ptnet.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.masdes.resources.ConsoleDisplayManager;
import com.masdes.resources.handlers.HandlerPtnet2Resource;
import com.masdes.sm2ptnet.files.SM2PTres;
import com.masdes.sm2ptnet.tools.Resources;

public class HandlerSm2ptnet extends HandlerPtnet2Resource {
    
    /**
     * External output file path
     */
    private Path outputFilePath;
    
    
    /**
     * The Constructor
     */
    public HandlerSm2ptnet() {
        
        super();
        initQueryArgs( 3 );
    }
    
    
    public HandlerSm2ptnet( Path inputFilePath ) {
        
        super( inputFilePath );
        
        initQueryArgs( 3 );
    }

    
    public Object execute( ExecutionEvent event ) 
            throws ExecutionException {
        
        IStructuredSelection structuredSelection = Resources.getStructuredSelection( event );
        ConsoleDisplayManager console = ConsoleDisplayManager.getDefault( Resources.CONSOLE_TITLE );
        super.execute( structuredSelection );
        
        //check if the structured selection is an IFile
        if ( ( structuredSelection.getFirstElement() instanceof IFile ) ) {
            
            String propertiesFileString = Resources.PATH_SEPARATOR + Resources.MODEL_LP + Resources.DOT + Resources.EXTENSION_XML;
            
            String outputFinalFilePath = getInputDirectoryPathString() + Resources.RESULTS_FOLDER + Resources.PATH_SEPARATOR + getInputFilePath().removeFileExtension().toFile().getName() + Resources.DOT + Resources.EXTENSION_PTNET;
            this.setOutputFilePath( new Path( outputFinalFilePath ) );
            
            // Input model (state machine/s to transform)
            setQueryArgs( 0, getInputFilePathString() );
            // Input properties
            setQueryArgs( 1, getInputDirectoryPathString().concat( propertiesFileString ) );
            // Output petri net (following the output ecore model)
            setQueryArgs( 2, outputFinalFilePath );
            
            try {
                
                SM2PTres.main( getQueryArgs() );
                
            } catch ( final Exception e ) {
                
                String[] errorMessage = {
                        e.getMessage(),
                        e.getStackTrace().toString()
                };
                console.println( 
                        errorMessage, 
                        ConsoleDisplayManager.MSG_TYPE_ERROR 
                );
            }
            return getOutputFilePath();
        }
        
        return null;
    }

    
    public Object executeHandler( ExecutionEvent event, Boolean useConsole, IStructuredSelection structuredSelection ) throws ExecutionException {
        
        if ( useConsole ) {
            
            return execute( event );
        } else {
            
            super.execute( structuredSelection );
            
            //check if the structured selection is an IFile
            if ( ( structuredSelection.getFirstElement() instanceof IFile ) ) {
                
                String propertiesFileString = Resources.PATH_SEPARATOR + Resources.MODEL_LP + Resources.DOT + Resources.EXTENSION_XML;
                
                String outputFinalFilePath = getInputDirectoryPathString() + Resources.RESULTS_FOLDER + Resources.PATH_SEPARATOR + getInputFilePath().removeFileExtension().toFile().getName() + Resources.DOT + Resources.EXTENSION_PTNET;
                this.setOutputFilePath( new Path( outputFinalFilePath ) );
                
                // Input model (state machine/s to transform)
                setQueryArgs( 0, getInputFilePathString() );
                // Input properties
                setQueryArgs( 1, getInputDirectoryPathString().concat( propertiesFileString ) );
                // Output petri net (following the output ecore model)
                setQueryArgs( 2, outputFinalFilePath );
                
                try {
                    
                     SM2PTres.mainSm2ptres( getQueryArgs(), useConsole );
                    
                } catch ( final Exception e ) {
                    
                    //e.printStackTrace();
                }
                
                return getOutputFilePath();
            } 
            
            return null;
        }
    }
    
    
    private void setOutputFilePath( Path outputFilePath ) {
        
        this.outputFilePath = outputFilePath;
    }
    
    
    private Path getOutputFilePath() {
        
        return this.outputFilePath;
    }


}
