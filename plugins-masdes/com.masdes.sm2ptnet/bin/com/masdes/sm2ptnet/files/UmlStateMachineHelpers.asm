<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="UmlStateMachineHelpers"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="self"/>
		<constant value="getSmName"/>
		<constant value="Muml!StateMachine;"/>
		<constant value="0"/>
		<constant value="name"/>
		<constant value="21:5-21:9"/>
		<constant value="21:5-21:14"/>
		<constant value="Muml!Vertex;"/>
		<constant value="container"/>
		<constant value="stateMachine"/>
		<constant value="J.getSmName():J"/>
		<constant value="27:5-27:9"/>
		<constant value="27:5-27:19"/>
		<constant value="27:5-27:32"/>
		<constant value="27:5-27:45"/>
		<constant value="Muml!Transition;"/>
		<constant value="33:5-33:9"/>
		<constant value="33:5-33:19"/>
		<constant value="33:5-33:32"/>
		<constant value="33:5-33:45"/>
		<constant value="getInitStateName"/>
		<constant value="J.getInitState():J"/>
		<constant value="J.getVertexName():J"/>
		<constant value="44:5-44:9"/>
		<constant value="44:5-44:25"/>
		<constant value="44:5-44:42"/>
		<constant value="50:5-50:9"/>
		<constant value="50:5-50:25"/>
		<constant value="50:5-50:42"/>
		<constant value="getInitState"/>
		<constant value="1"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="allUmlPseudostates"/>
		<constant value="2"/>
		<constant value="kind"/>
		<constant value="EnumLiteral"/>
		<constant value="initial"/>
		<constant value="J.=(J):J"/>
		<constant value="J.and(J):J"/>
		<constant value="B.not():B"/>
		<constant value="28"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="J.first():J"/>
		<constant value="outgoing"/>
		<constant value="target"/>
		<constant value="56:27-56:31"/>
		<constant value="56:27-56:44"/>
		<constant value="58:5-58:15"/>
		<constant value="58:5-58:34"/>
		<constant value="59:15-59:17"/>
		<constant value="59:15-59:22"/>
		<constant value="59:25-59:33"/>
		<constant value="59:15-59:33"/>
		<constant value="59:42-59:44"/>
		<constant value="59:42-59:57"/>
		<constant value="59:60-59:66"/>
		<constant value="59:42-59:66"/>
		<constant value="59:13-59:68"/>
		<constant value="58:5-60:10"/>
		<constant value="58:5-60:19"/>
		<constant value="58:5-60:28"/>
		<constant value="58:5-60:38"/>
		<constant value="58:5-60:45"/>
		<constant value="56:5-60:45"/>
		<constant value="ps"/>
		<constant value="smName"/>
		<constant value="connectionPoint"/>
		<constant value="20"/>
		<constant value="66:5-66:9"/>
		<constant value="66:5-66:25"/>
		<constant value="66:42-66:44"/>
		<constant value="66:42-66:49"/>
		<constant value="66:52-66:60"/>
		<constant value="66:42-66:60"/>
		<constant value="66:5-66:64"/>
		<constant value="66:5-67:35"/>
		<constant value="66:5-67:44"/>
		<constant value="66:5-67:54"/>
		<constant value="66:5-67:61"/>
		<constant value="getVertexName"/>
		<constant value="74:5-74:9"/>
		<constant value="74:5-74:14"/>
		<constant value="getTargetReferences"/>
		<constant value="J"/>
		<constant value="3"/>
		<constant value="4"/>
		<constant value="17"/>
		<constant value="J.resolveTemp(JJ):J"/>
		<constant value="81:5-81:19"/>
		<constant value="81:42-81:52"/>
		<constant value="81:42-81:65"/>
		<constant value="81:68-81:74"/>
		<constant value="81:42-81:74"/>
		<constant value="81:5-81:76"/>
		<constant value="82:31-82:41"/>
		<constant value="82:55-82:63"/>
		<constant value="82:65-82:75"/>
		<constant value="82:31-82:77"/>
		<constant value="81:5-82:79"/>
		<constant value="umlElement"/>
		<constant value="ptTarget"/>
		<constant value="allUmlElements"/>
		<constant value="targetName"/>
		<constant value="getStateName"/>
		<constant value="Muml!State;"/>
		<constant value="145:5-145:9"/>
		<constant value="145:5-145:14"/>
		<constant value="isInitState"/>
		<constant value="State"/>
		<constant value="uml"/>
		<constant value="J.oclIsKindOf(J):J"/>
		<constant value="incoming"/>
		<constant value="J.isSrcTransPseudo():J"/>
		<constant value="19"/>
		<constant value="source"/>
		<constant value="B.or(B):B"/>
		<constant value="156:5-156:9"/>
		<constant value="156:23-156:32"/>
		<constant value="156:5-156:34"/>
		<constant value="157:5-157:9"/>
		<constant value="157:5-157:18"/>
		<constant value="157:35-157:39"/>
		<constant value="157:35-157:59"/>
		<constant value="157:5-157:61"/>
		<constant value="158:25-158:28"/>
		<constant value="158:25-158:35"/>
		<constant value="158:25-158:40"/>
		<constant value="158:43-158:51"/>
		<constant value="158:25-158:51"/>
		<constant value="157:5-158:53"/>
		<constant value="156:5-158:53"/>
		<constant value="tran"/>
		<constant value="src"/>
		<constant value="isSrcTransPseudo"/>
		<constant value="Pseudostate"/>
		<constant value="169:5-169:9"/>
		<constant value="169:5-169:16"/>
		<constant value="169:31-169:46"/>
		<constant value="169:5-169:48"/>
		<constant value="isSrcTransInit"/>
		<constant value="5"/>
		<constant value="15"/>
		<constant value="180:12-180:16"/>
		<constant value="180:12-180:35"/>
		<constant value="182:14-182:19"/>
		<constant value="181:14-181:18"/>
		<constant value="181:14-181:25"/>
		<constant value="181:14-181:30"/>
		<constant value="181:33-181:41"/>
		<constant value="181:14-181:41"/>
		<constant value="180:9-183:14"/>
		<constant value="launchFireabilityRule"/>
		<constant value="18"/>
		<constant value="22"/>
		<constant value="J.FireabilityTransition(JJ):J"/>
		<constant value="190:62-190:66"/>
		<constant value="190:5-190:21"/>
		<constant value="191:11-191:32"/>
		<constant value="191:56-191:67"/>
		<constant value="191:70-191:80"/>
		<constant value="191:70-191:85"/>
		<constant value="191:56-191:85"/>
		<constant value="191:11-191:87"/>
		<constant value="193:11-193:16"/>
		<constant value="192:11-192:21"/>
		<constant value="192:45-192:49"/>
		<constant value="192:51-192:61"/>
		<constant value="192:11-192:63"/>
		<constant value="191:6-194:11"/>
		<constant value="190:5-195:6"/>
		<constant value="useCaseName"/>
		<constant value="transition"/>
		<constant value="launch"/>
		<constant value="ptnetTransitions"/>
		<constant value="xmlFireabilityUcNames"/>
		<constant value="launchLivenessRule"/>
		<constant value="J.LivenessTransition(JJ):J"/>
		<constant value="202:62-202:66"/>
		<constant value="202:5-202:21"/>
		<constant value="203:11-203:29"/>
		<constant value="203:53-203:64"/>
		<constant value="203:67-203:77"/>
		<constant value="203:67-203:82"/>
		<constant value="203:53-203:82"/>
		<constant value="203:11-203:84"/>
		<constant value="205:11-205:16"/>
		<constant value="204:11-204:21"/>
		<constant value="204:42-204:46"/>
		<constant value="204:48-204:58"/>
		<constant value="204:11-204:60"/>
		<constant value="203:6-206:11"/>
		<constant value="202:5-207:6"/>
		<constant value="xmlLivenessUcNames"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="-1"/>
		</localvariabletable>
	</operation>
	<operation name="4">
		<context type="5"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<get arg="7"/>
		</code>
		<linenumbertable>
			<lne id="8" begin="0" end="0"/>
			<lne id="9" begin="0" end="1"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="1"/>
		</localvariabletable>
	</operation>
	<operation name="4">
		<context type="10"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<get arg="11"/>
			<get arg="12"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="14" begin="0" end="0"/>
			<lne id="15" begin="0" end="1"/>
			<lne id="16" begin="0" end="2"/>
			<lne id="17" begin="0" end="3"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="3"/>
		</localvariabletable>
	</operation>
	<operation name="4">
		<context type="18"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<get arg="11"/>
			<get arg="12"/>
			<call arg="13"/>
		</code>
		<linenumbertable>
			<lne id="19" begin="0" end="0"/>
			<lne id="20" begin="0" end="1"/>
			<lne id="21" begin="0" end="2"/>
			<lne id="22" begin="0" end="3"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="3"/>
		</localvariabletable>
	</operation>
	<operation name="23">
		<context type="10"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<call arg="24"/>
			<call arg="25"/>
		</code>
		<linenumbertable>
			<lne id="26" begin="0" end="0"/>
			<lne id="27" begin="0" end="1"/>
			<lne id="28" begin="0" end="2"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="2"/>
		</localvariabletable>
	</operation>
	<operation name="23">
		<context type="5"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<call arg="24"/>
			<call arg="25"/>
		</code>
		<linenumbertable>
			<lne id="29" begin="0" end="0"/>
			<lne id="30" begin="0" end="1"/>
			<lne id="31" begin="0" end="2"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="2"/>
		</localvariabletable>
	</operation>
	<operation name="32">
		<context type="10"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<call arg="13"/>
			<store arg="33"/>
			<push arg="34"/>
			<push arg="35"/>
			<new/>
			<getasm/>
			<get arg="36"/>
			<iterate/>
			<store arg="37"/>
			<load arg="37"/>
			<get arg="38"/>
			<push arg="39"/>
			<push arg="35"/>
			<new/>
			<dup/>
			<push arg="40"/>
			<set arg="7"/>
			<call arg="41"/>
			<load arg="37"/>
			<call arg="13"/>
			<load arg="33"/>
			<call arg="41"/>
			<call arg="42"/>
			<call arg="43"/>
			<if arg="44"/>
			<load arg="37"/>
			<call arg="45"/>
			<enditerate/>
			<call arg="46"/>
			<get arg="47"/>
			<call arg="46"/>
			<get arg="48"/>
		</code>
		<linenumbertable>
			<lne id="49" begin="0" end="0"/>
			<lne id="50" begin="0" end="1"/>
			<lne id="51" begin="6" end="6"/>
			<lne id="52" begin="6" end="7"/>
			<lne id="53" begin="10" end="10"/>
			<lne id="54" begin="10" end="11"/>
			<lne id="55" begin="12" end="17"/>
			<lne id="56" begin="10" end="18"/>
			<lne id="57" begin="19" end="19"/>
			<lne id="58" begin="19" end="20"/>
			<lne id="59" begin="21" end="21"/>
			<lne id="60" begin="19" end="22"/>
			<lne id="61" begin="10" end="23"/>
			<lne id="62" begin="3" end="28"/>
			<lne id="63" begin="3" end="29"/>
			<lne id="64" begin="3" end="30"/>
			<lne id="65" begin="3" end="31"/>
			<lne id="66" begin="3" end="32"/>
			<lne id="67" begin="0" end="32"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="68" begin="9" end="27"/>
			<lve slot="1" name="69" begin="2" end="32"/>
			<lve slot="0" name="3" begin="0" end="32"/>
		</localvariabletable>
	</operation>
	<operation name="32">
		<context type="5"/>
		<parameters>
		</parameters>
		<code>
			<push arg="34"/>
			<push arg="35"/>
			<new/>
			<load arg="6"/>
			<get arg="70"/>
			<iterate/>
			<store arg="33"/>
			<load arg="33"/>
			<get arg="38"/>
			<push arg="39"/>
			<push arg="35"/>
			<new/>
			<dup/>
			<push arg="40"/>
			<set arg="7"/>
			<call arg="41"/>
			<call arg="43"/>
			<if arg="71"/>
			<load arg="33"/>
			<call arg="45"/>
			<enditerate/>
			<call arg="46"/>
			<get arg="47"/>
			<call arg="46"/>
			<get arg="48"/>
		</code>
		<linenumbertable>
			<lne id="72" begin="3" end="3"/>
			<lne id="73" begin="3" end="4"/>
			<lne id="74" begin="7" end="7"/>
			<lne id="75" begin="7" end="8"/>
			<lne id="76" begin="9" end="14"/>
			<lne id="77" begin="7" end="15"/>
			<lne id="78" begin="0" end="20"/>
			<lne id="79" begin="0" end="21"/>
			<lne id="80" begin="0" end="22"/>
			<lne id="81" begin="0" end="23"/>
			<lne id="82" begin="0" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="68" begin="6" end="19"/>
			<lve slot="0" name="3" begin="0" end="24"/>
		</localvariabletable>
	</operation>
	<operation name="83">
		<context type="10"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<get arg="7"/>
		</code>
		<linenumbertable>
			<lne id="84" begin="0" end="0"/>
			<lne id="85" begin="0" end="1"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="1"/>
		</localvariabletable>
	</operation>
	<operation name="86">
		<context type="5"/>
		<parameters>
			<parameter name="33" type="87"/>
			<parameter name="37" type="87"/>
			<parameter name="88" type="87"/>
		</parameters>
		<code>
			<push arg="34"/>
			<push arg="35"/>
			<new/>
			<push arg="34"/>
			<push arg="35"/>
			<new/>
			<load arg="33"/>
			<iterate/>
			<store arg="89"/>
			<load arg="89"/>
			<call arg="13"/>
			<load arg="37"/>
			<call arg="41"/>
			<call arg="43"/>
			<if arg="90"/>
			<load arg="89"/>
			<call arg="45"/>
			<enditerate/>
			<iterate/>
			<store arg="89"/>
			<getasm/>
			<load arg="89"/>
			<load arg="88"/>
			<call arg="91"/>
			<call arg="45"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="92" begin="6" end="6"/>
			<lne id="93" begin="9" end="9"/>
			<lne id="94" begin="9" end="10"/>
			<lne id="95" begin="11" end="11"/>
			<lne id="96" begin="9" end="12"/>
			<lne id="97" begin="3" end="17"/>
			<lne id="98" begin="20" end="20"/>
			<lne id="99" begin="21" end="21"/>
			<lne id="100" begin="22" end="22"/>
			<lne id="101" begin="20" end="23"/>
			<lne id="102" begin="0" end="25"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="4" name="103" begin="8" end="16"/>
			<lve slot="4" name="104" begin="19" end="24"/>
			<lve slot="0" name="3" begin="0" end="25"/>
			<lve slot="1" name="105" begin="0" end="25"/>
			<lve slot="2" name="69" begin="0" end="25"/>
			<lve slot="3" name="106" begin="0" end="25"/>
		</localvariabletable>
	</operation>
	<operation name="107">
		<context type="108"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<get arg="7"/>
		</code>
		<linenumbertable>
			<lne id="109" begin="0" end="0"/>
			<lne id="110" begin="0" end="1"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="1"/>
		</localvariabletable>
	</operation>
	<operation name="111">
		<context type="10"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<push arg="112"/>
			<push arg="113"/>
			<findme/>
			<call arg="114"/>
			<pushf/>
			<push arg="34"/>
			<push arg="35"/>
			<new/>
			<load arg="6"/>
			<get arg="115"/>
			<iterate/>
			<store arg="33"/>
			<load arg="33"/>
			<call arg="116"/>
			<call arg="43"/>
			<if arg="117"/>
			<load arg="33"/>
			<call arg="45"/>
			<enditerate/>
			<iterate/>
			<store arg="33"/>
			<load arg="33"/>
			<get arg="118"/>
			<get arg="38"/>
			<push arg="39"/>
			<push arg="35"/>
			<new/>
			<dup/>
			<push arg="40"/>
			<set arg="7"/>
			<call arg="41"/>
			<call arg="119"/>
			<enditerate/>
			<call arg="42"/>
		</code>
		<linenumbertable>
			<lne id="120" begin="0" end="0"/>
			<lne id="121" begin="1" end="3"/>
			<lne id="122" begin="0" end="4"/>
			<lne id="123" begin="9" end="9"/>
			<lne id="124" begin="9" end="10"/>
			<lne id="125" begin="13" end="13"/>
			<lne id="126" begin="13" end="14"/>
			<lne id="127" begin="6" end="19"/>
			<lne id="128" begin="22" end="22"/>
			<lne id="129" begin="22" end="23"/>
			<lne id="130" begin="22" end="24"/>
			<lne id="131" begin="25" end="30"/>
			<lne id="132" begin="22" end="31"/>
			<lne id="133" begin="5" end="33"/>
			<lne id="134" begin="0" end="34"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="135" begin="12" end="18"/>
			<lve slot="1" name="136" begin="21" end="32"/>
			<lve slot="0" name="3" begin="0" end="34"/>
		</localvariabletable>
	</operation>
	<operation name="137">
		<context type="18"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<get arg="118"/>
			<push arg="138"/>
			<push arg="113"/>
			<findme/>
			<call arg="114"/>
		</code>
		<linenumbertable>
			<lne id="139" begin="0" end="0"/>
			<lne id="140" begin="0" end="1"/>
			<lne id="141" begin="2" end="4"/>
			<lne id="142" begin="0" end="5"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="5"/>
		</localvariabletable>
	</operation>
	<operation name="143">
		<context type="18"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<call arg="116"/>
			<if arg="144"/>
			<pushf/>
			<goto arg="145"/>
			<load arg="6"/>
			<get arg="118"/>
			<get arg="38"/>
			<push arg="39"/>
			<push arg="35"/>
			<new/>
			<dup/>
			<push arg="40"/>
			<set arg="7"/>
			<call arg="41"/>
		</code>
		<linenumbertable>
			<lne id="146" begin="0" end="0"/>
			<lne id="147" begin="0" end="1"/>
			<lne id="148" begin="3" end="3"/>
			<lne id="149" begin="5" end="5"/>
			<lne id="150" begin="5" end="6"/>
			<lne id="151" begin="5" end="7"/>
			<lne id="152" begin="8" end="13"/>
			<lne id="153" begin="5" end="14"/>
			<lne id="154" begin="0" end="14"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="14"/>
		</localvariabletable>
	</operation>
	<operation name="155">
		<context type="18"/>
		<parameters>
			<parameter name="33" type="87"/>
			<parameter name="37" type="87"/>
		</parameters>
		<code>
			<pusht/>
			<store arg="88"/>
			<load arg="33"/>
			<iterate/>
			<store arg="89"/>
			<pushf/>
			<load arg="37"/>
			<iterate/>
			<store arg="144"/>
			<load arg="144"/>
			<load arg="89"/>
			<get arg="7"/>
			<call arg="41"/>
			<call arg="119"/>
			<enditerate/>
			<if arg="156"/>
			<pushf/>
			<goto arg="157"/>
			<getasm/>
			<load arg="6"/>
			<load arg="89"/>
			<call arg="158"/>
			<store arg="88"/>
			<enditerate/>
			<load arg="88"/>
		</code>
		<linenumbertable>
			<lne id="159" begin="0" end="0"/>
			<lne id="160" begin="2" end="2"/>
			<lne id="161" begin="6" end="6"/>
			<lne id="162" begin="9" end="9"/>
			<lne id="163" begin="10" end="10"/>
			<lne id="164" begin="10" end="11"/>
			<lne id="165" begin="9" end="12"/>
			<lne id="166" begin="5" end="14"/>
			<lne id="167" begin="16" end="16"/>
			<lne id="168" begin="18" end="18"/>
			<lne id="169" begin="19" end="19"/>
			<lne id="170" begin="20" end="20"/>
			<lne id="171" begin="18" end="21"/>
			<lne id="172" begin="5" end="21"/>
			<lne id="173" begin="0" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="5" name="174" begin="8" end="13"/>
			<lve slot="4" name="175" begin="4" end="22"/>
			<lve slot="3" name="176" begin="1" end="24"/>
			<lve slot="0" name="3" begin="0" end="24"/>
			<lve slot="1" name="177" begin="0" end="24"/>
			<lve slot="2" name="178" begin="0" end="24"/>
		</localvariabletable>
	</operation>
	<operation name="179">
		<context type="18"/>
		<parameters>
			<parameter name="33" type="87"/>
			<parameter name="37" type="87"/>
		</parameters>
		<code>
			<pusht/>
			<store arg="88"/>
			<load arg="33"/>
			<iterate/>
			<store arg="89"/>
			<pushf/>
			<load arg="37"/>
			<iterate/>
			<store arg="144"/>
			<load arg="144"/>
			<load arg="89"/>
			<get arg="7"/>
			<call arg="41"/>
			<call arg="119"/>
			<enditerate/>
			<if arg="156"/>
			<pushf/>
			<goto arg="157"/>
			<getasm/>
			<load arg="6"/>
			<load arg="89"/>
			<call arg="180"/>
			<store arg="88"/>
			<enditerate/>
			<load arg="88"/>
		</code>
		<linenumbertable>
			<lne id="181" begin="0" end="0"/>
			<lne id="182" begin="2" end="2"/>
			<lne id="183" begin="6" end="6"/>
			<lne id="184" begin="9" end="9"/>
			<lne id="185" begin="10" end="10"/>
			<lne id="186" begin="10" end="11"/>
			<lne id="187" begin="9" end="12"/>
			<lne id="188" begin="5" end="14"/>
			<lne id="189" begin="16" end="16"/>
			<lne id="190" begin="18" end="18"/>
			<lne id="191" begin="19" end="19"/>
			<lne id="192" begin="20" end="20"/>
			<lne id="193" begin="18" end="21"/>
			<lne id="194" begin="5" end="21"/>
			<lne id="195" begin="0" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="5" name="174" begin="8" end="13"/>
			<lve slot="4" name="175" begin="4" end="22"/>
			<lve slot="3" name="176" begin="1" end="24"/>
			<lve slot="0" name="3" begin="0" end="24"/>
			<lve slot="1" name="177" begin="0" end="24"/>
			<lve slot="2" name="196" begin="0" end="24"/>
		</localvariabletable>
	</operation>
</asm>
