<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="PTnetHelpers"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="self"/>
		<constant value="isInitState"/>
		<constant value="Mptnet!Place;"/>
		<constant value="0"/>
		<constant value="ptInitMarking"/>
		<constant value="1"/>
		<constant value="J.=(J):J"/>
		<constant value="19:5-19:9"/>
		<constant value="19:5-19:23"/>
		<constant value="19:26-19:27"/>
		<constant value="19:5-19:27"/>
		<constant value="getSmName"/>
		<constant value="Mptnet!Node;"/>
		<constant value="J.refImmediateComposite():J"/>
		<constant value="name"/>
		<constant value="26:5-26:9"/>
		<constant value="26:5-26:33"/>
		<constant value="26:5-26:38"/>
		<constant value="getSmPropertiesFromTransition"/>
		<constant value="J"/>
		<constant value="2"/>
		<constant value="J.get(J):J"/>
		<constant value="3"/>
		<constant value="J.oclIsUndefined():J"/>
		<constant value="9"/>
		<constant value="12"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="33:60-33:70"/>
		<constant value="33:76-33:82"/>
		<constant value="33:60-33:84"/>
		<constant value="34:8-34:20"/>
		<constant value="34:8-34:37"/>
		<constant value="36:10-36:22"/>
		<constant value="35:10-35:21"/>
		<constant value="34:5-37:10"/>
		<constant value="33:5-37:10"/>
		<constant value="smProperties"/>
		<constant value="properties"/>
		<constant value="smName"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="-1"/>
		</localvariabletable>
	</operation>
	<operation name="4">
		<context type="5"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<get arg="7"/>
			<pushi arg="8"/>
			<call arg="9"/>
		</code>
		<linenumbertable>
			<lne id="10" begin="0" end="0"/>
			<lne id="11" begin="0" end="1"/>
			<lne id="12" begin="2" end="2"/>
			<lne id="13" begin="0" end="3"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="3"/>
		</localvariabletable>
	</operation>
	<operation name="14">
		<context type="15"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<call arg="16"/>
			<get arg="17"/>
		</code>
		<linenumbertable>
			<lne id="18" begin="0" end="0"/>
			<lne id="19" begin="0" end="1"/>
			<lne id="20" begin="0" end="2"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="2"/>
		</localvariabletable>
	</operation>
	<operation name="21">
		<context type="2"/>
		<parameters>
			<parameter name="8" type="22"/>
			<parameter name="23" type="22"/>
		</parameters>
		<code>
			<load arg="8"/>
			<load arg="23"/>
			<call arg="24"/>
			<store arg="25"/>
			<load arg="25"/>
			<call arg="26"/>
			<if arg="27"/>
			<load arg="25"/>
			<goto arg="28"/>
			<push arg="29"/>
			<push arg="30"/>
			<new/>
		</code>
		<linenumbertable>
			<lne id="31" begin="0" end="0"/>
			<lne id="32" begin="1" end="1"/>
			<lne id="33" begin="0" end="2"/>
			<lne id="34" begin="4" end="4"/>
			<lne id="35" begin="4" end="5"/>
			<lne id="36" begin="7" end="7"/>
			<lne id="37" begin="9" end="11"/>
			<lne id="38" begin="4" end="11"/>
			<lne id="39" begin="0" end="11"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="40" begin="3" end="11"/>
			<lve slot="0" name="3" begin="0" end="11"/>
			<lve slot="1" name="41" begin="0" end="11"/>
			<lve slot="2" name="42" begin="0" end="11"/>
		</localvariabletable>
	</operation>
</asm>
