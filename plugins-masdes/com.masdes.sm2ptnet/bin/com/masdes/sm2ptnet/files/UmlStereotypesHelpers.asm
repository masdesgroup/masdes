<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="UmlStereotypesHelpers"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="self"/>
		<constant value="getStName"/>
		<constant value="S"/>
		<constant value="0"/>
		<constant value=":"/>
		<constant value="J.lastIndexOf(J):J"/>
		<constant value="2"/>
		<constant value="J.+(J):J"/>
		<constant value="J.size():J"/>
		<constant value="J.substring(JJ):J"/>
		<constant value="20:5-20:9"/>
		<constant value="20:21-20:25"/>
		<constant value="20:39-20:42"/>
		<constant value="20:21-20:44"/>
		<constant value="20:47-20:48"/>
		<constant value="20:21-20:48"/>
		<constant value="20:50-20:54"/>
		<constant value="20:50-20:61"/>
		<constant value="20:5-20:62"/>
		<constant value="getStPropValue"/>
		<constant value="Muml!Transition;"/>
		<constant value="1"/>
		<constant value="J"/>
		<constant value="J.getValue(JJ):J"/>
		<constant value="31:5-31:9"/>
		<constant value="31:20-31:30"/>
		<constant value="31:32-31:35"/>
		<constant value="31:5-31:37"/>
		<constant value="stereotype"/>
		<constant value="tag"/>
		<constant value="getStPropLabel"/>
		<constant value="J.getStPropValue(JJ):J"/>
		<constant value="3"/>
		<constant value="base_NamedElement"/>
		<constant value="J.getLabel():J"/>
		<constant value="42:33-42:37"/>
		<constant value="42:54-42:64"/>
		<constant value="42:66-42:69"/>
		<constant value="42:33-42:71"/>
		<constant value="44:5-44:9"/>
		<constant value="44:5-44:27"/>
		<constant value="44:5-44:39"/>
		<constant value="42:5-44:39"/>
		<constant value="prop"/>
		<constant value="getStereotypePropertiesLabels"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="J.getStPropLabel(JJ):J"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="50:5-50:9"/>
		<constant value="50:34-50:38"/>
		<constant value="50:55-50:65"/>
		<constant value="50:67-50:78"/>
		<constant value="50:34-50:80"/>
		<constant value="50:5-50:82"/>
		<constant value="propertyTag"/>
		<constant value="tags"/>
		<constant value="getAppliedSt"/>
		<constant value="J.getAppliedStereotypes():J"/>
		<constant value="name"/>
		<constant value="J.getStName():J"/>
		<constant value="J.=(J):J"/>
		<constant value="B.not():B"/>
		<constant value="16"/>
		<constant value="J.first():J"/>
		<constant value="60:5-60:9"/>
		<constant value="60:5-60:34"/>
		<constant value="60:49-60:51"/>
		<constant value="60:49-60:56"/>
		<constant value="60:49-60:68"/>
		<constant value="60:71-60:77"/>
		<constant value="60:49-60:77"/>
		<constant value="60:5-60:79"/>
		<constant value="60:5-61:15"/>
		<constant value="st"/>
		<constant value="stName"/>
		<constant value="isTagDefined"/>
		<constant value="J.oclIsUndefined():J"/>
		<constant value="18"/>
		<constant value="J.isStApplied(J):J"/>
		<constant value="11"/>
		<constant value="17"/>
		<constant value="J.not():J"/>
		<constant value="19"/>
		<constant value="73:8-73:10"/>
		<constant value="73:8-73:28"/>
		<constant value="75:12-75:16"/>
		<constant value="75:30-75:32"/>
		<constant value="75:30-75:37"/>
		<constant value="75:30-75:50"/>
		<constant value="75:12-75:52"/>
		<constant value="77:14-77:19"/>
		<constant value="76:22-76:26"/>
		<constant value="76:43-76:45"/>
		<constant value="76:47-76:50"/>
		<constant value="76:22-76:52"/>
		<constant value="76:22-76:70"/>
		<constant value="76:18-76:70"/>
		<constant value="75:9-78:14"/>
		<constant value="73:34-73:39"/>
		<constant value="73:5-79:10"/>
		<constant value="isTransitionDefined"/>
		<constant value="34"/>
		<constant value="4"/>
		<constant value="5"/>
		<constant value="J.isTagDefined(JJ):J"/>
		<constant value="J.and(J):J"/>
		<constant value="35"/>
		<constant value="91:8-91:10"/>
		<constant value="91:8-91:27"/>
		<constant value="93:56-93:60"/>
		<constant value="93:10-93:17"/>
		<constant value="94:10-94:15"/>
		<constant value="94:20-94:24"/>
		<constant value="94:39-94:41"/>
		<constant value="94:43-94:51"/>
		<constant value="94:20-94:53"/>
		<constant value="94:10-94:53"/>
		<constant value="93:10-94:55"/>
		<constant value="95:56-95:60"/>
		<constant value="95:7-95:17"/>
		<constant value="96:10-96:15"/>
		<constant value="96:24-96:28"/>
		<constant value="96:43-96:45"/>
		<constant value="96:47-96:55"/>
		<constant value="96:24-96:57"/>
		<constant value="96:20-96:57"/>
		<constant value="96:10-96:57"/>
		<constant value="95:7-96:59"/>
		<constant value="93:10-96:59"/>
		<constant value="92:10-92:15"/>
		<constant value="91:5-97:7"/>
		<constant value="stepName"/>
		<constant value="isDef"/>
		<constant value="daSteps"/>
		<constant value="notDaSteps"/>
		<constant value="isStApplied"/>
		<constant value="B.or(B):B"/>
		<constant value="108:5-108:9"/>
		<constant value="108:5-108:34"/>
		<constant value="108:49-108:51"/>
		<constant value="108:49-108:56"/>
		<constant value="108:59-108:65"/>
		<constant value="108:49-108:65"/>
		<constant value="108:5-108:67"/>
		<constant value="stPath"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="-1"/>
		</localvariabletable>
	</operation>
	<operation name="4">
		<context type="5"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<load arg="6"/>
			<push arg="7"/>
			<call arg="8"/>
			<pushi arg="9"/>
			<call arg="10"/>
			<load arg="6"/>
			<call arg="11"/>
			<call arg="12"/>
		</code>
		<linenumbertable>
			<lne id="13" begin="0" end="0"/>
			<lne id="14" begin="1" end="1"/>
			<lne id="15" begin="2" end="2"/>
			<lne id="16" begin="1" end="3"/>
			<lne id="17" begin="4" end="4"/>
			<lne id="18" begin="1" end="5"/>
			<lne id="19" begin="6" end="6"/>
			<lne id="20" begin="6" end="7"/>
			<lne id="21" begin="0" end="8"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="8"/>
		</localvariabletable>
	</operation>
	<operation name="22">
		<context type="23"/>
		<parameters>
			<parameter name="24" type="25"/>
			<parameter name="9" type="25"/>
		</parameters>
		<code>
			<load arg="6"/>
			<load arg="24"/>
			<load arg="9"/>
			<call arg="26"/>
		</code>
		<linenumbertable>
			<lne id="27" begin="0" end="0"/>
			<lne id="28" begin="1" end="1"/>
			<lne id="29" begin="2" end="2"/>
			<lne id="30" begin="0" end="3"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="3"/>
			<lve slot="1" name="31" begin="0" end="3"/>
			<lve slot="2" name="32" begin="0" end="3"/>
		</localvariabletable>
	</operation>
	<operation name="33">
		<context type="23"/>
		<parameters>
			<parameter name="24" type="25"/>
			<parameter name="9" type="25"/>
		</parameters>
		<code>
			<load arg="6"/>
			<load arg="24"/>
			<load arg="9"/>
			<call arg="34"/>
			<store arg="35"/>
			<load arg="35"/>
			<get arg="36"/>
			<call arg="37"/>
		</code>
		<linenumbertable>
			<lne id="38" begin="0" end="0"/>
			<lne id="39" begin="1" end="1"/>
			<lne id="40" begin="2" end="2"/>
			<lne id="41" begin="0" end="3"/>
			<lne id="42" begin="5" end="5"/>
			<lne id="43" begin="5" end="6"/>
			<lne id="44" begin="5" end="7"/>
			<lne id="45" begin="0" end="7"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="46" begin="4" end="7"/>
			<lve slot="0" name="3" begin="0" end="7"/>
			<lve slot="1" name="31" begin="0" end="7"/>
			<lve slot="2" name="32" begin="0" end="7"/>
		</localvariabletable>
	</operation>
	<operation name="47">
		<context type="23"/>
		<parameters>
			<parameter name="24" type="25"/>
			<parameter name="9" type="25"/>
		</parameters>
		<code>
			<push arg="48"/>
			<push arg="49"/>
			<new/>
			<load arg="9"/>
			<iterate/>
			<store arg="35"/>
			<load arg="6"/>
			<load arg="24"/>
			<load arg="35"/>
			<call arg="50"/>
			<call arg="51"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="52" begin="3" end="3"/>
			<lne id="53" begin="6" end="6"/>
			<lne id="54" begin="7" end="7"/>
			<lne id="55" begin="8" end="8"/>
			<lne id="56" begin="6" end="9"/>
			<lne id="57" begin="0" end="11"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="58" begin="5" end="10"/>
			<lve slot="0" name="3" begin="0" end="11"/>
			<lve slot="1" name="31" begin="0" end="11"/>
			<lve slot="2" name="59" begin="0" end="11"/>
		</localvariabletable>
	</operation>
	<operation name="60">
		<context type="23"/>
		<parameters>
			<parameter name="24" type="25"/>
		</parameters>
		<code>
			<push arg="48"/>
			<push arg="49"/>
			<new/>
			<load arg="6"/>
			<call arg="61"/>
			<iterate/>
			<store arg="9"/>
			<load arg="9"/>
			<get arg="62"/>
			<call arg="63"/>
			<load arg="24"/>
			<call arg="64"/>
			<call arg="65"/>
			<if arg="66"/>
			<load arg="9"/>
			<call arg="51"/>
			<enditerate/>
			<call arg="67"/>
		</code>
		<linenumbertable>
			<lne id="68" begin="3" end="3"/>
			<lne id="69" begin="3" end="4"/>
			<lne id="70" begin="7" end="7"/>
			<lne id="71" begin="7" end="8"/>
			<lne id="72" begin="7" end="9"/>
			<lne id="73" begin="10" end="10"/>
			<lne id="74" begin="7" end="11"/>
			<lne id="75" begin="0" end="16"/>
			<lne id="76" begin="0" end="17"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="77" begin="6" end="15"/>
			<lve slot="0" name="3" begin="0" end="17"/>
			<lve slot="1" name="78" begin="0" end="17"/>
		</localvariabletable>
	</operation>
	<operation name="79">
		<context type="23"/>
		<parameters>
			<parameter name="24" type="25"/>
			<parameter name="9" type="25"/>
		</parameters>
		<code>
			<load arg="24"/>
			<call arg="80"/>
			<if arg="81"/>
			<load arg="6"/>
			<load arg="24"/>
			<get arg="62"/>
			<call arg="63"/>
			<call arg="82"/>
			<if arg="83"/>
			<pushf/>
			<goto arg="84"/>
			<load arg="6"/>
			<load arg="24"/>
			<load arg="9"/>
			<call arg="34"/>
			<call arg="80"/>
			<call arg="85"/>
			<goto arg="86"/>
			<pushf/>
		</code>
		<linenumbertable>
			<lne id="87" begin="0" end="0"/>
			<lne id="88" begin="0" end="1"/>
			<lne id="89" begin="3" end="3"/>
			<lne id="90" begin="4" end="4"/>
			<lne id="91" begin="4" end="5"/>
			<lne id="92" begin="4" end="6"/>
			<lne id="93" begin="3" end="7"/>
			<lne id="94" begin="9" end="9"/>
			<lne id="95" begin="11" end="11"/>
			<lne id="96" begin="12" end="12"/>
			<lne id="97" begin="13" end="13"/>
			<lne id="98" begin="11" end="14"/>
			<lne id="99" begin="11" end="15"/>
			<lne id="100" begin="11" end="16"/>
			<lne id="101" begin="3" end="16"/>
			<lne id="102" begin="18" end="18"/>
			<lne id="103" begin="0" end="18"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="18"/>
			<lve slot="1" name="77" begin="0" end="18"/>
			<lve slot="2" name="32" begin="0" end="18"/>
		</localvariabletable>
	</operation>
	<operation name="104">
		<context type="23"/>
		<parameters>
			<parameter name="24" type="25"/>
			<parameter name="9" type="25"/>
			<parameter name="35" type="25"/>
		</parameters>
		<code>
			<load arg="24"/>
			<call arg="80"/>
			<if arg="105"/>
			<pusht/>
			<store arg="106"/>
			<load arg="9"/>
			<iterate/>
			<store arg="107"/>
			<load arg="106"/>
			<load arg="6"/>
			<load arg="24"/>
			<load arg="107"/>
			<call arg="108"/>
			<call arg="109"/>
			<store arg="106"/>
			<enditerate/>
			<load arg="106"/>
			<pusht/>
			<store arg="106"/>
			<load arg="35"/>
			<iterate/>
			<store arg="107"/>
			<load arg="106"/>
			<load arg="6"/>
			<load arg="24"/>
			<load arg="107"/>
			<call arg="108"/>
			<call arg="85"/>
			<call arg="109"/>
			<store arg="106"/>
			<enditerate/>
			<load arg="106"/>
			<call arg="109"/>
			<goto arg="110"/>
			<pushf/>
		</code>
		<linenumbertable>
			<lne id="111" begin="0" end="0"/>
			<lne id="112" begin="0" end="1"/>
			<lne id="113" begin="3" end="3"/>
			<lne id="114" begin="5" end="5"/>
			<lne id="115" begin="8" end="8"/>
			<lne id="116" begin="9" end="9"/>
			<lne id="117" begin="10" end="10"/>
			<lne id="118" begin="11" end="11"/>
			<lne id="119" begin="9" end="12"/>
			<lne id="120" begin="8" end="13"/>
			<lne id="121" begin="3" end="16"/>
			<lne id="122" begin="17" end="17"/>
			<lne id="123" begin="19" end="19"/>
			<lne id="124" begin="22" end="22"/>
			<lne id="125" begin="23" end="23"/>
			<lne id="126" begin="24" end="24"/>
			<lne id="127" begin="25" end="25"/>
			<lne id="128" begin="23" end="26"/>
			<lne id="129" begin="23" end="27"/>
			<lne id="130" begin="22" end="28"/>
			<lne id="131" begin="17" end="31"/>
			<lne id="132" begin="3" end="32"/>
			<lne id="133" begin="34" end="34"/>
			<lne id="134" begin="0" end="34"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="5" name="135" begin="7" end="14"/>
			<lve slot="4" name="136" begin="4" end="16"/>
			<lve slot="5" name="135" begin="21" end="29"/>
			<lve slot="4" name="136" begin="18" end="31"/>
			<lve slot="0" name="3" begin="0" end="34"/>
			<lve slot="1" name="77" begin="0" end="34"/>
			<lve slot="2" name="137" begin="0" end="34"/>
			<lve slot="3" name="138" begin="0" end="34"/>
		</localvariabletable>
	</operation>
	<operation name="139">
		<context type="23"/>
		<parameters>
			<parameter name="24" type="25"/>
		</parameters>
		<code>
			<pushf/>
			<load arg="6"/>
			<call arg="61"/>
			<iterate/>
			<store arg="9"/>
			<load arg="9"/>
			<get arg="62"/>
			<load arg="24"/>
			<call arg="64"/>
			<call arg="140"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="141" begin="1" end="1"/>
			<lne id="142" begin="1" end="2"/>
			<lne id="143" begin="5" end="5"/>
			<lne id="144" begin="5" end="6"/>
			<lne id="145" begin="7" end="7"/>
			<lne id="146" begin="5" end="8"/>
			<lne id="147" begin="0" end="10"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="77" begin="4" end="9"/>
			<lve slot="0" name="3" begin="0" end="10"/>
			<lve slot="1" name="148" begin="0" end="10"/>
		</localvariabletable>
	</operation>
</asm>
