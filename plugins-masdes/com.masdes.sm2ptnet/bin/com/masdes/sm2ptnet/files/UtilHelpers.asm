<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="UtilHelpers"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="self"/>
		<constant value="sum"/>
		<constant value="I"/>
		<constant value="1"/>
		<constant value="J"/>
		<constant value="0"/>
		<constant value="J.+(J):J"/>
		<constant value="20:5-20:9"/>
		<constant value="20:12-20:13"/>
		<constant value="20:5-20:13"/>
		<constant value="n"/>
		<constant value="StringToBoolean"/>
		<constant value="S"/>
		<constant value="J.toLower():J"/>
		<constant value="true"/>
		<constant value="J.=(J):J"/>
		<constant value="17"/>
		<constant value="false"/>
		<constant value="15"/>
		<constant value="Sequence"/>
		<constant value="#native"/>
		<constant value="QJ.first():J"/>
		<constant value="16"/>
		<constant value="18"/>
		<constant value="27:8-27:12"/>
		<constant value="27:8-27:22"/>
		<constant value="27:25-27:31"/>
		<constant value="27:8-27:31"/>
		<constant value="29:13-29:17"/>
		<constant value="29:13-29:27"/>
		<constant value="29:30-29:37"/>
		<constant value="29:13-29:37"/>
		<constant value="31:15-31:27"/>
		<constant value="30:15-30:20"/>
		<constant value="29:10-32:15"/>
		<constant value="28:10-28:14"/>
		<constant value="27:5-33:10"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="-1"/>
		</localvariabletable>
	</operation>
	<operation name="4">
		<context type="5"/>
		<parameters>
			<parameter name="6" type="7"/>
		</parameters>
		<code>
			<load arg="8"/>
			<load arg="6"/>
			<call arg="9"/>
		</code>
		<linenumbertable>
			<lne id="10" begin="0" end="0"/>
			<lne id="11" begin="1" end="1"/>
			<lne id="12" begin="0" end="2"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="2"/>
			<lve slot="1" name="13" begin="0" end="2"/>
		</localvariabletable>
	</operation>
	<operation name="14">
		<context type="15"/>
		<parameters>
		</parameters>
		<code>
			<load arg="8"/>
			<call arg="16"/>
			<push arg="17"/>
			<call arg="18"/>
			<if arg="19"/>
			<load arg="8"/>
			<call arg="16"/>
			<push arg="20"/>
			<call arg="18"/>
			<if arg="21"/>
			<push arg="22"/>
			<push arg="23"/>
			<new/>
			<call arg="24"/>
			<goto arg="25"/>
			<pushf/>
			<goto arg="26"/>
			<pusht/>
		</code>
		<linenumbertable>
			<lne id="27" begin="0" end="0"/>
			<lne id="28" begin="0" end="1"/>
			<lne id="29" begin="2" end="2"/>
			<lne id="30" begin="0" end="3"/>
			<lne id="31" begin="5" end="5"/>
			<lne id="32" begin="5" end="6"/>
			<lne id="33" begin="7" end="7"/>
			<lne id="34" begin="5" end="8"/>
			<lne id="35" begin="10" end="13"/>
			<lne id="36" begin="15" end="15"/>
			<lne id="37" begin="5" end="15"/>
			<lne id="38" begin="17" end="17"/>
			<lne id="39" begin="0" end="17"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="17"/>
		</localvariabletable>
	</operation>
</asm>
