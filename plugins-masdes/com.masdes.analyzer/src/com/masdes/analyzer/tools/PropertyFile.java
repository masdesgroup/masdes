package com.masdes.analyzer.tools;

public class PropertyFile {

    private String stateMachine;
    
    private String recoverabilityState;
    
    private String fireabilityUseCase;
    
    private String causalityCsUseCase;
    
    private String causalityEfUseCase;
    
    private Boolean recoverabilityCheck;
    
    private Boolean deadlockFreenessCheck;
    
    private Boolean fireabilityCheck;
    
    private Boolean causalityCheck;
    
    private String analyzer;
    
    private String pathPropertyFile;

    /**
     * Constructor
     * @param stateMachine the name of the state machine
     * @param analyzer the tool to analyze properties
     * @param pathPropertyFile the path to put the xml resulted file
     */
    public PropertyFile( String stateMachine, String analyzer, String pathPropertyFile ) {
        
        super();
        this.stateMachine = stateMachine;
        this.analyzer = analyzer;
        this.pathPropertyFile = pathPropertyFile;
    }

    public String getStateMachine() {
        
        return stateMachine;
    }

    public String getRecoverabilityState() {
        
        return recoverabilityState;
    }

    public String getFireabilityUseCase() {
        
        return fireabilityUseCase;
    }

    public String getCausalityCsUseCase() {
        
        return causalityCsUseCase;
    }

    public String getCausalityEfUseCase() {
        
        return causalityEfUseCase;
    }

    public Boolean getRecoverabilityCheck() {
        
        return recoverabilityCheck;
    }

    public Boolean getDeadlockFreenessCheck() {
        
        return deadlockFreenessCheck;
    }

    public Boolean getFireabilityCheck() {
        
        return fireabilityCheck;
    }

    public Boolean getCausalityCheck() {
        
        return causalityCheck;
    }

    public String getAnalyzer() {
        
        return analyzer;
    }
    
    public String getPathPropertyFile() {
        
        return pathPropertyFile;
    }

    public void setStateMachine( String stateMachine ) {
        
        this.stateMachine = stateMachine;
    }

    public void setRecoverabilityState( String recoverabilityState ) {
        
        this.recoverabilityState = recoverabilityState;
    }

    public void setFireabilityUseCase( String fireabilityUseCase ) {
        
        this.fireabilityUseCase = fireabilityUseCase;
    }

    public void setCausalityCsUseCase( String causalityCsUseCase ) {
        
        this.causalityCsUseCase = causalityCsUseCase;
    }

    public void setCausalityEfUseCase( String causalityEfUseCase ) {
        
        this.causalityEfUseCase = causalityEfUseCase;
    }

    public void setRecoverabilityCheck( Boolean recoverabilityCheck ) {
        
        this.recoverabilityCheck = recoverabilityCheck;
    }

    public void setDeadlockFreenessCheck( Boolean deadlockFreenessCheck ) {
        
        this.deadlockFreenessCheck = deadlockFreenessCheck;
    }

    public void setFireabilityCheck( Boolean fireabilityCheck ) {
        
        this.fireabilityCheck = fireabilityCheck;
    }

    public void setCausalityCheck( Boolean causalityCheck ) {
        
        this.causalityCheck = causalityCheck;
    }

    public void setAnalyzer( String analyzer ) {
        
        this.analyzer = analyzer;
    }
    
    public void setPathPropertyFile ( String pathPropertyFile ) {
        
        this.pathPropertyFile = pathPropertyFile;
    }
}
