package com.masdes.analyzer.tools;

public class Resources extends com.masdes.resources.Resources {

    public static final String SOLVER_TINA = "Tina";
    public static final String SOLVER_PROD = "Prod";
    public static final String EMPTY_STRING_CMBOX = "";
    public static final String ST_DA_SERVICE_MODE = "DaServiceMode";
    public static final String ST_DA_SERVICE = "DaService";
    public static final String ST_DA_STEP = "DaStep";
    public static final String ST_DA_RECOGNITION = "DaRecognition";
    public static final String ST_DA_RECOVERY = "DaRecovery";
    public static final String ST_DA_RESISTANCE = "DaResistance";
    
    public static final String GUI_ANALYZER_SELECTION = "You must select an Analyzer Tool";
    public static final String GUI_STATE_MACHINE_SELECTION = "You must select a State Machine";
    public static final String GUI_RECOVERABILITY_SELECTION = "You must select a Recoverability state if checked";
    public static final String GUI_FIREABILITY_SELECTION = "You must select a Fireability use case if checked";
    public static final String GUI_CAUSALITY_SELECTION = "You must select cause and effect use cases if checked";
    
    public static final String PROPERTY_FILE_NAME = "logicalProperties.xml";
    public static final String PROPERTY_ROOT = "logicalProperties";
    public static final String PROPERTY_RECOVERABILITY = "recoverability";
    public static final String PROPERTY_DEADLOCK_FREENESS = "deadlockFreeness";
    public static final String PROPERTY_FIREABILITY = "fireability";
    public static final String PROPERTY_CAUSALITY = "causality";
    public static final String PROPERTY_ATTRIBUTE_NAME_STATE_MACHINE = "stateMachine";
    public static final String PROPERTY_STATE = "state";
    public static final String PROPERTY_USECASE = "useCase";
    public static final String PROPERTY_CAUSE = "cause";
    public static final String PROPERTY_EFFECT = "effect";
    public static final String ATTRIBUTE_NAME = "name";
    public static final String ATTRIBUTE_VALUE = "value";
}
