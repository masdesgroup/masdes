package com.masdes.analyzer.tools;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.masdes.analyzer.tools.Resources;
import com.masdes.resources.ConsoleDisplayManager;

public class XMLWriterDom {
    
    public static void main( PropertyFile xmlFile ) {
        
        ConsoleDisplayManager console = ConsoleDisplayManager.getDefault( Resources.CONSOLE_TITLE );
        
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        
        try {
            
            dBuilder = dbFactory.newDocumentBuilder();
            Document xmlDocument = dBuilder.newDocument();
            
            // Add elements to Document
            Element rootElement = xmlDocument.createElementNS( null, Resources.PROPERTY_ROOT );
            rootElement.setAttribute( Resources.ATTRIBUTE_NAME, Resources.PROPERTY_ATTRIBUTE_NAME_STATE_MACHINE );
            rootElement.setAttribute( Resources.ATTRIBUTE_VALUE, xmlFile.getStateMachine() );
            xmlDocument.appendChild( rootElement );
            
            // Add recoverability
            if ( xmlFile.getRecoverabilityCheck() ) {
                
                rootElement.appendChild( getElementAndChild( xmlDocument, Resources.PROPERTY_RECOVERABILITY, xmlFile.getRecoverabilityState(), Resources.PROPERTY_STATE ) );
            }
            
            // Add deadlockFreenesss
            if ( xmlFile.getDeadlockFreenessCheck() ) {
                
                Element deadlockFreeness = xmlDocument.createElement( Resources.PROPERTY_DEADLOCK_FREENESS );
                rootElement.appendChild( deadlockFreeness );
            }
            
            // Add fireability
            if ( xmlFile.getFireabilityCheck() ) {
                
                rootElement.appendChild( getElementAndChild( xmlDocument, Resources.PROPERTY_FIREABILITY, xmlFile.getFireabilityUseCase(), Resources.PROPERTY_USECASE ) );
            }
            
            // Add causality
            if ( xmlFile.getCausalityCheck() ) {
                
                Element causality = xmlDocument.createElement( Resources.PROPERTY_CAUSALITY );
                causality.appendChild( getElementAndChild( xmlDocument, Resources.PROPERTY_CAUSE, xmlFile.getCausalityCsUseCase(), Resources.PROPERTY_USECASE ) );
                causality.appendChild( getElementAndChild( xmlDocument, Resources.PROPERTY_EFFECT, xmlFile.getCausalityEfUseCase(), Resources.PROPERTY_USECASE ) );
                rootElement.appendChild( causality );
            }
            
            //for output to file, console
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            //for pretty print
            transformer.setOutputProperty( OutputKeys.INDENT, "yes" );
            DOMSource source = new DOMSource( xmlDocument );
 
            //write to console or file
            //StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult( new File( xmlFile.getPathPropertyFile() + Resources.PROPERTY_FILE_NAME ) );
 
            //write data
            //transformer.transform(source, console);
            transformer.transform( source, file );
            
        } catch ( Exception e ) {
            
            String[] errorMessage = { 
                    "Exception",
                    e.getMessage(), 
                    e.getStackTrace().toString() 
            };
            console.println( errorMessage, ConsoleDisplayManager.MSG_TYPE_ERROR );
        }
    }

    private static Node getElementAndChild( Document xmlDocument, String elementLabel, String childName, String childLabel ) {
        
        Element recoverability = xmlDocument.createElement( elementLabel );
        
        String attributeName = "name";
        recoverability.appendChild( createChild( xmlDocument, attributeName, childName, childLabel ) );
        return recoverability;
    }

    private static Node createChild( Document xmlDocument, String name, String value, String childElement ) {
        
        Element state = xmlDocument.createElement( childElement );
        state.setAttribute( Resources.ATTRIBUTE_NAME, name );
        state.setAttribute( Resources.ATTRIBUTE_VALUE, value );
        
        return state;
    }

}
