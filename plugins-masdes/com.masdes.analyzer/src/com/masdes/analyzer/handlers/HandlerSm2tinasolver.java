/**
 * 
 */
package com.masdes.analyzer.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.masdes.resources.Resources;
import com.masdes.solvers.handlers.HandlerSolverTina;
import com.masdes.transformations.handlers.HandlerSm2tina;

/**
 * @author maria
 *
 */
public class HandlerSm2tinasolver extends AbstractHandler {

    private String stateMachineName = "";
    
    protected String getStateMachineName() {
        
        return stateMachineName;
    }
    
    public void setStateMachineName( String stateMachineName) {
        
        this.stateMachineName = stateMachineName;
    }
    /**
     * Constructor
     */
    public HandlerSm2tinasolver() {
        
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute( ExecutionEvent event ) 
            throws ExecutionException {
        
        if ( getStateMachineName().equals( "" ) ) {
            
            throw new ExecutionException( Resources.MSG_STATE_MACHINE_NOT_DEFINED );
        }
        HandlerSm2tina handlerSm2tina = new HandlerSm2tina( getStateMachineName() );
        
        String netFileName = ( String ) handlerSm2tina.execute( event );
        
        HandlerSolverTina handlerSolverTina = new HandlerSolverTina( netFileName );
        handlerSolverTina.execute( event );
        
        return null;
    }

    public Object executeHandler( ExecutionEvent event, Boolean useConsole, IStructuredSelection structuredSelection ) throws ExecutionException {
        
        if ( useConsole ) {
            
            return execute( event );
        } else {

            if ( getStateMachineName().equals( "" ) ) {
                
                throw new ExecutionException( "StateMachine not defined" );
            }
            HandlerSm2tina handlerSm2tina = new HandlerSm2tina( getStateMachineName() );
            String netFileName = ( String ) handlerSm2tina.executeHandler( event, useConsole, structuredSelection );
            
            HandlerSolverTina handlerSolverTina = new HandlerSolverTina( netFileName );
            handlerSolverTina.executeHandler( event, useConsole, structuredSelection );
            return netFileName;
        }
        
        //return null;
    }
}
