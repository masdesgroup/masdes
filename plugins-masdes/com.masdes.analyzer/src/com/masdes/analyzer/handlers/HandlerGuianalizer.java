/**
 * 
 */
package com.masdes.analyzer.handlers;


import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.masdes.analyzer.gui.GuiAnalyzer;
import com.masdes.resources.ConsoleDisplayManager;
import com.masdes.analyzer.tools.Resources;

/**
 * @author maria
 *
 */
public class HandlerGuianalizer extends AbstractHandler {

    public static String[] stateMachines;
    /**
     * 
     */
    public HandlerGuianalizer() {
        // TODO Auto-generated constructor stub
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute( ExecutionEvent event ) 
            throws ExecutionException {
        
        ConsoleDisplayManager console = ConsoleDisplayManager.getDefault( Resources.CONSOLE_TITLE );
        console.clear();
        
        IStructuredSelection structuredSelection = Resources.getStructuredSelection( event );
        
        if ( structuredSelection.getFirstElement() instanceof IFile ) {
            
            IFile inputFile = ( IFile ) structuredSelection.getFirstElement();
            String inputFilePathString = inputFile.getLocation().toString();
            
            GuiAnalyzer.pathUmlFile = inputFilePathString;
            GuiAnalyzer.executionEvent = event;
            GuiAnalyzer.structuredSelection = structuredSelection;
            GuiAnalyzer.main( null );
        }
        
        return null;
    }


}
