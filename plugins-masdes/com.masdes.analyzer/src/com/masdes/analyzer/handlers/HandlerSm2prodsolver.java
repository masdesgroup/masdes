package com.masdes.analyzer.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.masdes.resources.Resources;
import com.masdes.solvers.handlers.HandlerSolverProd;
import com.masdes.transformations.handlers.HandlerSm2prod;

/**
 * @author maria
 *
 */
public class HandlerSm2prodsolver extends AbstractHandler {

    private String stateMachineName = "";
    
    protected String getStateMachineName() {
        
        return stateMachineName;
    }

    public void setStateMachineName( String stateMachineName ) {
        
        this.stateMachineName = stateMachineName;
    }

    /**
     * Constructor
     */
    public HandlerSm2prodsolver() {
        
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute(ExecutionEvent event) 
            throws ExecutionException {
        
        if ( getStateMachineName().equals( "" ) ) {
            
            throw new ExecutionException( Resources.MSG_STATE_MACHINE_NOT_DEFINED );
        }
        HandlerSm2prod handlersm2prod = new HandlerSm2prod( getStateMachineName() );
        
        String netFileName = ( String ) handlersm2prod.execute( event );
        
        HandlerSolverProd handlerSolverProd = new HandlerSolverProd( netFileName );
        handlerSolverProd.execute( event );
        
        return null;
    }
    
    public Object executeHandler( ExecutionEvent event, Boolean useConsole, IStructuredSelection structuredSelection ) throws ExecutionException {
    
        if ( useConsole ) {
            
            return execute( event );
        } else {

            if ( getStateMachineName().equals( "" ) ) {
                
                throw new ExecutionException( "StateMachine not defined" );
            }
            HandlerSm2prod handlerSm2prod = new HandlerSm2prod( getStateMachineName() );
            String netFileName = ( String ) handlerSm2prod.executeHandler( event, useConsole, structuredSelection );
            
            HandlerSolverProd handlerSolverProd = new HandlerSolverProd( netFileName );
            handlerSolverProd.executeHandler( event, useConsole, structuredSelection );
            return netFileName;
        }
    }

}
