package com.masdes.analyzer.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.UMLPackage;

import com.masdes.analyzer.handlers.HandlerSm2prodsolver;
import com.masdes.analyzer.handlers.HandlerSm2tinasolver;
import com.masdes.analyzer.tools.PropertyFile;
import com.masdes.analyzer.tools.Resources;
import com.masdes.analyzer.tools.UmlToGui;
import com.masdes.analyzer.tools.XMLWriterDom;


public class GuiAnalyzer extends JFrame {

    /**
     * 
     */
    private final int DEFAULT_FILL = GridBagConstraints.NONE;
    private final int DEFAULT_ANCHOR = GridBagConstraints.CENTER;
    private final int FIRST_COL = 1;
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;

    private final JLabel lblStateMachine = new JLabel("Select degraded service modes specification:");
    private final JComboBox<String> cmboxStateMachine = new JComboBox<String>();
    private final JCheckBox checkRecoverability = new JCheckBox("System should always recover to");
    private final JComboBox<String> cmboxRecoverability = new JComboBox<String>();
    private final JCheckBox checkDeadlockFreeness = new JCheckBox("There aren't permanent degraded service modes");
    private final JCheckBox checkFireability = new JCheckBox("The strategy ");
    private final JComboBox<String> cmboxFireability = new JComboBox<String>();
    private final JLabel lblFireability = new JLabel(" is feasible");
    private final JCheckBox checkCausalityCs = new JCheckBox("As a response to the threat");
    private final JComboBox<String> cmboxCausalityCs = new JComboBox<String>();
    private final JLabel lblCausalityEf = new JLabel("the system should be able to carry out the strategy");
    private final JComboBox<String> cmboxCausalityEf = new JComboBox<String>();
    private final JLabel lblAnalyzer = new JLabel("PN Analyzer");
    private final JComboBox<String> cmboxAnalyzer = new JComboBox<String>();
    private final JButton btnVerify = new JButton("Verify");
    private final JLabel lblVerifyMsg = new JLabel();
    public static String pathUmlFile;
    public static ExecutionEvent executionEvent;
    public static IStructuredSelection structuredSelection;
    
    
    /**
     * Launch the application.
     */
    public static void main( final String[] args ) {
        EventQueue.invokeLater( new Runnable() {
            public void run() {
                try {
                    GuiAnalyzer frame = new GuiAnalyzer();
                    frame.setVisible( true );
                } catch ( Exception e ) {
                    e.printStackTrace();
                }
            }
        } );
    }

    /**
     * Create the frame.
     */
    public GuiAnalyzer() {
        
        initGUI();
        final Model umlModel = UmlToGui.loadUmlModel( GuiAnalyzer.pathUmlFile );
        
        // Initialize State Machine ComboBox
        final Collection<NamedElement> stateMachines = UmlToGui.loadUmlOwnedElements( umlModel, UMLPackage.Literals.STATE_MACHINE );
        ArrayList<String> stateMachineNames = UmlToGui.getCollectionNames( stateMachines );
        UmlToGui.initComboxWithStrings( cmboxStateMachine, stateMachineNames );
        
        // Initialize Fireability and Causality ComboBoxes
        final Collection<NamedElement> useCases = UmlToGui.loadUmlOwnedElements( umlModel, UMLPackage.Literals.USE_CASE );
        
        ArrayList<String> useCaseNamesDaStep = UmlToGui.filterElementsByStereotype( useCases, Resources.ST_DA_STEP );
        UmlToGui.initComboxWithStrings( cmboxCausalityCs, useCaseNamesDaStep );
        
        ArrayList<String> useCaseNamesDaRecognition = UmlToGui.filterElementsByStereotype( useCases, Resources.ST_DA_RECOGNITION );
        ArrayList<String> useCaseNamesDaRecovery = UmlToGui.filterElementsByStereotype( useCases, Resources.ST_DA_RECOVERY );
        ArrayList<String> useCaseNamesDaResistance = UmlToGui.filterElementsByStereotype( useCases, Resources.ST_DA_RESISTANCE );
        ArrayList<String> useCaseNamesStrategy = useCaseNamesDaRecognition;
        useCaseNamesStrategy.addAll( useCaseNamesDaRecovery );
        useCaseNamesStrategy.addAll( useCaseNamesDaResistance );
        
        UmlToGui.initComboxWithStrings( cmboxFireability, useCaseNamesStrategy );
        UmlToGui.initComboxWithStrings( cmboxCausalityEf, useCaseNamesStrategy );
        
        @SuppressWarnings("serial")
        ArrayList<String> analyzers = new ArrayList<String>() {
            {
                add( Resources.SOLVER_TINA );
                add( Resources.SOLVER_PROD );
            }
        };
        UmlToGui.initComboxWithStrings( cmboxAnalyzer, analyzers );
        
        // Capture event: when a State Machine is selected, Recoverability comboBox is filled with its states.
        cmboxStateMachine.addItemListener( new ItemListener() {
            @Override
            public void itemStateChanged( ItemEvent event ) {
                
                if ( event.getStateChange() == ItemEvent.SELECTED ) {
                    
                    String stateMachineName = ( String ) event.getItem();
                    
                    if ( stateMachineName.equals( Resources.EMPTY_STRING_CMBOX ) ) {
                        
                        cmboxRecoverability.removeAllItems();
                    } else {
                        
                        StateMachine stMachine = UmlToGui.findStateMachine( stateMachines, stateMachineName );
                        
                        Collection<NamedElement> states = UmlToGui.getStatesFromStateMachine( stMachine );
                        
                        //ArrayList<String> stateNames = getCollectionNames( states );
                        //initComboxWithStrings( cmboxRecoverability, stateNames );
                        
                        ArrayList<String> useCaseNamesRecoverability = UmlToGui.filterElementsByStereotype( states, Resources.ST_DA_SERVICE_MODE );
                        UmlToGui.initComboxWithStrings( cmboxRecoverability, useCaseNamesRecoverability );
                        
                        if ( lblVerifyMsg.getText().equals( Resources.GUI_STATE_MACHINE_SELECTION ) ) {
                            
                            lblVerifyMsg.setVisible( false );
                        }
                        
                    }
                    
                }
            }
        } );
        
        // Capture event: when a Recoverability state is selected, checkbox is marked
        cmboxRecoverability.addItemListener( new ItemListener() {
            @Override
            public void itemStateChanged( ItemEvent event ) {
                
                setCheckSelection( event, checkRecoverability );
                
                if ( lblVerifyMsg.getText().equals( Resources.GUI_RECOVERABILITY_SELECTION ) ) {
                    
                    lblVerifyMsg.setVisible( false );
                }
            }

        });
        
        
        // Capture event: when a Fireability usecase is selected, checkbox is marked
        cmboxFireability.addItemListener( new ItemListener() {
            @Override
            public void itemStateChanged( ItemEvent event ) {
                
                setCheckSelection( event, checkFireability );
                
                if ( lblVerifyMsg.getText().equals( Resources.GUI_FIREABILITY_SELECTION ) ) {
                    
                    lblVerifyMsg.setVisible( false );
                }
            }
        });
        
        
        // Capture event: when a Causality usecase is selected, checkbox is marked
        cmboxCausalityCs.addItemListener( new ItemListener() {
            @Override
            public void itemStateChanged( ItemEvent event ) {
                
                setCheckSelection( event, checkCausalityCs );
                
                if ( lblVerifyMsg.getText().equals( Resources.GUI_CAUSALITY_SELECTION ) && 
                     !cmboxCausalityEf.getSelectedItem().toString().equals( Resources.EMPTY_STRING_CMBOX ) ) {
                    
                    lblVerifyMsg.setVisible( false );
                }
            }
        });
        cmboxCausalityEf.addItemListener( new ItemListener() {
            @Override
            public void itemStateChanged( ItemEvent event ) {
                
                if ( lblVerifyMsg.getText().equals( Resources.GUI_CAUSALITY_SELECTION ) && 
                     !cmboxCausalityCs.getSelectedItem().toString().equals( Resources.EMPTY_STRING_CMBOX ) ) {
                    
                    lblVerifyMsg.setVisible( false );
                }
            }
        });
        
        
        // Capture event: when an analyzer tool is selected, error message must disappear
        cmboxAnalyzer.addItemListener( new ItemListener()  {
            
            @Override
            public void itemStateChanged( ItemEvent event ) {
                
                if ( event.getStateChange() == ItemEvent.SELECTED ) {
                    
                    String selectedItem = ( String ) event.getItem();
                    
                    Boolean emptyItemSelected = selectedItem.equals( Resources.EMPTY_STRING_CMBOX );
                    if ( emptyItemSelected.equals( false ) && lblVerifyMsg.getText().equals( Resources.GUI_ANALYZER_SELECTION ) ) {
                        
                        lblVerifyMsg.setVisible( false );
                    }
                }
            }
        });
        
        
        // Event to start transformations and verifications
        btnVerify.addActionListener( new ActionListener() {

            @Override
            public void actionPerformed( ActionEvent event ) {
                
                String errorMessage = "";
                
                String analyzerTool = ( String ) cmboxAnalyzer.getSelectedItem();
                String stateMachineName = ( String ) cmboxStateMachine.getSelectedItem();
                
                // Check if an Analyzer is selected
                if ( analyzerTool.equals( Resources.EMPTY_STRING_CMBOX ) ) {
                    
                    errorMessage = Resources.GUI_ANALYZER_SELECTION;
                }
                
                // Check if a State Machine is selected
                if ( stateMachineName.equals( Resources.EMPTY_STRING_CMBOX ) ) {
                    
                    errorMessage = Resources.GUI_STATE_MACHINE_SELECTION;
                }
                
                // Check if propertycheckboxes are checked, then combox must be selected
                if ( checkRecoverability.isSelected() ) {
                    if ( cmboxRecoverability.getSelectedItem().toString().equals( Resources.EMPTY_STRING_CMBOX ) ) {
                        
                        errorMessage = Resources.GUI_RECOVERABILITY_SELECTION;
                    }
                }
                if ( checkFireability.isSelected() ) {
                    if ( cmboxFireability.getSelectedItem().toString().equals( Resources.EMPTY_STRING_CMBOX ) ) {
                        
                        errorMessage = Resources.GUI_FIREABILITY_SELECTION;
                    }
                }
                if ( checkCausalityCs.isSelected() ) {
                    if ( cmboxCausalityCs.getSelectedItem().toString().equals( Resources.EMPTY_STRING_CMBOX ) || 
                         cmboxCausalityEf.getSelectedItem().toString().equals( Resources.EMPTY_STRING_CMBOX ) ) {
                        
                        errorMessage = Resources.GUI_CAUSALITY_SELECTION;
                    }
                }
                
                if ( errorMessage.equals( Resources.EMPTY_STRING_CMBOX ) ) {
                    
                    final String propertyFilePath = GuiAnalyzer.pathUmlFile.substring( 0 , GuiAnalyzer.pathUmlFile.lastIndexOf( "/" ) + 1 );
                    PropertyFile propertyFile = new PropertyFile( stateMachineName, analyzerTool, propertyFilePath );
                    
                    propertyFile.setRecoverabilityCheck( checkRecoverability.isSelected() );
                    propertyFile.setRecoverabilityState( ( String ) cmboxRecoverability.getSelectedItem() );
                    propertyFile.setDeadlockFreenessCheck( checkDeadlockFreeness.isSelected() );
                    propertyFile.setFireabilityCheck( checkFireability.isSelected() );
                    propertyFile.setFireabilityUseCase( ( String ) cmboxFireability.getSelectedItem() );
                    propertyFile.setCausalityCheck( checkCausalityCs.isSelected() );
                    propertyFile.setCausalityCsUseCase( ( String ) cmboxCausalityCs.getSelectedItem() );
                    propertyFile.setCausalityEfUseCase( ( String ) cmboxCausalityEf.getSelectedItem() );
                    synchronized( this ) {
                        XMLWriterDom.main( propertyFile );
                        
                        String message = "XML File created";
                        lblVerifyMsg.setText( message );
                        lblVerifyMsg.setForeground( Color.BLUE );
                        lblVerifyMsg.setVisible( true );
    
                        switch ( analyzerTool ) {
                        case Resources.SOLVER_TINA:
                            HandlerSm2tinasolver handlerSm2tinasolver = new HandlerSm2tinasolver();
                            handlerSm2tinasolver.setStateMachineName( stateMachineName );
                            
                            try {
                                
                                message = (String) handlerSm2tinasolver.executeHandler( GuiAnalyzer.executionEvent, false, GuiAnalyzer.structuredSelection );
                                lblVerifyMsg.setForeground( Color.BLUE );
                            } catch (ExecutionException e) {
                                
                                message = e.getStackTrace().toString();
                                lblVerifyMsg.setForeground( Color.RED );
                            }
                            lblVerifyMsg.setText( message );
                            break;
                        case Resources.SOLVER_PROD:
                            HandlerSm2prodsolver handlerSm2prodsolver = new HandlerSm2prodsolver();
                            handlerSm2prodsolver.setStateMachineName( stateMachineName );
                            try {
                                
                                message = (String) handlerSm2prodsolver.executeHandler( GuiAnalyzer.executionEvent, false, GuiAnalyzer.structuredSelection );
                                lblVerifyMsg.setForeground( Color.BLUE );
                            } catch (ExecutionException e) {
                                
                                message = e.getStackTrace().toString();
                                lblVerifyMsg.setForeground( Color.RED );
                            }
                            lblVerifyMsg.setText( message );
                            break;
                        default:
                            break;
                        }
                    }
                } else {
                    
                    lblVerifyMsg.setText( errorMessage );
                    lblVerifyMsg.setForeground( Color.RED );
                    lblVerifyMsg.setVisible( true );
                }
            }
            
        });
        
        
    }
    
    
    /**
     * Selects or unselects the checkBox depending on the event item selected
     * @param event
     * @param checkBox
     */
    private void setCheckSelection( ItemEvent event, JCheckBox checkBox ) {
        
        if ( event.getStateChange() == ItemEvent.SELECTED ) {
            
            String selectedItem = ( String ) event.getItem();
            if ( selectedItem.equals( Resources.EMPTY_STRING_CMBOX ) ) {
                
                checkBox.setSelected( false );
            } else {
                
                checkBox.setSelected( true );
            }
        }
    }
    
    
    private void initGUI() {
        
        setFont( new Font( "Ubuntu", Font.BOLD, 14 ) );
        setTitle( "Survivability Properties Analyzer" );
        
        setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
        setBounds( 100, 100, 660, 500 );
        contentPane = new JPanel();
        contentPane.setBorder( new EmptyBorder( 25, 5, 5, 5 ) );
        setContentPane( contentPane );
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30 };
        gbl_contentPane.rowHeights = new int[]{ 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30 };
        gbl_contentPane.columnWeights = new double[]{ 0.0, Double.MIN_VALUE, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0 };
        gbl_contentPane.rowWeights = new double[]{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        contentPane.setLayout(gbl_contentPane);
        
        
        // State Machine selection components
        
        int gridY = 0;
        Insets insets = new Insets( 0, 0, 5, 5 );
        addGridBagToPanel( lblStateMachine, insets, GridBagConstraints.WEST, DEFAULT_FILL, 11, FIRST_COL, gridY );
        addGridBagToPanel( cmboxStateMachine, insets, DEFAULT_ANCHOR, GridBagConstraints.HORIZONTAL, 9, 12, gridY );
        
        
        // Recoverability property components
        
        gridY = gridY + 2;
        addGridBagToPanel( checkRecoverability, insets, GridBagConstraints.WEST, DEFAULT_FILL, 9, FIRST_COL, gridY );
        addGridBagToPanel( cmboxRecoverability, insets, DEFAULT_ANCHOR, GridBagConstraints.HORIZONTAL, 10, 10, gridY );
        
        
        // DeadlockFreeness property components
        
        gridY++;
        addGridBagToPanel( checkDeadlockFreeness, insets, GridBagConstraints.WEST, DEFAULT_FILL, 13, FIRST_COL, gridY );
        
        
        // Fireability property components
        
        gridY++;
        addGridBagToPanel( checkFireability, insets, GridBagConstraints.WEST, DEFAULT_FILL, 4, FIRST_COL, gridY);
        addGridBagToPanel( cmboxFireability, insets, DEFAULT_ANCHOR, GridBagConstraints.HORIZONTAL, 13, 5, gridY );
        addGridBagToPanel( lblFireability, insets, GridBagConstraints.WEST, DEFAULT_FILL, 3, 18, gridY );
        
        
        // Causality property components
        
        gridY++;
        addGridBagToPanel( checkCausalityCs, insets, GridBagConstraints.WEST, DEFAULT_FILL, 8, FIRST_COL, gridY);
        addGridBagToPanel( cmboxCausalityCs, insets, DEFAULT_ANCHOR, GridBagConstraints.HORIZONTAL, 9, 9, gridY );
        
        gridY++;
        addGridBagToPanel( lblCausalityEf, insets, GridBagConstraints.WEST, DEFAULT_FILL, 14, FIRST_COL + 1, gridY );
        
        gridY++;
        addGridBagToPanel( cmboxCausalityEf, insets, DEFAULT_ANCHOR, GridBagConstraints.HORIZONTAL, 16, FIRST_COL + 1, gridY );
        
        // Analyzer selector components
        
        gridY = gridY + 2;
        addGridBagToPanel( lblAnalyzer, insets, GridBagConstraints.WEST, DEFAULT_FILL, 3, FIRST_COL + 1, gridY );
        addGridBagToPanel( cmboxAnalyzer, insets, DEFAULT_ANCHOR, GridBagConstraints.HORIZONTAL, 4, 5, gridY );
        
        // Buttons
        
        gridY++;
        addGridBagToPanel( btnVerify, insets, DEFAULT_ANCHOR, DEFAULT_FILL, 3, 15, gridY );
        
        gridY++;
        addGridBagToPanel( lblVerifyMsg, insets, GridBagConstraints.WEST, DEFAULT_FILL, 14, FIRST_COL + 1, gridY );
        lblVerifyMsg.setVisible( false );
        
        
    }
    
    
    /**
     * @param component
     *          Any java.awt.Component to add to the JPane.
     * @param insets
     *          Specifies the internal padding of the component, how much space to add to the minimum width of the component.
     * @param anchor
     *          This field is used when the component is smaller than its display area.
     *          It determines where, within the display area, to place the component.
     * @param fill 
     *          This field is used when the component's display area is larger than the component's requested size.
     *          It determines whether to resize the component, and if so, how.
     * @param gridWidth
     *          The number of cells in a row for the component's display area.
     * @param gridX 
     *          The cell containing the leading edge of the component's display area.
     * @param gridY
     *          The cell at the top of the component's display area.
     * 
     */
    private void addGridBagToPanel( Component component, Insets insets, int anchor, int fill, int gridWidth, int gridX, int gridY ) {
        
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = insets;
        gridBagConstraints.anchor = anchor;
        gridBagConstraints.fill = fill;
        gridBagConstraints.gridwidth = gridWidth;
        gridBagConstraints.gridx = gridX;
        gridBagConstraints.gridy = gridY;
        contentPane.add( component, gridBagConstraints );
    }
}
