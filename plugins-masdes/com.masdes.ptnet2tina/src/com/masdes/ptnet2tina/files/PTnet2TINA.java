/*******************************************************************************
 * Copyright (c) 2010, 2012 Obeo.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Obeo - initial API and implementation
 *     María Berenguer - final implementation
 *******************************************************************************/
package com.masdes.ptnet2tina.files;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.m2m.atl.common.ATLExecutionException;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IInjector;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.core.IReferenceModel;
import org.eclipse.m2m.atl.core.ModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFInjector;
import org.eclipse.m2m.atl.core.emf.EMFModelFactory;
import org.eclipse.m2m.atl.core.launch.ILauncher;
import org.eclipse.m2m.atl.engine.emfvm.launch.EMFVMLauncher;

import com.masdes.ptnet2tina.tools.Resources;
import com.masdes.resources.ConsoleDisplayManager;
/**
 * Entry point of the 'PTnet2TINA' transformation module.
 */
public class PTnet2TINA {
    
    
    /**
     * The property file. Stores module list, the metamodel and library locations.
     * @generated
     */
    private Properties properties;
    
    
    /**
     * The IN model.
     * @generated
     */
    protected IModel inModel;
    
    
    /**
     * The main method.
     * 
     * @param args
     *            are the arguments
     * @generated
     */
    @SuppressWarnings("unchecked")
    public static String main( String[] args ) {
    
        ConsoleDisplayManager console = ConsoleDisplayManager.getDefault( Resources.CONSOLE_TITLE );
        try {
            
            if ( args.length < 3 ) {
                
                console.println(
                        Resources.MSG_ARGUMENTS_NOT_VALID3, 
                        ConsoleDisplayManager.MSG_TYPE_ERROR 
                );
            } else {
                
                PTnet2TINA runner = new PTnet2TINA();
                
                console.println( 
                        Resources.MSG_LOAD_INPUT_MODEL + args[ 0 ] + Resources.DOTS, 
                        ConsoleDisplayManager.MSG_TYPE_INFORMATION
                );
                runner.loadModels( args[ 0 ] );
                
                console.println( 
                        Resources.MSG_ATL_START, 
                        ConsoleDisplayManager.MSG_TYPE_INFORMATION
                );
                // Get current time
                long startTime = System.currentTimeMillis();
                
                Collection<String> tinaFiles = ( Collection<String> ) runner.doPTnet2TINA( new NullProgressMonitor() );
                
                // Get elapsed time in milliseconds
                long elapsedTime = System.currentTimeMillis() - startTime; 
                float execSeconds = elapsedTime / 1000F;
                String runnerMessage = Resources.MSG_ATL_SUCCESS + execSeconds + Resources.MSG_SECONDS;
                console.println( 
                        runnerMessage,
                        ConsoleDisplayManager.MSG_TYPE_INFORMATION
                );
                
                console.println( 
                        Resources.MSG_RESULT_FILES,
                        ConsoleDisplayManager.MSG_TYPE_INFORMATION
                );
                
                return Resources.createResultFiles( tinaFiles, args[ 1 ], args[ 2 ] );
                
            }
        } catch ( ATLCoreException e ) {
            
            String[] errorMessage = { 
                    "ATLCoreException",
                    e.getMessage(), 
                    e.getStackTrace().toString() 
            };
            console.println( errorMessage, ConsoleDisplayManager.MSG_TYPE_ERROR );
            // e.printStackTrace();
        } catch ( IOException e ) {
            
            String[] errorMessage = { 
                    "IOException",
                    e.getMessage(), 
                    e.getStackTrace().toString() 
            };
            console.println( errorMessage, ConsoleDisplayManager.MSG_TYPE_ERROR );
            // e.printStackTrace();
        } catch ( ATLExecutionException e ) {
            
            String[] errorMessage = { 
                    "ATLExecutionException",
                    e.getMessage(), 
                    e.getStackTrace().toString() 
            };
            console.println( errorMessage, ConsoleDisplayManager.MSG_TYPE_ERROR );
            // e.printStackTrace();
        }
        return null;
    }
    
    
    @SuppressWarnings("unchecked")
    public static String mainPtnet2tina( String[] args, Boolean useConsole ) {
        
        if ( useConsole ) {
            
            return PTnet2TINA.main( args );
        } else {
            
            try {
                
                if ( args.length < 3 ) {
                    
                } else {
                    
                    PTnet2TINA runner = new PTnet2TINA();
                    
                    runner.loadModels( args[ 0 ] );
                    
                    Collection<String> tinaFiles = ( Collection<String> ) runner.doPTnet2TINA( new NullProgressMonitor() );
                    
                    return Resources.createResultFiles( tinaFiles, args[ 1 ], args[ 2 ] );
                    
                }
            } catch ( ATLCoreException e ) {
                
                // e.printStackTrace();
            } catch ( IOException e ) {
                
                // e.printStackTrace();
            } catch ( ATLExecutionException e ) {
                
                // e.printStackTrace();
            }
        }
        return null;
    }
    
    
    /**
     * Constructor.
     *
     * @generated
     * @throws IOException
     *             if the .properties file cannot be read
     */
    public PTnet2TINA()
            throws IOException {
        
        properties = new Properties();
        properties.load( getFileURL( Resources.PTNET2TINA_PROPERTIES_FILE ).openStream() );
        
        Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(
                Resources.EXTENSION_ECORE,
                new EcoreResourceFactoryImpl()
        );
    }
    
    
    /**
     * Load the input and input/output models, initialize output models.
     * 
     * @param inModelPath
     *            the IN model path
     * @throws ATLCoreException
     *             if a problem occurs while loading models
     * @throws IOException 
     *             if a problem occurs in getMetamodelPath
     *
     * @generated
     */
    public void loadModels( String inModelPath )
            throws ATLCoreException, IOException {
        
        ModelFactory factory = new EMFModelFactory();
        IInjector injector = new EMFInjector();
        
        IReferenceModel ptnetMetamodel = factory.newReferenceModel();
        injector.inject( ptnetMetamodel, getMetamodelPath( Resources.METAMODEL_PTNET ) );
        
        this.inModel = factory.newModel( ptnetMetamodel );
        injector.inject( inModel, inModelPath );
    }
    
    
    /**
     * Transform the models.
     * 
     * @param monitor
     *            the progress monitor
     * @throws ATLCoreException
     *             if an error occurs during models handling
     * @throws IOException
     *             if a module cannot be read
     * @throws ATLExecutionException
     *             if an error occurs during the execution
     *
     * @generated
     */
    public Object doPTnet2TINA( IProgressMonitor monitor )
            throws ATLCoreException, IOException, ATLExecutionException {
        
        ILauncher launcher = new EMFVMLauncher();
        Map<String, Object> launcherOptions = getOptions();
        
        launcher.initialize( launcherOptions );
        launcher.addInModel( inModel, Resources.MODEL_IN, Resources.METAMODEL_PTNET );
        
        return launcher.launch(
                Resources.LAUNCHER_RUN,
                monitor,
                launcherOptions,
                ( Object[] ) getModulesList());
    }
    
    
    /**
     * Returns an Array of the module input streams, parameterized by the
     * property file.
     * 
     * @return an Array of the module input streams
     * @throws IOException
     *             if a module cannot be read
     *
     * @generated
     */
    protected InputStream[] getModulesList()
            throws IOException {
        
        InputStream[] modules = null;
        
        final String modulesProp = Resources.ATL_PTNET2TINA + Resources.PROP_MODULES;
        String modulesList = properties.getProperty( modulesProp );
        
        if ( modulesList != null ) {
            
            String[] moduleNames = modulesList.split( Resources.SEPARATOR_MODULES );
            modules = new InputStream [ moduleNames.length ];
            for ( int i = 0; i < moduleNames.length; i++ ) {
                
                String asmModulePath = new Path( moduleNames[ i ].trim() )
                        .removeFileExtension().addFileExtension( Resources.EXTENSION_ASM ).toString();
                modules[ i ] = getFileURL( asmModulePath ).openStream();
            }
        }
        return modules;
    }
    
    
    /**
     * Returns the URI of the given metamodel, parameterized from the property file.
     * 
     * @param metamodelName
     *            the metamodel name
     * @return the metamodel URI
     *
     * @generated
     */
    protected String getMetamodelUri( String metamodelName ) {
        
        final String metamodelString =
                Resources.ATL_PTNET2TINA + Resources.PROP_METAMODELS + Resources.DOT;
        
        return properties.getProperty( metamodelString + metamodelName );
    }
    
    
    /**
     * Returns the path of the given metamodel, parameterized from the property file.
     * 
     * @param metamodelName
     *            the metamodel name
     * @return the metamodel file path
     * @throws IOException
     *             if getFileURL throws exception
     */
    private String getMetamodelPath( String metamodelName ) 
              throws IOException {
        
        return getFileURL( getMetamodelUri( metamodelName ) ).toString();
    }
    
    
    /**
     * Returns the file name of the given library, parameterized from the property file.
     * 
     * @param libraryName
     *            the library name
     * @return the library file name
     *
     * @generated
     */
    protected InputStream getLibraryAsStream( String libraryName )
            throws IOException {
        
        final String libraryString = 
                Resources.ATL_PTNET2TINA + Resources.PROP_LIBRARIES + Resources.DOT;
        
        return getFileURL( properties.getProperty( libraryString + libraryName ) ).openStream();
    }
    
    
    /**
     * Returns the options map, parameterized from the property file.
     * 
     * @return the options map
     *
     * @generated
     */
    protected Map<String, Object> getOptions() {
        
        Map<String, Object> options = new HashMap<String, Object>();
        final String option = 
                Resources.ATL_PTNET2TINA + Resources.PROP_OPTIONS + Resources.DOT;
        
        for ( Entry<Object, Object> entry : properties.entrySet() ) {
            
            if ( entry.getKey().toString().startsWith( option ) ) {
                
                options.put(
                        entry.getKey().toString().replaceFirst( option, "" ), 
                        entry.getValue().toString()
                );
            }
        }
        
        return options;
    }
    
    
    /**
     * Finds the file in the plug-in. Returns the file URL.
     * 
     * @param fileName
     *            the file name
     * @return the file URL
     * @throws IOException
     *             if the file doesn't exist
     * 
     * @generated
     */
    protected static URL getFileURL( String fileName )
            throws IOException {
        
        final URL fileURL;
        if ( Resources.isEclipseRunning() ) {
            
            URL resourceURL = PTnet2TINA.class.getResource( fileName );
            if ( resourceURL != null ) {
                
                fileURL = FileLocator.toFileURL( resourceURL );
            } else {
                
                fileURL = null;
            }
        } else {
            
            fileURL = PTnet2TINA.class.getResource( fileName );
        }
        if ( fileURL == null ) {
            
            throw new IOException( fileName + Resources.MSG_FILE_NOT_FOUND );
        } else {
            
            return fileURL;
        }
    }
}
