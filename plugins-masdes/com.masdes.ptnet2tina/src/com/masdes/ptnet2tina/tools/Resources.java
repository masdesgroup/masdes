package com.masdes.ptnet2tina.tools;


public class Resources extends com.masdes.resources.Resources {

    // PTnet2TINA
    //***********
    
    public static final String ATL_PTNET2TINA = "PTnet2TINA";
    public static final String PTNET2TINA_PROPERTIES_FILE = "PTnet2TINA.properties";
}
