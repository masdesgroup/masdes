package com.masdes.ptnet2prod.tools;

public class Resources extends com.masdes.resources.Resources {

    // PTnet2PROD
    //***********
    
    public static final String ATL_PTNET2PROD = "PTnet2PROD";
    public static final String PTNET2PROD_PROPERTIES_FILE = "PTnet2PROD.properties";
}
