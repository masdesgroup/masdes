/**
 * 
 */
package com.masdes.ptnet2prod.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.masdes.ptnet2prod.files.PTnet2PROD;
import com.masdes.resources.ConsoleDisplayManager;
import com.masdes.resources.Resources;
import com.masdes.resources.handlers.HandlerPtnet2Resource;

/**
 * @author maria
 *
 */
public class HandlerPtnet2prod extends HandlerPtnet2Resource {

    /**
     * Constructor
     */
    public HandlerPtnet2prod( String netFileName ) {
        
        super( netFileName );
        initQueryArgs( 3 );
    }
    
    
    public HandlerPtnet2prod( Path inputFilePath, String netFileName ) {
        
        super( inputFilePath, netFileName );
        initQueryArgs( 3 );
    }
    
    
    /* (non-Javadoc)
     * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    public Object execute( ExecutionEvent event ) 
            throws ExecutionException {
        
        IStructuredSelection structuredSelection = Resources.getStructuredSelection( event );
        ConsoleDisplayManager console = ConsoleDisplayManager.getDefault( Resources.CONSOLE_TITLE );
        
        super.execute( structuredSelection );
        
        //check if it is an IFile
        if ( structuredSelection.getFirstElement() instanceof IFile ) {
            
            // Input model (state machine/s to transform)
            this.setQueryArgs( 0, getInputFilePathString() );
            // Output petri net (following the output ecore model)
            this.setQueryArgs( 1, getOutputDirectoryPathString() );
            // State Machine name
            this.setQueryArgs( 2, getNetFileName() );
            
            try {
                
                String netSolverString = PTnet2PROD.main( getQueryArgs() );
                return netSolverString;
                
            } catch ( final Exception e ) {
                
                String[] errorMessage = {
                        e.getMessage(),
                        e.getStackTrace().toString()
                };
                console.println( 
                        errorMessage, 
                        ConsoleDisplayManager.MSG_TYPE_ERROR 
                );
            }
        }
        
        // Clear inputFPath
        setUseExternalFilePath( false );
        
        return null;
    }
    
    
    public Object executeHandler( ExecutionEvent event, Boolean useConsole, IStructuredSelection structuredSelection ) 
            throws ExecutionException {
        
        if ( useConsole ) {
            
            execute( event );
        } else {
            
            super.execute( structuredSelection );
            
            //check if it is an IFile
            if ( structuredSelection.getFirstElement() instanceof IFile ) {
                
                // Input model (state machine/s to transform)
                this.setQueryArgs( 0, getInputFilePathString() );
                // Output petri net (following the output ecore model)
                this.setQueryArgs( 1, getOutputDirectoryPathString() );
                // State Machine name
                this.setQueryArgs( 2, getNetFileName() );
                
                try {
                    
                    String netSolverString = PTnet2PROD.mainPtnet2prod( getQueryArgs(), useConsole );
                    return netSolverString;
                    
                } catch ( final Exception e ) {
                    
                }
            }
        }
        
        // Clear inputFPath
        setUseExternalFilePath( false );
        
        return null;
    }

}
