#!/bin/sh
# Script to launch TINA, create .ktz, and generate a log file 
# for the query defined in .ktz and .ltl
# Author: María Berenguer / Simona Bernardi
# Params:
#     $1 Absolute path of tina folder (example: /usr/local/tina-3.1.0)
#     $2 Absolute path of plugin results folder (example: $HOME/workspace/ModelProject/results)
#     $3 Name without extension of the .net and .ltl file (must be the same)
cd $2
export TINAPATH=$1/
export PATH=$PATH:$1/bin/
echo "Launch TINA..." > $3.log
tina -r $3.net -ktz $3.ktz >> $3.log
echo "Launch selt..." >> $3.log
selt $3.ktz $3.ltl -v -p >> $3.log
echo "Finished script" >> $3.log
