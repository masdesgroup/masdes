#!/bin/sh
# Script to launch PROD, build RG, calculate the strongly connected components and 
# generate a log file for the query defined in a data file .prb
# Author: María Berenguer / Simona Bernardi
# Params:
#     $1 Absolute path of prod folder (example: /usr/local/prod)
#     $2 Absolute path of plugin results folder (example: $HOME/workspace/ModelProject/results)
#     $3 The name (without extension) of the net file to analyze
cd $2
export PRODPATH=$1/
export PATH=$PATH:$1/bin/
echo "Launching PROD..." > $3.log
prod $3.init >> $3.log
echo "RG constructing..." >> $3.log
./$3 >> $3.log
echo "RG constructed" >> $3.log
strong $3 >> $3.log
echo "Make queries to interactive program probe..." >> $3.log
probe $3.gph -. -l $3.macro -w70 -e  < $3.prb >> $3.log
prod $3.clean
