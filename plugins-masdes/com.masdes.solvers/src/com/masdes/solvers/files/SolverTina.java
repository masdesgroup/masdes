/**
 * 
 */
package com.masdes.solvers.files;

import java.io.IOException;

import com.masdes.solvers.tools.Resources;
import com.masdes.resources.ConsoleDisplayManager;

/**
 * @author maria
 *
 */
public class SolverTina extends LauncherScript {
    
    public SolverTina(  ) {
        
        super( );
    }

    /**
     * @param args
     *         args[ 0 ] = absolute results folder path
     *         args[ 1 ] = results file name without extension
     */
    public static void main( String[] args ) {
        
        ConsoleDisplayManager console = ConsoleDisplayManager.getDefault( Resources.CONSOLE_TITLE );
        
        try {
            if ( args.length < 3 ) {
                
                console.println(
                        Resources.MSG_ARGUMENTS_NOT_VALID3, 
                        ConsoleDisplayManager.MSG_TYPE_ERROR 
                );
            }
            else {
                
                SolverTina solver = new SolverTina();
                
                solver.loadProperties( Resources.SOLVERTINA_PROPERTIES_FILE );
                
                // Construct the command to exec the Script
                console.println( Resources.MSG_LOAD_PARAMETERS, ConsoleDisplayManager.MSG_TYPE_INFORMATION );
                // Call to script
                final String propertyScript = Resources.PROP_SOLVERS + Resources.PROP_SCRIPT;
                
                String scriptFilePathString = solver.getSolverUri( propertyScript );
                
                scriptFilePathString = args[ 2 ] + scriptFilePathString;
                
                solver.setScriptCommand( scriptFilePathString );
                
                // Parameter 1: Tina folder
                final String propertySolver = Resources.PROP_SOLVERS + Resources.PROP_SOLVER;
                solver.addParameterToScriptCommand( solver.properties.getProperty( propertySolver ) );
                // Parameter 2: results folder
                solver.addParameterToScriptCommand( args[ 0 ] );
                // Parameter 3: results file name
                solver.addParameterToScriptCommand( args[ 1 ] );
                
                console.println( Resources.MSG_SOLVER_START, ConsoleDisplayManager.MSG_TYPE_INFORMATION );
                
                synchronized ( solver ) {
                    Runtime.getRuntime().exec( solver.getScriptCommand() );
                }
                //Thread.sleep( 1000 );
                
                // Print results in console
                console.printFile( args[ 0 ] + Resources.PATH_SEPARATOR + args[ 1 ] + Resources.DOT + Resources.EXTENSION_LOG );
                console.println( Resources.MSG_FIN, ConsoleDisplayManager.MSG_TYPE_INFORMATION );
            }
        } catch ( IOException e ) {
            
            String[] errorMessage = { 
                    "IOException",
                    e.getMessage(), 
                    e.getStackTrace().toString() 
            };
            console.println( errorMessage, ConsoleDisplayManager.MSG_TYPE_ERROR );
        } catch (ClassNotFoundException e) {
            
            String[] errorMessage = { 
                    "ClassNotFoundException",
                    e.getMessage(), 
                    e.getStackTrace().toString() 
            };
            console.println( errorMessage, ConsoleDisplayManager.MSG_TYPE_ERROR );
        }
    }
    
    
    public static void mainSolvertina( String[] args, Boolean useConsole ) {
        
        if ( useConsole ) {
            
            SolverTina.main( args );
        } else {
            
            try {
                if ( args.length < 3 ) {
                    
                }
                else {
                    
                    SolverTina solver = new SolverTina();
                    
                    solver.loadProperties( Resources.SOLVERTINA_PROPERTIES_FILE );
                    
                    // Call to script
                    final String propertyScript = Resources.PROP_SOLVERS + Resources.PROP_SCRIPT;
                    
                    String scriptFilePathString = solver.getSolverUri( propertyScript );
                    
                    scriptFilePathString = args[ 2 ] + scriptFilePathString;
                    
                    solver.setScriptCommand( scriptFilePathString );
                    
                    // Parameter 1: Tina folder
                    final String propertySolver = Resources.PROP_SOLVERS + Resources.PROP_SOLVER;
                    solver.addParameterToScriptCommand( solver.properties.getProperty( propertySolver ) );
                    // Parameter 2: results folder
                    solver.addParameterToScriptCommand( args[ 0 ] );
                    // Parameter 3: results file name
                    solver.addParameterToScriptCommand( args[ 1 ] );
                    
                    synchronized ( solver ) {
                        Runtime.getRuntime().exec( solver.getScriptCommand() );
                    }
                    //Thread.sleep( 1000 );
                    
                }
            } catch ( IOException e ) {
                
            } catch (ClassNotFoundException e) {
                
            }
        }
    }
}
