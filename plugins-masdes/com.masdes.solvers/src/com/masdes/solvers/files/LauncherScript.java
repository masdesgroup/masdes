package com.masdes.solvers.files;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.eclipse.core.runtime.FileLocator;

import com.masdes.resources.ConsoleDisplayManager;
import com.masdes.resources.Resources;


public abstract class LauncherScript {

    
    /**
     * The property file. Stores solver info and arguments.
     * @generated
     */
    protected Properties properties;
    
    
    private String scriptCommand;
    
    
    protected String getScriptCommand() {
        
        return scriptCommand;
    }


    protected void setScriptCommand( String scriptCommand ) {
        
        this.scriptCommand = scriptCommand;
    }

    protected void addParameterToScriptCommand ( String parameter ) {
        
        this.scriptCommand = this.scriptCommand + " " + parameter;
    }

    /**
     * 
     * @param launcherName
     */
    public LauncherScript(  ) {
        
        setScriptCommand( "" );
    }
    
    
//    protected void loadProperties( String propertiesFile, String className ) 
//            throws IOException, ClassNotFoundException {
    protected void loadProperties( String propertiesFile ) throws ClassNotFoundException, IOException {
        
        properties = new Properties();
        //properties.load( getFileURL( propertiesFile, className ).openStream() );
        properties.load( getFileURL( propertiesFile ).openStream() );
    }
    
    
    /**
     * Finds the file in the plug-in. Returns the file URL.
     * 
     * @param fileName
     *            the file name
     * @return the file URL
     * @throws ClassNotFoundException 
     * @throws IOException
     *             if the file doesn't exist
     * 
     * @generated
     */
    //protected static URL getFileURL( String fileName, String className ) throws ClassNotFoundException {
    protected static URL getFileURL( String fileName ) throws ClassNotFoundException, IOException {
        
        URL fileURL = null;
        if ( Resources.isEclipseRunning() ) {
            
            //URL resourceURL = Class.forName( className ).getResource( fileName );
            URL resourceURL = LauncherScript.class.getResource( fileName );
            
            if ( resourceURL != null ) {
                
                try {
                    fileURL = FileLocator.toFileURL( resourceURL );
                } catch (IOException e) {
                    
                    throw e;
                }
            } else {
                
                fileURL = null;

            }
        } else {
            
            //fileURL = Class.forName( className ).getResource( fileName );
            fileURL = LauncherScript.class.getResource( fileName );
        }
        return fileURL;
    }
    
    
    /**
     * Returns the URI of the given solver, parameterized from the property file.
     * 
     * @param solverString
     *            the solver property name
     * @return the solver URI
     *
     * @generated
     */
    protected String getSolverUri( String solverString ) {
        
        return properties.getProperty( solverString );
    }
    
    
    /**
     * Returns the path of the given solver, parameterized from the property file.
     * 
     * @param metamodelName
     *            the solver name
     * @return the solver file path
     * @throws IOException
     *             if getFileURL throws exception
     * @throws ClassNotFoundException 
     */
//    protected String getSolverPath( String solverName, String className ) 
//              throws IOException, ClassNotFoundException {
      protected String getSolverPath( String solverName ) throws ClassNotFoundException, IOException {
        
        //return getFileURL( getSolverUri( solverName ), className ).toString();
        return getFileURL( getSolverUri( solverName ) ).toString();
    }
}
