/**
 * 
 */
package com.masdes.solvers.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.masdes.solvers.tools.Resources;
import com.masdes.resources.handlers.HandlerSolver;
import com.masdes.solvers.files.SolverProd;

/**
 * @author maria berenguer
 *
 */
public class HandlerSolverProd extends HandlerSolver {

    /**
     * 
     */
    public HandlerSolverProd() {
        
        super();
        initCommandParameters( 3 );
    }
    
    
    public HandlerSolverProd( String netFileName ) {
        
        super( netFileName );
        initCommandParameters( 3 );
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute( ExecutionEvent event ) 
            throws ExecutionException {
        
        IStructuredSelection structuredSelection = Resources.getStructuredSelection( event );
        
        super.execute( structuredSelection );
        
      //check if it is an IFile
        if ( structuredSelection.getFirstElement() instanceof IFile ) {
            
            this.setCommandParameters( 0 , getOutputDirectoryPathString() );
            this.setCommandParameters( 1, getNetFileName() );
            this.setCommandParameters( 2, getWorkspaceLocation().toPortableString() );
            SolverProd.main( getCommandParameters() );
        }
        
        return null;
    }
    
    
    public Object executeHandler( ExecutionEvent event, Boolean useConsole, IStructuredSelection structuredSelection ) 
            throws ExecutionException {
        
        if ( useConsole ) {
            
            return execute( event );
        } else {
            
            super.execute( structuredSelection );
            
          //check if it is an IFile
            if ( structuredSelection.getFirstElement() instanceof IFile ) {
                
                this.setCommandParameters( 0 , getOutputDirectoryPathString() );
                this.setCommandParameters( 1, getNetFileName() );
                this.setCommandParameters( 2, getWorkspaceLocation().toPortableString() );
                SolverProd.mainSolverprod( getCommandParameters(), false );
            }
        
            return null;
        }
    }

}
