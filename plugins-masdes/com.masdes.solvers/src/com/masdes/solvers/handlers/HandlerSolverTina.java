/**
 * 
 */
package com.masdes.solvers.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.masdes.solvers.tools.Resources;
import com.masdes.resources.handlers.HandlerSolver;
import com.masdes.solvers.files.SolverTina;

/**
 * @author maria berenguer
 *
 */
public class HandlerSolverTina extends HandlerSolver {

    /**
     * 
     */
    public HandlerSolverTina() {
        
        super();
        initCommandParameters( 3 );
    }
    
    
    public HandlerSolverTina( String netFileName ) {
        
        super( netFileName );
        initCommandParameters( 3 );
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
     */
    @Override
    public Object execute( ExecutionEvent event ) 
            throws ExecutionException {
        
        IStructuredSelection structuredSelection = Resources.getStructuredSelection( event );
        
        super.execute( structuredSelection );
        
        //check if it is an IFile
        if ( structuredSelection.getFirstElement() instanceof IFile ) {
            
            this.setCommandParameters( 0, getOutputDirectoryPathString() );
            this.setCommandParameters( 1, getNetFileName() );
            this.setCommandParameters( 2, getWorkspaceLocation().toPortableString() );
            SolverTina.main( getCommandParameters() );
        }
        
        return null;
    }
    
    public Object executeHandler( ExecutionEvent event, Boolean useConsole, IStructuredSelection structuredSelection ) 
            throws ExecutionException {
        
        if ( useConsole ) {
            
            return execute( event );
        } else {
            
            super.execute( structuredSelection );
            
          //check if it is an IFile
            if ( structuredSelection.getFirstElement() instanceof IFile ) {
                
                this.setCommandParameters( 0 , getOutputDirectoryPathString() );
                this.setCommandParameters( 1, getNetFileName() );
                this.setCommandParameters( 2, getWorkspaceLocation().toPortableString() );
                SolverTina.mainSolvertina( getCommandParameters(), false );
            }
        
            return null;
        }
    }
}
