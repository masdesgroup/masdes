package com.masdes.solvers.tools;

public class Resources extends com.masdes.resources.Resources {

    public static final String SOLVERTINA_PROPERTIES_FILE = "SolverTina.properties";
    public static final String SOLVERPROD_PROPERTIES_FILE = "SolverProd.properties";
    
    public static final String PROP_SOLVERS = "solvers";
    public static final String PROP_SCRIPT = ".script";
    public static final String PROP_SOLVER = ".solver";
    
    public static final String LOADING_COMMAND_PARAMETERS = "Loading command parameters";
}
